Rechteverwaltung
=================


Ein Datenbankserver verwaltet unter Umständen eine Vielzahl von Datenbanken, die von vielen Applikationen/Clients genutzt werden. Manchmal ist es auch so, dass verschiedene Applikationen auf die gleichen Datenbanken zugreifen müssen, dort aber nur jeweils spezifische Informationen sehen sollen. Manchmal ist es nicht wünschenswert, dass Benutzer die Möglichkeit haben, andere Datenbanken als ihre eigenen zu sehen, insbesondere wenn sie über Administrationswerkzeuge Zugriff auf den Datenbankserver haben. 

Es gibt also viele Gründe, warum man sich über eine Rechtevergabe als Datenbank Gedanken machen sollte.

Aufgabe des Rechtesystems
--------------------------


.. only:: html

	.. sidebar:: Aufgabe


		Finden Sie heraus, wie das Rechtesystem von MS SQL Server bzw. Oracle aufgebaut ist.
	
.. only:: latex

     .. addmonition
     
     		inden Sie heraus, wie das Rechtesystem von MS SQL Server bzw. Oracle aufgebaut ist.

     		

Die Aufgabe des Rechtesystems besteht darin,

- einen User, der von einem bestimmten Host auf die Datenbank connected, zu authentifizieren.

- dem User für eine spezielle Datenbank mit seinen jeweiligen Rechten für beispielsweise INSERT, UPDATE, DELETE und SELECT zu versehen.

- ihn mit speziellen Rechten zu versorgen wie z.B. LOAD DATA INFILE und administrative Aufgaben zu ermöglichen.


Diese Grundaufgaben sind unabhängig vom jeweiligen Datenbanksystem; in der Umsetzung dieser Aufgaben sowie dem Umfang der Fähigkeiten unterscheiden sich allerdings die verschiedenen Datenbanksysteme. Die Datenbankabfragesprache SQL bietet mit ihrem deklarativen Ansatz allerdings wieder eine allgemeine Lösung, die für viele Datenbanken einen gemeinsamen Nenner definiert.

Im Folgenden wird das Datenbanksystem MySQL beschrieben.


Funktionsweise des Rechtesystems 
---------------------------------
 
.. only:: html

	.. sidebar:: DB mysql
	
		.. image:: figure/mysql_db_mysql.jpg
		
		
.. only:: latex

		.. image:: figure/mysql_db_mysql.jpg
	


MySQL benutzt immer die Kombination aus Usernamen und Clienthost, um einen Anwender zu identifizieren. Alle Informationen zum Rechtesystem speichert mySQl in der Datenbank mysql; insbesondere die Tabellen *user, host, db* werden hierzu benötigt.

Die Zugriffskontrolle in MySQL läuft in 2 verschiedenen Phasen ab.

#. Der Server überprüft, ob es dem User erlaubt ist, sich auf den Datenbankserver zu verbinden. Der Server benutzt dazu die Tabelle **user** .

#. Diese Tabelle benutzt alle Host/User Kombinationen, welche den MySQL-Server connecten dürfen. Alle Berechtigungen die ein Benutzer in dieser Tabelle enthält gelten für alle Datenbanken, sofern keine erweiterten Berechtigungen für den jeweiligen Benutzer in der Tabelle **db** definiert wurden. Man kann diese Berechtigungen auch als grundlegende Einstellungen ansehen und ein datenbankabhängiges Fein-Tuning in der Tabelle db festlegen. 


#. Wenn Phase 1 erfolgreich abgelaufen ist, wird vom Server jedes Statement daraufhin geprüft, ob der jeweilige User genügend Rechte hat, um dieses Statement auszuführen. Der Server benutzt dabei die Tabelle **db**.


#.  host-Tabelle

    Die host-Tabelle ist ab der mysql-Version 5.5.7 obsolet.
    Sie diente in großen Netzwerken als Nachschlage-Tabelle für leere Host-Einträge.


#. Fein-Tuning per tables_priv und columns_priv

   In der 2. Phase können zusätzlich die Tabellen tables_priv und columns_priv abgefragt werden, wenn sich der Request auf Tabellen bezieht. Letztendlich kann damit bis auf Spaltenebene eine Zugriffsverwaltung realisiert werden.




Aufgabe

Vertretung SYC am 21.11.2013

Donnerstag, 21. November 2013
08:17

Aufgaben zu GRANT, REVOKE
	1. Which components of MySQL must you protect on the filesystem level?
	2. You want to set up a MySQL administrator account with all privileges. This administrator should be called superuser and the password should be s3creT. superuser should be able to connect only from the local host, and should be allowed to create new MySQL users. How would you create this account?
	3. What GRANT statement would you issue to set up an account for user steve, who should be able to manipulate data of tables only in the accounting database? steve should be able to connect to the server from anywhere. The account password should be some_password1.
	4. What GRANT statement would you issue to set up an account for user pablo, who should be able to do all kinds of operations on the tables in the marketing database and should also be able to grant permissions to do those operations to other MySQL users? pablo should be able to connect to the server from the local network where IP numbers of all machines start with 192.168. The account password should be some_password2.
	5. What GRANT statement would you issue to set up an account for user admin, who should be able to administer the database server, including performing all operations on all its databases and tables? admin should not, however, be able to grant privileges to other accounts. admin should be able to connect to the server only from the local host. The account password should be some_password3.
	6. Consider the following privilege settings for the accounts associated with a given MySQL username, where the Select_priv column indicates the setting for the global SELECT privilege:
mysql> SELECT Host, User, Select_priv FROM mysql.user WHERE User = 'icke';

+--------------+------+-------------+
| Host         | User | Select_priv |
+--------------+------+-------------+
| 62.220.12.66 | icke | N           |
| 62.220.12.%  | icke | Y           |
| 62.220.%     | icke | N           |
+--------------+------+-------------+
The Select_priv column indicates that the SELECT privilege for the second entry has been granted on a global scope (*.*). Will user icke be able to select data from any table on the MySQL server when connecting from the following hosts:
62.220.12.66
62.220.12.43
62.220.42.43
localhost
Assume that the icke accounts are not granted privileges in any of the other grant tables.




