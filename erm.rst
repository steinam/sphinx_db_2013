ERM (Entity Relationship Model) 
===============================

.. only:: html

	.. sidebar:: Link
	
		http://de.wikipedia.org/wiki/ER-Modell
		
		.. image:: figure/ERD_Darstellungen.png
		   :width: 300 px
		   
		Videos
	
		 - :download:`ERM I <figure/database/13.08_ERM_Grundlagen_Funktion.mp4>`.
		 - :download:`ERM II <figure/database/13.09 Entity Relationship Modellierung II_ Chen versus min_m.mp4>`.	
		 - :download:`ERM II <figure/database/13.10_ERM_III_ schwache Entitaet.mp4>`.
		 - :download:`ERM II <figure/database/13.11_ERM_IV_Sichtenkonsolidierung.mp4>`.
		 
		


In der Informatik ist es üblich, dass man EDV-Systeme als Modell darstellt. Vielleicht kennen Sie schon solche Modelle aus der Programmierung: Programmablaufplan nach [ISO85](ISO/IEC 5807, DIN 66001), Struktogramm nach [Nor85] (DIN 66261), Datenflussdiagramm nach [ISO85] (ISO 5807), UML-Klassendiagramme usw. 

Die Modellierungstechnik für relationale Datenbanken ist das Entity Relationship Model oder auch ER-Modell. Dieses gibt es in verschiedenen Notationen.



- **Chen**: Hier werden die Tabellen als Rechtecke dargestellt. Die Spalten der Tabellen werden als Blasen um die Tabelle herum notiert. In die Blase schreibt man den Spaltennamen. Der Name des Primärschlüssels wird dabei unterstrichen.

- **IDEF1X**: Die Tabellen werden auch hier als Rechtecke (ggf. mit abgerundeten Ecken) dargestellt. Die Spalten werden aber innerhalb des Rechtecks notiert. Die Primärschlüsselspalten werden dabei durch einen Linie von den anderen Spalten abgetrennt. Diese Notation ist der Quasistandard US-amerikanischer Behörden.

- **Krähenfuß (Martin)**: Die Tabellen werden in Rechtecken dargestellt. Diese enthalten die Spaltennamen. Vor dem Spaltennamen ist Platz für eine weitere Spezifikation der Spalte (z.B. als Fremdschlüssel).

- Weitere Notationen, z.B. UML



.. note:: Ausgangssituation

	 Die Datenbank FIRMA verwaltet die Angestellten, Abteilungen und Projekte einer Firma. Die Phase der Erfassung und Analyse der Anforderungen ist abgeschlossen und die Designer haben folgende Beschreibung der Firma erstellt.

	 Die Firma ist in Abteilungen organisiert. Jede Abteilung hat eine eindeutige Bezeichnung, eine eindeutige Nummer und einen bestimmten Angestellten, der die Abteilung leitet. Wir verfolgen das Anfangsdatum, ab dem dieser Angestellte die Leitung der Firma übernommen hat. Eine Abteilung verfügt über mehrere Standorte

	 Eine Abteilung kontrolliert eine Reihe von Projekten, die jeweils einen eindeutigen Namen, eine eindeutige Nummer und einen einzigen Standort haben. Die Abteilung kennt die Anzahl ihrer Mitarbeiter.

	 Wir speichern zu jedem Angestellten den Namen, die Sozialversicherungsnummer, die Adresse, das Gehalt, das Geschlecht und das Geburtsdatum. Ein Angestellter wird einer Abteilung zugewiesen, kann aber an mehreren Projekten arbeiten, die nicht unbedingt alle von der gleichen Abteilung kontrollirt werden. Wir verfolgen die Stundenzahl pro Woche, die ein Angestellter an jedem Projekt arbeiten, und den unmittelbaren Vorgesetzten jedes Angestellten.

	 Zu Versicherungszwecken möchten wir die Familienangehörigen jedes Mitarbeiters verfolgen. Wir führen also jeden Angehörigen mit Vornamen, Geschlecht, geburtsdatum und Verwandtschaftsgrad zum jeweiligen Angestellten.



.. raw:: latex

         \newpage
	 
	 
.. index:: Entität

Entität
--------

Das vom ER-Modell dargestellte Basisobjekt ist die sog. Entität . Sie kann real existieren (z.B. Auto) oder lediglich konzeptionell (z.B. eine Firma).
Jede Entität hat Attribute , d.h. bestimmte Eigenschaften, die sie beschreiben. Ein Attribut einer Entität wird mit sog. Attributwerten belegt.

Untenstehende Abbildung zeigen mehrere Darstellungsformen einer Entität. 

.. image:: figure/entitaet.png

.. index:: Attribut

**Attributarten**

Im ER-Modell kommen mehrere Attributarten vor: 

.. image:: figure/ERMAtributeType.png



.. raw:: latex

         \newpage


.. index:: Relation, Kardinalität

Relation und Kardinalität
-------------------------

Eine Entität besitzt neben ihren eigentlichen Attributen auch Beziehungen zu anderen Entitäten. Diese werden durch Linien zwischen den Entitäten (Relation) zum Ausdruck gebracht. Neben dieser Linie werden durch sog. **Kardinalitäten** eine Aussage über die Anzahl der Beziehungsobjekte der jeweils anderen Seite zu sich selbst getroffen.

Dies mündet in die drei grundlegenden Beziehungsmengen:

.. index:: Kardinalität, 1:n

1: n (one to many)
^^^^^^^^^^^^^^^^^^
 
.. sidebar::  Grafik
  
     .. image:: figure/kard_1_n.jpg
          :width: 400 px
     	 
Hier hält eine Entität jeweils mehrere Beziehungen zu Entitäten der anderen Seite, die andere Seite jedoch nur eine Beziehung. 
  
So hat eine Abteilung, z.B. Einkauf, zwar viele Angestellte, ein Angestellter arbeitet aber nur in einer Abteilung
  
.. image:: figure/ERM_Kard_1_N_ERM.png
       



n : m (many to many)
^^^^^^^^^^^^^^^^^^^^

.. sidebar:: Grafik 
  
     .. image:: figure/ERM_Kard_N_M_Schueler.png
        :width: 400 px
     	
Hier hat jede Entität jeweils mehrere Beziehungen zu Entitäten der anderen Seite. So arbeitet **ein* Angestellter in vielen Projekten, an *einem** Projekt können aber auch viele Angestellte arbeiten.


.. image:: figure/ERM_Kard_N_M_ERM.png
     :width: 500 px
  
  
  
1 : 1 (one to one)
^^^^^^^^^^^^^^^^^^^

.. sidebar:: Grafik

	.. image:: figure/ERM_Kard_1_1_Schueler.png
	    :width: 300 px

Hier hat jede Entität maximal eine Beziehung zur anderen Entität und umgekehrt.

.. image:: figure/ERM_Kard_1_1_ERM.png
     :width: 500 px
     
     
                               
.. raw:: latex

         \newpage

     
Attribut einer Relation
^^^^^^^^^^^^^^^^^^^^^^^^^

In manchen Fällen können auch die Relationen Träger von Attributen sein. So ist z.B. der Beginn der Leitung einer Abteilung ein Attribut der Relation und nicht eines der beteiligten Entitäten.


.. image:: figure/ERM_RELATION_Attribut.png


Rekursive Assoziation
^^^^^^^^^^^^^^^^^^^^^

.. sidebar:: Grafik

	.. image:: figure/ERM_Kard_Rekursiv_Schueler.png
	     :width: 300 px

In manchen Fällen kann eine Assoziation mit beiden Enden auf die gleiche Entität verweisen. So ist der Vorgesetzte eines Angestellten ebenfalls ein Angestellter. 
Um die beiden Teile der Assoziation besser auseindanderhalten zu können, schreibt man auf die jeweilige Seite die sog. Rolle , die eine Entität im Bezug auf seine Beziehungsseite spielt. 


.. image:: figure/ERM_Kard_Rekursiv_ERM.png
     :width: 300 px



.. raw:: latex

	\newpage
	
     
Aufgaben
---------

.. only:: html

	.. sidebar:: Lösungen
	
		 - :download:`Bibliothek <loesung/Bibliothek.jpg>`.
		 - :download:`Pizza <loesung/loesung_pizza.png>`.	 
		 - :download:`Bus <loesung/loesung_bus.png>`.
		 - :download:`Olympia <loesung/loesung_olympia.png>`.
	 
	 
	 


#. Bibliothek

   Folgender Sachverhalt beschreibt die Verhältnisse in einer Bibliothek
   
   In einer Bibliothek befinden sich zahlreiche Bücher, die jeweils von einem oder mehreren Autoren verfasst wurden . Von einigen Autoren existieren bereits verschiedene Bücher. Jedes Buch stammt von einem bestimmten Verlag (wobei die Verlage natürlich mehrere Bücher veröffentlicht haben) und befindet sich an einem definierten Standort. Ein (Regal-)Standort umfasst allerdings mehrere Bücher. Entliehen werden die Bücher von Kunden, die mehrere Bücher ausleihen können.


   Lösung

#. Pizza-Service

   Zur Zeit finanzieren Sie Ihr kostspieliges Privatleben als EDV-Berater bei einem Pizzaservice. Aufgrund seiner guten Pizzen fallen immer mehr Bestellungen an, so dass die wachsende Zahl an Zetteln zu einem Chaos in der Küche geführt hat. Eine Lösung verspricht sich der Inhaber des Pizzaservices durch die Verwendung einer Datenbank, mit der alle Bestellungen und Lieferungen verwaltet werden können. Dazu liegt Ihnen folgender zu modellierender Wirklichkeitsausschnitt vor:
   
   Der Pizzaservice bietet verschiedene Pizzen an (gekennzeichnet durch eine Nummer). Jede Pizza besteht aus Teig, Tomaten, Käse und diversen Zutaten (Salami, Schinken, Pilze usw.). Die Zutaten zu einer Pizza sind fest vorgegeben; eine maximale Anzahl an (möglichen) Zutaten ist jedoch nicht vorgegeben. Es gibt eine Kundendatei, in der die Kunden anhand ihrer Telefonnummern eindeutig identifiziert werden. Jede Bestellung kommt von genau einem Kunden, umfasst beliebig viele Pizzen (aber mindestens eine) und wird von einem Fahrer ausgeliefert. Die Bestellungen werden durch eine fortlaufende Bestellnummer identifiziert. Auf einer Tour kann ein Fahrer mehrere Bestellungen ausliefern.

   
#. Bus

   Sie werden beauftragt, für ein Busreiseunternehmen eine Datenbank zu entwickeln. Gehen Sie hierbei von folgendem Wirklichkeitsausschnitt aus, der abgebildet werden soll. Das Unternehmen bietet ausschließlich Städtereisen an. Für verschiedene europäische Städte existieren bestimmte Reiseangebote (jeweils genau eines), die Eigenschaften wie den Namen der Stadt, den (konstanten) Preis sowie die Reisedauer (Anzahl der Übernachtungen) besitzen. Für jede Stadt bzw. das entsprechende Reiseangebot existieren verschiedene terminliche Ausprägungen. Eine solche konkrete Reise wird jeweils von genau einem Busfahrer durchgeführt. Die Busfahrer des Unternehmens besitzen jeweils nur Kenntnisse für bestimmte Städte; dementsprechend besitzen sie nur die Fähigkeit zur Durchführung bestimmter Reisen. Die Kunden des Unternehmens sind ebenfalls in dem System abzubilden. Hierbei ist auch abzubilden, welche Kunden welche konkreten Reisen gebucht haben bzw. hatten.
   
#. Olympia

   Sie arbeiten in einer EDV-Beratungsfirma. Diese Beratungsfirma erhält den Auftrag, für das IOC (International Olympic Committee) eine Datenbank für die nächsten Olympischen Spiele zu erstellen, die folgenden Wirklichkeitsausschnitt enthalten soll.
   
   Die einzelnen Wettkämpfe der Olympischen Spiele sind durch den Namen der Sportart, den Termin und die Sportstätte gekennzeichnet. An jedem Wettkampf nehmen mehrere Sportler teil, die durch eine Startnummer identifiziert werden und außerdem natürlich, wie jede Person, einen Namen besitzen. Jeder Wettkampf wird von einem Schiedsrichter geleitet, dem für diese Spiele eine eindeutige Personalnummer zugeordnet wurde. Die Schiedsrichter werden bei einem Wettkampf von verschiedenen Helfern unterstützt, die ebenfalls eine eindeutige Personalnummer erhalten haben. Die Sportler und Schiedsrichter gehören jeweils einer Nation an, zu der der Name des Mannschaftsleiters und eine Telefonnummer für Rückfragen abgespeichert werden. Dies gilt zwar ebenfalls für die Helfer, soll jedoch hier nicht berücksichtigt werden. Erstellen Sie ein ER-Modell mit den wesentlichen Informationen bzgl Attributen, Beziehungen und Kardinalitäten.

#. Hotel

   Im Rahmen eines Hotel-Softwareprojektes werden Sie gebeten, die Datenhaltungsschicht zu programmieren. Erstellen Sie ein ER-Modell nach Auszug aus dem Lastenheft. Die Anwendung soll folgende Bereiche abdecken:

   1. Kundenverwaltung
   Zu jedem Kunden werden seine persönlichen Daten sowie Rechnungsanschriften erfasst. Falls Kunden einer bestimmten Firma angehören, sollen diese einer Firma zugeordnet werden können, um spätere Rabatte berechnen zu können.

   2. Raumreservierung
   Für jeden Raum wird seine Bezeichnung, Ausstattung und Tarifgruppe erfasst. Ebenso sollen pro Raum mehrere Reservierungszeiträume erfasst werden können. Diese Zeiträume sollen einem Kunden zugeordnet werden.

   3. Rechnungsstellung
   Für jeden Kunden soll anhand der Belegzeiten und sonstiger Leistungen (Mini-Bar, Sauna etc.) eine Rechnung zusammengestellt werden können.

     
#. Schwache Entität
   Suchen Sie im Internet nach diesem Begriff und erläutern Sie seine Bedeutung
   
   
   
.. TODO::  add some text


