.. Datenbanken documentation master file, created by
   sphinx-quickstart on Sat Jul 27 16:23:39 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Datenbanken !
=============


.. toctree::
   :maxdepth: 2



   erm.rst
   erm_table_nf.rst
   sql_ddl.rst
   sql_select.rst
   sql_transaktion.rst
   sql_view.rst
   sql_trigger.rst
   sql_function.rst
   sql_procedure.rst
   sql_index.rst
   grant_revoke.rst
   sql_aufgaben.rst


..    db_history.rst
..   aufbau.rst
..   mysql_as_example.rst




Links
=====

http://wwwiti.cs.uni-magdeburg.de/iti_db/lehre/db1/

Literatur- und Quellenverzeichnis
=================================

- http://de.wikibooks.org/wiki/Einf%C3%BChrung_in_SQL
  Für die wesentlichen Elemente der Sprache SQL und der Beispieldatenbank

- Adams


Stichwort- und Tabellenverzeichnis
===================================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
