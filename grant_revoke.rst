.. include:: image.txt



grant, revoke - UserManagament
====================================

Ein Datenbankserver verwaltet unter Umständen eine Vielzahl von Datenbanken, die von vielen Applikationen genutzt werden.
Applikationen werden von vielen Benutzern genutzt, die unterschiedliche Berechtigungen besitzen sollten. Manchmal ist es nicht wünschenswert, dass Benutzer die Möglichkeit haben, andere Datenbanken als ihre eigenen zu sehen, insbesondere wenn sie per Administrationswerkzeugen Zugriff auf den Datenbankserver haben.

Aufgabe des Rechtesystems
--------------------------------

Die Aufgabe des Rechtesystems besteht darin,


-  einen User, der von einem bestimmten Host auf die Datenbank connected, zu authentifizieren.
-  dem User für eine spezielle Datenbank mit seinen jeweiligen Rechten für beispielsweise INSERT, UPDATE, DELETE und SELECT zu versehen.
-  ihn mit speziellen Rechten zu versorgen wie z.B. LOAD DATA INFILE und administrative Aufgaben


Funktionsweise des Rechtesystems
----------------------------------------

MySQL benutzt immer die Kombination aus Usernamen und Clienthost, um einen Anwender zu identifizieren.

Die Zugriffskontrolle in MySQL läuft in 2 verschiedenen Phasen ab.


#. Der Server überprüft, ob es dem User erlaubt ist, sich auf den
   Datenbankserver zu verbinden. Der Server benutzt dazu die Tabelle
   **user**.

   Diese Tabelle benutzt alle Host/User Kombinationen, welche den
   MySQL-Server connecten dürfen. Alle Berechtigungen die ein Benutzer
   in dieser Tabelle enthält gelten für alle Datenbanken, sofern keine
   erweiterten Berechtigungen für den jeweiligen Benutzer in der
   Tabelle **db** definiert wurden. Man kann diese Berechtigungen auch
   als grundlegende Einstellungen ansehen und ein datenbankabhängiges
   Fein-Tunig in der Tabelle db festlegen.


   .. image:: figure/mysql/table_user.jpg

#. Wenn Phase 1 erfolgreich abgelaufen ist, wird vom Server jedes
   Statement daraufhin geprüft, ob der jeweilige User genügend Rechte
   hat, um dieses Statement auszuführen. Der Server benutzt dabei die
   Tabelle **db**.

   .. image:: figure/mysql/table_db.jpg
   
   **host-Tabelle**

   Die host-Tabelle ist in großen Netzwerken als Nachschlage-Tabelle
   für leere Host-Einträge in der db-Tabelle sinnvoll. Möchte man, daß
   ein Benutzer von jedem Host in dem Netzwerk auf den DB-Server
   zugreifen kann, sollte man den Host-Eintrag in der db-Tabelle
   auslassen und alle Host des Netzwerkes in der host-Tabelle
   eintragen. Sie wird in neueren MySQL-Versionen nicht mehr genutzt.

   
   
   
   .. image:: figure/mysql/table_host.jpg
   
   
   Sollten diese Rechte zur Laufzeit einer Verbindung geändert werden,
   so bedeutet es nicht, dass diese Änderungen automatisch geändert
   sind. Da der Server die Rechte beim Start aus den jeweiligen
   Tabellen liest, muss man ihn dazu auffordern, diese Rechte neu
   laden.

#. **Fein-Tuning per tables_priv und columns_priv**

   In der 2. Phase können zusätzlich die Tabellen **tables\_priv** und
   **columns\_priv** abgefragt werden, wenn sich der Request auf
   Tabellen bezieht.

   .. image:: figure/mysql/table_column_priv.jpg
   
   .. image:: figure/mysql/table_tables_priv.jpg

   
   
Definitionen
--------------------

Die HOST und DB Spalten können Strings mit Wildcards % und _
beinhalten. Wird für diese Spalten kein Wert eingetragen entspricht
dies dem Wert %. Ein HOST kann sein:




-  localhost
-  ein Hostname
-  eine IP-Nummer
-  ein String mit Wildcards


Ein leerer HOST-Eintrag in der db-Tabelle bedeutet -kein Host- aus der host-Tabelle. Ein leerer HOST-Eintag in der host- oder user-Tabelle bedeutet -kein Host-.

Die Spalte DB beinhaltet den Namen einer Datenbank oder einer SQL Regexp.

Ein leerer Benutzereintrag bedeutet -kein Benutzer-. In dieser Spalte können keine Wildcards verwendet werden.

Die Berechtigungen der user-Tabelle werden ge-OR-d mit den Berechtigungen aus der db-Tabelle. Dies Bedeutet, daß ein Superuser nur in der Tabelle user mit allen Berechtigungen festgelegt auf Y eingetragen werden muß.

Wenn man sich nun den Aufbau der Tabellen näher betrachtet, wird man feststellen, daß die user-Tabelle zusätzlich zu den Zugriffsberechtigungen auf die jeweilige Datenbank auch administrative Berechtigungen regelt. Dadurch sollte klar sein, dass diese Tabelle die grundlegenden Berechtigungen regelt.

Benutzerkonten mit GRANT und REVOKE erstellen.    
------------------------------------------------------

.. code-block:: sql

    GRANT priv_type [(column_list)] [, priv_type [(column_list)]] ...
        ON {tbl_name | * | *.* | db_name.*}
        TO user [IDENTIFIED BY [PASSWORD] 'password']
            [, user [IDENTIFIED BY [PASSWORD] 'password']] ...
        [REQUIRE
            NONE |
            [{SSL| X509}]
            [CIPHER 'cipher' [AND]]
            [ISSUER 'issuer' [AND]]
            [SUBJECT 'subject']]
        [WITH [GRANT OPTION | MAX_QUERIES_PER_HOUR count |
                              MAX_UPDATES_PER_HOUR count |
                              MAX_CONNECTIONS_PER_HOUR count]]
    
    REVOKE priv_type [(column_list)] [, priv_type [(column_list)]] ...
        ON {tbl_name | * | *.* | db_name.*}
        FROM user [, user] ...
    
        REVOKE ALL PRIVILEGES, GRANT OPTION FROM user [, user] ...
        

Beispiel für die Vergabe von Benutzerrechten
---------------------------------------------------



.. code-block:: sql

      GRANT SELECT ON testgrant.* TO 'jim'@'localhost' IDENTIFIED BY 'Abc123'
        

.. image:: figure/mysql/grant_jim1.jpg

.. image:: figure/mysql/grant_jim2.jpg

Erzeugt ein **SELECT-Privileg** für alle Tabellen in der Datenbank **testgrant** für eine User namens **jim**. jim muss sich von **localhost** verbinden und das Passwort **Abc123** benutzen:

.. code-block:: sql

    GRANT SELECT, INSERT, DELETE, UPDATE ON world.* TO 'jim'@'localhost' IDENTIFIED BY 'Abc123';

Mehrere Rechte werden vergeben

.. code-block:: sql

    GRANT SELECT (ID, Name, CountryCode), UPDATE (Name, CountryCode)
    ON world.City TO 'jim'@'localhost' IDENTIFIED BY 'Abc123';

Die **IDENTIFIED BY** -Klausel ist optional. Ist sie vorhanden, weist sie einem User ein Passwort zu. Hat der User bereits ein Passwort, sow wird es überschrieben. Wenn der User bereits existiert und die Klausel weggelassen wird, so bleibt das gegenwärtige Passwort des Users wie es ist. Rechte auf spezielle Spalten einer Tabelle.

.. code-block:: sql

    GRANT SELECT ON world.* TO ''@'localhost';

Anonymer Zugriff auf alle Tabellen der Tabelle world

.. code-block:: sql

    GRANT SELECT ON world.* TO 'jim'@'localhost' IDENTIFIED BY 'Abc123' WITH GRANT OPTION;

SELECT-Rechte auf die Datenbank world mit der Möglichkeit, dieses
Recht weiterzugeben.

.. code-block:: sql

	mysql> SHOW GRANTS FOR jim@localhost
    
    mysql>   Grants for jim@localhost
            GRANT USAGE ON *.* TO 'jim'@'localhost' IDENTIFIED...
            GRANT SELECT ON `testgrant`.* TO 'jim'@'localhost'
            
    mysql>  SHOW GRANTS FOR jim@192.168.0.140 
            #1141 - There is no such grant defined for user 'jim' on host '192.168.0.140' 

Zeigt alle Rechte für den jeweiligen User laut Eintrag in User-Tabelle an.


REVOKE - Wegnahme von Benutzerrechten
---------------------------------------------

Das REVOKE statement nimmt die Privilegien eines Accounts. Seine Syntax hat die 
folgenden Abschnitte:


-  Das Schlüsselwort **REVOKE**, gefolgt von der Liste der wegzunehmenden Rechte
-  Die **ON** - Klause, die den Level angibt, an der die Rechte entzogen werden.
-  Die **FROM**-Klausel, die den Usernamen spezifiziert


.. code-block:: sql

	REVOKE DELETE, INSERT, UPDATE ON world.* FROM 'jim'@'localhost';
    REVOKE GRANT OPTION ON world.* FROM 'jill'@'localhost';
      
    mysql> SHOW GRANTS FOR 'jen'@'myhost.example.com';
    
    +----------------------------------------------------------------+
    | Grants for jen@myhost.example.com                              |
    +----------------------------------------------------------------+
    | GRANT FILE ON *.* TO 'jen'@'myhost.example.com'                |
    | GRANT SELECT ON `mydb`.* TO 'jen'@'myhost.example.com'         |
    | GRANT UPDATE ON `test`.`mytable` TO 'jen'@'myhost.example.com' |
    +----------------------------------------------------------------+
    
    mysql> REVOKE FILE ON *.* FROM 'jen'@'myhost.example.com';
    mysql> REVOKE SELECT ON mydb.* FROM 'jen'@'myhost.example.com';
    mysql> REVOKE UPDATE ON test.mytable FROM 'jen'@'myhost.example.com';
    
    
    mysql> SHOW GRANTS FOR 'jen'@'myhost.example.com';
    +--------------------------------------------------+
    | Grants for jen@myhost.example.com                |
    +--------------------------------------------------+
    | GRANT USAGE ON *.* TO 'jen'@'myhost.example.com' |
    +--------------------------------------------------+
    
    mysql> USE mysql;
    mysql> DELETE FROM user WHERE User = 'jen' AND Host = 'myhost.example.com';
    mysql> FLUSH PRIVILEGES;
    
      
Bearbeiten Sie folgende :download:`Aufgabe <figure/arbeitsblatt/Uebung_grant.docx>`.
