Trigger
=======

.. figure:: figure/logik_schueler.png


Ein Trigger ist so etwas wie eine Routine zur Ereignisbehandlung: Immer dann, wenn sich in der Datenbank eine bestimmte Situation ereignet, wird eine spezielle Prozedur automatisch ausgeführt.

Ein Trigger ist in folgenden Situationen nützlich:

- Werte in einer Zeile einer Tabelle sollen festgelegt werden.
- Werte sollen vor dem Speichern auf ihre Plausibilität geprüft werden.
- Veränderungen in der Datenbank sollen automatisch protokolliert werden.

Trigger erstellen
------------------

Die Syntax für die Deﬁnition eines Triggers sieht grundsätzlich so aus:

.. code-block:: sql

	CREATE OR ALTER TRIGGER <routine-name> FOR <Tabellenname>
	[ ACTIVE | INACTIVE ]
	{ BEFORE | AFTER } 
	{ INSERT | UPDATE | DELETE }
	[ POSITION <zahl> ]
	AS
	BEGIN
	[ <variablenliste> ]
	<routine body>
	END


Notwendig sind folgende Angaben:
- neben dem Befehlsnamen der Name des Triggers
- dazu die Tabelle, zu der er gehört
- mehrere Angaben, bei welchem Befehl und an welcher Stelle er wirksam sein soll;
- das Schlüsselwort AS als Zeichen für den Inhalt
- die Schlüsselwörter BEGIN und END als Begrenzer für den Inhalt

Hinzu kommen die folgenden Angaben:

- eine Liste von Variablen, die innerhalb der Routine verwendet werden; diese
Liste steht je nach DBMS zwischen AS und BEGIN oder innerhalb des Rumpfes
(also zwischen BEGIN und END)
- die Befehle, die innerhalb der Prozedur ausgeführt werden sollen
Eingabe- und Ausgabe-Parameter gibt es nicht, weil es sich um automatische Ar-
beitsabläufe handelt.

   		
Die folgenden Variablen stehen immer zur Verfügung.

- **OLD** ist der Datensatz vor einer Änderung.
- **NEW** ist der Datensatz nach einer Änderung.

Mit diesen Variablen zusammen mit den Spaltennamen können die Werte „vorher“ und „nachher“ geprüft und bearbeitet werden.
Bei den Befehlen innerhalb des Triggers handelt es sich (eher selten)
um „normale“ SQL-Befehle und überwiegend um Bestandteile der SQL-
Programmiersprache. 

	
.. Addmonition:: Beispiel

	Beim Ausscheiden eines Mitarbeiters wird (soweit vorhanden) ein persönlicher Dienstwagen gestrichen.

	.. code-block:: sql
		--Firebird
		CREATE TRIGGER Mitarbeiter_BD1 for Mitarbeiter
			ACTIVE BEFORE DELETE POSITION 1
		AS
		BEGIN
			update Dienstwagen
			set Mitarbeiter_ID = null
			where Mitarbeiter_ID = old.ID;
		END

		
		delimiter //
		
		create trigger Mitarbeiter_BD1 before DELETE
		on Mitarbeiter
		for each row
		
        	BEGIN
			update Dienstwagen
			set Mitarbeiter_ID = null
			where Mitarbeiter_ID = old.ID;	
        	END;
		delimiter ;
		
	Bevor ein Mitarbeiter gelöscht wird, wird geprüft, ob ihm ein persönlicher Dienstwagen zugewiesen ist. Dieser Vermerk wird (sofern vorhanden) auf NULL gesetzt;
	die WHERE-Klausel greift dabei auf die ID des bisherigen Datensatzes der Tabelle
	Mitarbeiter zu.


**Aufgabe**

Skizzieren Sie den Inhalt eines Triggers Mitarbeiter_On_Delete für die Tabelle
Mitarbeiter, der folgende Aufgaben ausführen soll:

- Zu behandeln ist die Situation, dass ein Mitarbeiter aus der Firma ausscheidet, also zu löschen ist.
- Suche die ID des zugehörigen Abteilungsleiters.
- In allen Tabellen und Datensätzen, in denen der Mitarbeiter als Sachbearbeiter registriert ist, ist stattdessen der Abteilungsleiter als „zuständig“ einzutragen. Ignorieren Sie die Situation, dass der Abteilungsleiter selbst ausscheidet.


**Lösung**

- Wir brauchen einen Trigger BEFORE DELETE zur Tabelle Mitarbeiter.
- Wir brauchen eine Variable für die ID des Abteilungsleiters.
- Durch einen SELECT auf die Abteilung_ID des ausscheidenden Mitarbeiters be-
kommen wir die ID des Abteilungsleiters; die wird in der Variablen gespeichert.
- Für alle betroffenen Tabellen ist ein UPDATE zu machen, durch das die Mitarbeiter_ID durch die ID des Abteilungsleiters ersetzt wird. Dies betrifft die Tabellen Schadensfall, Versicherungsvertrag.
- Die Tabelle Dienstwagen kann unberücksichtigt bleiben wegen des o. g. Beispiels.

.. code-block:: sql

	-- Firebird-Version
	create or alter trigger Mitarbeiter_On_Delete
		for Mitarbeiter
		ACTIVE BEFORE DELETE POSITION 10
		as
			DECLARE VARIABLE ltr_id INTEGER;
			BEGIN
			ltr_id = null;
			-- hole die ID des Abteilungsleiters in die Variable
			select ID
			from Mitarbeiter
			where Abteilung_id = old.Abteilung_ID
			and Ist_Leiter = ’J’
			into :ltr_id;
			-- ändere die Mitarbeiter_ID für die Schadensfälle
			update Schadensfall
			set Mitarbeiter_ID = :ltr_id
			where Mitarbeiter_ID = old.ID;
			-- ändere die Mitarbeiter_ID für die Verträge
			update Versicherungsvertrag
			set Mitarbeiter_ID = :ltr_id
			where Mitarbeiter_ID = old.ID;
			END

	-- mysql-Version
	-- what you write
	delimiter //

	create trigger Mitarbeiter_On_Delete2 after DELETE
		on Mitarbeiter
		for each row
        	BEGIN
			DECLARE  ltr_id INTEGER;
		
        
        	select ID
			from Mitarbeiter
			where Abteilung_id = old.Abteilung_ID
			and Ist_Leiter = 'J'
			into ltr_id;
			
			update Schadensfall
			set Mitarbeiter_ID = ltr_id
			where Mitarbeiter_ID = old.ID;
			
			update Versicherungsvertrag
			set Mitarbeiter_ID = ltr_id
			where Mitarbeiter_ID = old.ID;
			END;//
	delimiter;
	
	
	
	
	-- is not what you get



	CREATE DEFINER = 'root'@'localhost' TRIGGER `Mitarbeiter_On_Delete` BEFORE DELETE ON `mitarbeiter`
	FOR EACH ROW
	BEGIN
		DECLARE ltr_id INTEGER;
 
		 
		 select ID
		 from Mitarbeiter
		 where Abteilung_id = old.Abteilung_ID
		 and Ist_Leiter = 'J'
		 into ltr_id;
		 
		 update Schadensfall
		 set Mitarbeiter_ID = ltr_id
		 where Mitarbeiter_ID = old.ID;
		 
		 update Versicherungsvertrag
		 set Mitarbeiter_ID = ltr_id
		 where Mitarbeiter_ID = old.ID;
		 END;

**Aufgabe**
Ein Trigger soll folgende Aufgabe erledigen:

- Wenn ein neuer Fahrzeugtyp aufgenommen werden soll, ist der Hersteller zu überprüfen.
- Ist der angegebene Hersteller (d. h. die Hersteller_ID) in der Tabelle Fahrzeughersteller gespeichert? Wenn ja, dann ist nichts weiter zu erledigen.
- Wenn nein, dann ist ein Fahrzeughersteller mit dem Namen „unbekannt“ zu suchen.
- Wenn dieser vorhanden ist, ist dessen ID als Hersteller_ID zu übernehmen.
- Andernfalls ist er mit der angegebenen Hersteller_ID neu als „unbekannt“ zu registrieren.

	
**Lösung**

.. code-block:: sql

	1. CREATE Fahrzeugtyp_Check_Hersteller as trigger
	2. to Fahrzeugtyp
	3. for INSERT
	4. AS
	5. DECLARE VARIABLE INTEGER fh_id
	6. BEGIN
	7. -- Initialisierung
	8. fh_id = null;
	9. -- prüfen, ob ID vorhanden ist
	10. select ID
	11. from Fahrzeughersteller
	12. where ID = new.Hersteller_ID
	13. -- wenn sie nicht gefunden wurde
	14. if (fh_id is null) THEN
	15. -- suche stattdessen den Hersteller ’unbekannt’
	16. select ID
	17. from Fahrzeughersteller
	18. where Name = ’unbekannt’
	19. -- wenn auch dazu die ID nicht gefunden wurde
	20. if (fh_id is null) THEN
	21. fh_id = new.Hersteller_ID;
	22. insert into Fahrzeughersteller
	23. values ( :fh_id, ’unbekannt’, ’unbekannt’ )
	24. END
	25. END
	26. new.Hersteller_ID = :fh_id
	27. END
