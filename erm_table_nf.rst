ERM zu Tabellen
===============

.. only:: html

		.. sidebar:: Videos

			 - :download:`ERM-Relational <figure/database/13.13_Umsetzung_ER_Relational_Grundlagen.mp4>`.
			 - :download:`Verfeinerung <figure/database/13.14_Verfeinerung_Relational_Zusammenfassen.mp4>`.


Das ERM diente als Annäherung an das zu programmierende Datenmodell; es muss nun durch weitere Schritte verfeinert werden. Das ERM wird in Tabellen aufgelöst, die Attribute des Entitäten werden zu Spalten der Tabellen.

Als Ausgangssituation dient folgendes ERM:

.. image:: figure/loesung_firma.jpg
    :width: 600 px


.. raw:: latex

         \newpage

Tabellenmodell
--------------

In einem ersten Schritt müssen die Entitäten und ihre Beziehungen in ein Tabellenmodell umgewandelt werden. Dies erfolgt unter Zugrundelegen folgender Regeln.


Auflösung der zusammengesetzten Attribute in Einzelattribute
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. admonition:: Lösung

    .. image:: figure/firma_zusgesAttribut.png
        :width: 400 px

    .. image:: figure/Notizblock.jpg
        :width: 200 px



.. raw:: latex

         \newpage


Auflösung der Mehrfachattribute
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Mehrfachattribute müssen zu eigenen Entitäten und einer 1:n-Beziehung umgesetzt werden.

.. admonition:: Lösung

    .. image:: figure/firma_mehrfach.png

    .. image:: figure/Notizblock.jpg
        :width: 400 px


.. raw:: latex

	\newpage


.. raw:: latex

         \newpage


Umwandeln der Entitäten zu Tabellen
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


Jede Entität wird zu einer eigenen Tabelle. Die eindeutigen Attribute sind Schlüsselkandidaten und können als Primärschlüssel verwendet werden. Eigene künstliche Schlüssel sind ebenfalls möglich und häufig vorzuziehen.

.. admonition:: Lösung

    .. image:: figure/relation_angestellter.png

    .. image:: figure/Notizblock.jpg
        :width: 400 px



.. raw:: latex

         \newpage


Auflösen der 1:n-Beziehung
^^^^^^^^^^^^^^^^^^^^^^^^^^

Bei Beziehungen zwischen Entitäten muss sichergestellt sein, dass die beteiligten Ausgangsentitäten nach der Umwandlung noch über das Wissen der jeweils anderen Seite verfügen. Dies erfolgt über das Einfügen von weiteren Spalten in der Tabelle, den. sog. Fremdschlüsseln (Foreign key).


Um die Forderung nach Redundanzfreiheit, Atomarität, etc. nicht zu verletzen macht es allerdings nur Sinn, dass die n-Seite das neue Attribut aufnimmt. Es wird zu einem Fremdschlüssel und verweist dazu auf den Primärschlüssel der 1-Seite


.. admonition:: Lösung

    .. image:: figure/ERM_Kard_1_N_ERM.png

    .. image:: figure/Notizblock.jpg
        :width: 400 px


.. raw:: latex

	\newpage



Auflösen der n:m-Beziehung
^^^^^^^^^^^^^^^^^^^^^^^^^^

Da bei der n:m-Beziehung alle Entitäten mehrere Beziehungen zu anderen Entitäten aufweisen können, ist es nicht möglich, innerhalb einer bestehenden Entität durch Fremdschlüsselattribute eine Beziehung herzustellen, da es evtl. einer unbekannten Zahl von Attributen bedürfen könnte.

Deshalb wird eine eigene Zwischentabelle gebildet, deren Primärschlüssel sich aus den Primärschlüsseln der beteiligten Entitäten bildet.


.. admonition:: Lösung

    .. image:: figure/N_M_aufloesen.png

    .. image:: figure/Notizblock.jpg
        :width: 400 px


.. raw:: latex

	\newpage



Auflösen der 1:1-Beziehung
^^^^^^^^^^^^^^^^^^^^^^^^^^

Hier hält jede Entität maximal eine Beziehung zu einer anderen Entität. Deshalb kann eine der beiden Entitäten einen Fremdschlüssel zur anderen Seite führen. Auf die Anzahl möglicher Null-Werte ist dabei zu achten. So hat z.B. **jede Abteilung einen Abteilungsleiter, aber nicht jeder Angestellte ist Abteilungsleiter**. Somit ist es besser, die Information in der Tabelle Abteilung zu führen.

.. admonition:: Lösung

 	.. image:: figure/ERM_Kard_1_1_ERM.png


 	.. image:: figure/Notizblock.jpg
        :width: 200px



.. raw:: latex

	\newpage


Fazit
^^^^^

Als Ergebnis der Umwandlung ist dann folgendes Tabellenmodell entstanden.


.. image:: figure/ERM_RELATION_Firma.png


.. raw:: latex

         \newpage


Aufgaben
^^^^^^^^

.. admonition:: Aufgabe

	Wandeln Sie die ER-Modelle aus dem vorhergehenden Kapitel in Tabellenmodelle um.

	.. image:: figure/arbeitsblatt/loesung_bus.png

	.. image:: figure/arbeitsblatt/loesung_olympia.png

.. raw:: latex

         \newpage



.. index:: Normalform, Normalisierung


Normalformen
------------

.. only:: html

	.. sidebar:: Ziele

		 – Wiederholungsgruppen und Atomarität
		 – Die ersten drei Normalformen
		 – Unterschied zwischen teilfunktionalen und transitiven Informationen
		 – Erkennen, wann Normalformen sinnvoll sind und wann nicht


Wenn Datenbanken über einen längeren Zeitraum verändert und angepasst werden, kann man erkennen, dass die Konsistenz der Daten, d.h. die "Stimmigkeit der Daten" abnimmt. Ebenso kommt es oft vor, dass Daten redundant gehalten und somit an vielen Stellen gepflegt werden müssen.

Dies versucht man durch das Konzept der **Normalisierung** zu verhindern. Sie soll

- redundante Daten
- Anomalien unter den Daten (Einfüge-, Änderungs-, Löschanomalie)

verhindern.


Normalform 1
^^^^^^^^^^^^

.. sidebar:: 1. NF

	- keine Wiederholungsgruppen
	- Atomarität

Die 1. NF versucht, das Problem von Wiederholungsgruppen zu verhindern und fordert die Atomarität von Spaltenwerten. Folgendes Beispiel soll dies verdeutlichen.

In einer Tabelle soll der Warenkorb eines Shop-Besuchs abgelegt werden. Jeder Warenkorb wird durch seinen Primärschlüssel identifiziert. Wir gehen hier davon aus, dass der Warenkorb nur von angemeldeten Kunden gefüllt wird. Im Wesentlichen wird hier die Information gespeichert, welcher Artikel wie oft im Warenkorb abgelegt wurde. Das Ergebnis erster Überlegungen sieht wie folgt aus.

.. image:: figure/normalform_1_ausgangslage.png
    :width: 700 px

.. admonition:: Frage

	Betrachten Sie die Inhalte der Tabelle **warenkorb** und diskutieren Sie die möglichen Nachteile. Gibt es auch Vorteile?


.. image:: figure/Notizblock_hell.png
       :width: 300px





Die Werte der Spalte artikel in der Tabelle warenkorb sind im Grunde Listen, und die Listenelemente bestehen aus zwei Informationen: Artikelnummer und Anzahl. Dies sind zu vermeidende Wiederholgunsgruppen:  Die Listen sollen aufgelöst und die Teilinformationen in jeweils eigene Spalten überführt werden.


Um die Wiederholungsgruppenfreiheit herzustellen, brauchen wir eine neue Tabelle, die ähnlich der Tabelle position die Artikel aufnimmt

.. image:: figure/normalform_1_kwdhg.png
     :width: 800 px


.. admonition:: Frage

	Warum ist die Spalte artikel in der Tabelle **korbposition** keine Wiederholungsgruppe ?

.. image:: figure/Notizblock_hell.png
       :width: 300px

Was danach noch störend ins Auge fällt, ist die Spalte artikel in der Tabelle korbposition. Die Artikelnummer und die Anzahl sind in der gleichen Spalte abgelegt, was eine Auswertung und Veränderung der Daten erheblich erschwert.
Die Spalte ist somit nicht atomar und sie kann noch weiter zerlegt werden.
Zuletzt haben die Tabellen folgenden Aufbau.


.. image:: figure/normalform_1_ende.png
     :width: 800 px



.. important:: **Erste Normalform**

	Eine Tabelle ist dann in der 1. Normalform, wenn sie atomar und wiederholungsgruppenfrei ist. Eine Datenbank ist dann in der 1. Normalform, wenn alle Tabellen in der 1. Normalform sind.



.. raw:: latex

         \newpage


Normalform 2
^^^^^^^^^^^^

.. sidebar::  2. NF

	Volle funktionale Abhängigkeit der Nichtschlüsselattribute

Wir betrachten eine Tabelle tblPositionen, die die Bestellpositionen eines Auftrags zusammenfasst. Ein Auftrag kann mehrere Bestellpositionen besitzen; pro Bestellposition wird ein Artikel definiert.  Der Primärschlüssel ist zusammengesetzt aus den Spalten AuftragNr und TeileNr.


.. image:: figure/normalform_2_ausgangslage.png


Die 2. NF fordert, dass die Werte alle Nichtschlüsselspalten voll funktional vom Gesamtschlüssel abhängig sind. Dies ist in der obigen Tabelle nicht immer der Fall, da der Artikelname lediglich abhängig von der TeileNr ist. Diese Spalten sind aus der Tabelle herauszulösen und in eigenen Tabellen zu speichern.


.. image:: figure/Notizblock_hell.png
       :width: 300px



::

	TblPositionen                 |    tblArtikel
	A_Nr      T_Nr     A_Menge    |    T_Nr     T_Bezeichnung     P_Preis
	99-8419   12088     3         |    12088    P400 komplett      999,00
	99-8420   11000     2         |    11000    PC-Tisch           399,00
	99-8420   11002     1         |    11002    Monitor 19"        599,00
	99-8420   12089    10         |    12089    P500 komplett     1199,00
	99-8421   12089     5         |    12089    P500 komplett     1199,00
	99-8421   23044     2         |    23044    Tischfax           489,00
	99-8422   33002    25         |    33002    Toner HP           189,00
	99-8422   11000    10
	99-8422   11002     5


.. raw:: latex

         \newpage


Normalform 3
^^^^^^^^^^^^

.. sidebar::  3. NF

	Keine transitive Abhängigkeit zwischen Nichtschlüsselspalten

.. admonition:: Frage

	Ermitteln Sie in untenstehender Grafik den  Zusammenhang zwischen Bankleitzahl (blz) und dem Banknamen sowie ebenso zwischen der Bankleitzahl, der Kontonummer und der IBAN.



.. image:: figure/normalform_3_ausgangslage.png
     :width: 800 px

Wenn Nichtschlüsselspalten aus anderen Nichtschlüsselspalten herleitbar sind, bedeutet dies in der Regel, dass Informationen redundant in der Tabelle gehalten werden. Hier werden beispielsweise die Banknamen mehrfach genannt. Dies verbraucht nicht nur Speicherplatz, sondern macht eine Änderung der Banknamen teuer, da diese in vielen Zeilen durchgeführt werden müssen.


.. image:: figure/normalform_3_ende.png
     :width: 800 px



Transitive Informationen begegnen Ihnen relativ oft. Der Kontoinhaber ergibt sich aus der Kontonummer, der Ortsname aus der Postleitzahl, der Rabatt aus der Kundenart etc.



.. raw:: latex

         \newpage


Aufgabe:
--------

- Finden Sie Beispiele für Einfüge-, Änderungs- und Löschanomalien
- Erläutern Sie den Begriff Wiederholungsgruppe.
- Was bedeutet die Forderung nach Atomarität.
- Diskutieren Sie die Thematik Zusammengesetzter Schlüssel vs . Künstlicher Schlüssel


.. TODO:: Weitere Aufgaben zu Normalformen schreiben




.. raw:: latex

         \newpage


.. index:: Referentielle Integrität, Anomalie

Referentielle Integrität
------------------------

Im Bereich der relationalen Datenbanken wird die referentielle Integrität dazu verwendet die Konsistenz und die Integrität der Daten sicherzustellen. Dazu werden Regeln aufgestellt, wie und unter welchen Bedingungen ein Datensatz in die Datenbank eingetragen wird.

Bei der referentiellen Integrität können Datensätze die einen Fremdschlüssel aufweisen nur dann gespeichert werden, wenn der Wert des Fremdschlüssels einmalig in der referenzierten Tabelle existiert. Im Falle, dass ein referenzierter Wert nicht vorhanden ist, kann der Datensatz nicht gespeichert werden.

**Warum wird die Referentielle Integrität benötigt?**

Eine Datenbank kann schnell in einen inkonsistenten Zustand geraten. Im ungünstigsten Fall liegt eine nicht-normalisierte Datenbank vor, die starke Redundanzen aufweist. Dabei können Anomalien im Datenbestand auftreten, die verschiedene Formen annehmen. Man spricht hier von Einfüge-, Lösch- und Änderungsanomalien. Tritt eine oder mehrerer dieser Anomalien auf, kann das zur Verfälschung oder Löschung von Informationen führen.

.. index:: Einfügeanomalie

**Einfüge-Anomalie**

Eine Einfüge-Anomalie tritt auf, wenn ein Datensatz gespeichert werden soll und dieser keine oder keine eindeutigen Primärschlüsselwerte aufweist. Das Einfügen in eine Tabelle ist somit nicht möglich. Informationen können nicht gespeichert werden und gehen womöglich verloren. Das kann zum Beispiel der Fall sein, wenn für die Speicherung der Kundendaten zu Verifizierungszwecken die Personalausweisnummer als Primärschlüssel verwendet wird, diese aber leider vom Sachbearbeiter nicht erfasst werden konnte. Der Datensatz des Kunden kann nicht gespeichert werden.

.. index:: Änderungsanomalie

**Änderungs-Anomalie**

Man spricht von einer Änderungs-Anomalie, wenn eine Entität redundant in einer oder sogar in mehreren Tabellen enthalten ist und bei einer Aktualisierung nicht alle berücksichtigt werden. Dadurch kommt es zur Inkonsistenz im Datenbestand. Es kann möglicherweise nicht mehr nachvollzogen werden welcher Wert der gültige ist. Dieser Sachverhalt lässt sich gut an einer Auftragstabelle darstellen. Diese speichert neben der Auftragsnummer auch den Namen eines Kunden und dessen Bestellung. Ein Kunde kann mehrere Bestellungen aufgegeben haben, wobei jede Bestellung in einem Datensatz erfasst wird. Wird nun aufgrund eines Schreibfehlers nachträglich der Name des Kunden „Reiher“ in „Reier“ bei einem Datensatz geändert, führt dies zu einem inkonsistenten Datenbestand. Nach der Änderung liegen demnach Aufträge für scheinbar zwei verschiedene Kunden vor und zwar für einen Kunden „Reiher“ und einen Kunden „Reier“.


.. index:: Löschanomalie

**Lösch-Anomalie**

Enthalten die Datensätze einer Tabelle mehrere unabhängige Informationen, so kann es leicht zu Lösch-Anomalien kommen. Da sich die Daten in einem nicht-normalisierten Zustand befinden, kann durch Löschen eines Datensatzes ein Informationsverlust entstehen. Die Ursache liegt darin, dass in einer Tabelle unterschiedliche Sachverhalte gespeichert werden. Am Beispiel einer nicht-normalisierten Mitarbeitertabelle soll dies kurz skizziert werden. In der Mitarbeitertabelle werden die Personalnummer, der Name und die Abteilung gespeichert. Der Mitarbeiter „Krause“, der als einziger in der Abteilung „Lager“ war, ist aus dem Unternehmen ausgetreten und wird daher aus der Datenbank gelöscht. Da die Abteilung in der gleichen Tabelle gespeichert wird, verschwindet das „Lager“ aus der Datenbank, da „Herr Krause“ ja als einziger dieser Abteilung zugeordnet war.


**Datenbank-Anomalien auflösen**

Die beschriebenen Anomalien treten durch ein schlechtes Datenbank-Design auf. Daraus ergibt sich auch die redundante Datenhaltung. Um diese zu vermeiden, müssen die Tabellen einer Datenbank normalisiert werden. Die Normalisierung umfasst in der Praxis drei Stufen und sorgt für eine redundanzfreie und nach Entitäts-Typ getrennte Datenhaltung.


.. index:: Löschweitergabe, Änderungsweitergabe

**Weitergabe**

Während die RI grundsätzlich vor inkonsistenten Datenaktionen schützt, bieten viele Datenbanksysteme Zusatzfunktionen an, die bei Updates von Master-Datensätzen nützlich sein können:

Änderungsweitergabe (ÄW)

    Wenn der eindeutige Schlüssel eines Datensatzes geändert wird, kann das DBMS die Fremdschlüssel in allen abhängigen Datensätzen anpassen – anstatt die Änderung abzulehnen. Änderungsweitergabe wird insbesondere dann benutzt, wenn natürliche Schlüssel (die sich ändern können; Familienname bei Heirat) verwendet werden; denn künstliche Schlüssel sind i. d. R. unveränderlich und eine Änderungsweitergabe nicht erforderlich.


Löschweitergabe (LW)

    In bestimmten Fällen ergibt es einen Sinn, abhängige Datensätze bei Löschung des Masterdatensatzes mitzulöschen.

Diese Funktionen können in der RI-Spezifikation optional gesetzt und (je nach DBMS) durch zusätzliche Bedingungen  erweitert/präzisiert werden.


.. code-block:: sql

	create table ...
	    .... ,
	    foreign key (id) references tabelle.pk
	    	on delete cascade 
	    	on update restrict
