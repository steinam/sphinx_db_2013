// Drop down box for the Special character menu in [[MediaWiki:Edittools]]
// will be called from [[MediaWiki:Common.js]].
// Adapted from:
// http://commons.wikimedia.org/wiki/MediaWiki:Edittools.js

function addCharSubsetMenu() {
  var specialchars = document.getElementById('specialchars');

  if (specialchars) {
    var menu = document.createElement('select');
    menu.id = 'specialcharsselect';
    menu.style.cssFloat = 'left';
    addHandler(menu, 'change', chooseCharSubset);
    menu.setAttribute('accesskey', 'e');
    var paras = specialchars.getElementsByTagName('p');
    for (var i = 0; i < paras.length; i++)
      if (paras[i].className == 'specialbasic')
        menu.options.add(new Option(paras[i].id.replace(/&/g, '&'+'amp;').replace(/</g, '&'+'lt;')));
    specialchars.insertBefore(menu, specialchars.firstChild);
    specialchars.appendChild(document.createElement('div')).className = 'visualClear';
 
    // Standard-CharSubset
    var pos = 0;
    if (document.cookie) {
      var results = document.cookie.match('(^|;) ?charinsert=([^;]*)(;|$)');
      if (results)
        pos = parseInt(results[2]);
    }
    menu.selectedIndex = pos;
    chooseCharSubset();
  }
}
 
// CharSubset selection
function chooseCharSubset() {
  s = document.getElementById('specialcharsselect').selectedIndex;
  var l = document.getElementById('specialchars').getElementsByTagName('p');
  for (var i = 0; i < l.length ; i++) {
    l[i].style.display = i == s ? 'inline' : 'none';
  }
  document.cookie = 'charinsert=' + s + ';';
}
 
// Menu insertion
addOnloadHook(addCharSubsetMenu);