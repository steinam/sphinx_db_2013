Transaktionen
=============

- Datenbanken müssen für den gleichzeitigen Zugriff mehrerer Benutzer ausgelegt sein. Daraus können sich mannigfaltige Probleme ergeben. DBMS versuchen mit dem Prinzip der Transaktion diese Probleme in den Griff zu bekommen.

- Transaktionen führen einen konsistenten Datenbankzustand in einen anderen konsistenten Datenbankzustand über.

- Eine Transaktion ist als eine Folge von Anweisungen zu sehen, die entweder komplett übernommen (committ) oder abgebrochen (rollback) werden.


Probleme von Multi-User-Datenbanken
-----------------------------------
.. index:: Lost Update

- **Verlorengegangene Änderungen**

   2 User ändern zur gleichen Zeit einen einzelnen Datensatz (Lost
   Updates).

   ::

       Zeit            Transaktion 1          Transaktion 2
       1                  read(x)
       2                                      read(x)
       3                                      x = x + 100
       4                                      write(x)
       5                  x = x + 1
       6                  write(x)
           
.. index:: DirtyRead
       
-  Schmutziges Lesen

   Ein User sieht Änderungen,die noch nicht von ihm selbst oder von
   anderen Usern committed oder rollbacked wurden. (Dirty Reads)

   ::

       Zeit            Transaktion 1          Transaktion 2
       1                  read(x)
       2                  x = x + 100
       3                  write(x)
       4                                      read(x)
       5                                      x = x - 100
       6                  ABORT
       7                                      write(x)
           
.. index:: Non-Repeatable Reads

-  Nicht wiederholbare Lesezugriffe

   User wählen wiederholt Zeilen aus, die andere User ändern oder
   löschen. Ob dies ein Problem darstellt hängt von den jeweiligen
   Umständen ab (Inventur vs. Reisebüro) ====> Unrepeatable Reads.

   ::

            
       x =40 y =50 z = 30
       Zeit            Transaktion 1          Transaktion 2
       1               sum = 0
       2               read( x )
       3               read( y )
       4               sum = sum + x
       5               sum = sum + y
       6                                       read( z )
       7                                       z = z 10
       8                                       write( z )
       9                                       read( x )
       10                                      x = x + 10
       11                                      write( x )
       12              read( z )
       13              sum = sum + z
          
       
.. index:: Phantom-Rows 
  
-  Phantom-Zeilen

   Ein User kann einige, aber nicht alle neuen Datensätze lesen, die
   ein anderer User eingegeben hat (Phantom rows).

   ::

           
       Zeit            Transaktion 1          Transaktion 2
       1               Select Counter
                       from PassCounter
       2                                       Update Passengers
                                               set Flight = 4711
                                               where Name = Ã¢â‚¬â„¢PhantomÃ¢â‚¬â„¢
       3                                       Update PassCounter
                                               set Counter = Counter + 1
       4               select *
                       from Passengers
           

-  Inkonsistenzen (Interleaved Transactions)

   Sie können auftreten, wenn Tabellen miteinander über Spaltenwerte
   verbunden sind (Fremdschlüssel), und diese Spalten nicht durch
   Trigger oder Constraints geschützt sind. ÃƒÅ“blicherweise handelt es
   sich um ein timing-Problem, wenn nicht vorhergesagt werden kann, in
   welcher Reihenfolge oder in welcher Häufigkeit auf die Tabellen
   zugegriffen wird.

   ::

           Integritätsbedingung x = y
       Zeit            Transaktion 1          Transaktion 2
                       Inkrementieren T1      Verdoppeln T2
       1               read( x )
       2               x = x + 1
       3               write( x )
       4                                      read( x )
       5                                      x = 2 * x
       6                                      write( x )
       7                                      read( y )
       8                                      y = 2 * y
       9                                      write( y )
       10              read( y )
       11              y = y + 1
       12              write( y )
           


Eigenschaften einer Transaktion
-------------------------------

.. index:: ACID

Transaktionen werden durch die sog. ACID - Eigenschaften beschrieben:


-  Atomarität

   Eine Transaktion wird entweder ganz oder gar nicht ausgeführt

-  Konsistenz (Serialisierbarkeit)

   Transaktionen überführen die Datenbank von einem konsistenten
   Zustand in einen anderen konsistenten Zustand. Dies wird durch das
   Prinzip der Serialisierung erreicht.

-  Isolation

   Nebenläufige (gleichzeitige) Transaktionen laufen jede für sich so
   ab, als ob sie alleine ablaufen würden. Verschiedene
   Isolationslevel sind möglich

-  Dauerhaftigkeit

   Die Wirkung einer abgeschlossenen (Dauerhaftigkeit) Transaktionen
   bleibt (auch nach einem Systemausfall) erhalten. Dies wird durch
   spezielle Recovery-Mechanismen erreicht


   .. figure:: figure/trans_acid.png
   
   .. figure:: figure/trans_acid1.png


Serialisierung als Garantie der Transaktion
-------------------------------------------


Serialisierung bedeutet, dass geprüft wird, ob gleichzeitig
stattfindende Transaktionen sich gegenseitig behindern würden.


-  Strenge Serialisierbarkeit - pessimistisch

   Die einzelnen Transaktionen der User werden nur streng
   hintereinander ausgeführt

   
   -  Keine Überlappung der einzelnen Transaktionen.

   -  Geschwindigkeitsnachteile.


-  Weiche Serialisierung - optimistisch

   Erst beim Abschluss¸ einer Transaktion wird geprüft, ob sie sich
   serialisieren lässt. Sie kann deshalb abgebrochen werden und muss
   von vorne beginnen. Sie durchlaufen normalerweise drei Phasen:

   
   -  Lesephase (Daten lesen und Berechnungen durchführen)
   -  Validierungsphase (überprüft Einhaltung der
      Konsistenzkriterien)
   -  Schreibphase (die in der Lesephase berechneten Änderungen werden
      eingetragen)


   Optimistische Serialisierung lässt alle Reihenfolgen zu, die keinen
   Schaden anrichten können.

   Beispiel: überlappend, aber unschädlich
   ::

       Zeit            Transaktion 1          Transaktion 2
       -----------------------------------------------------
       1               read(x) 1
       2                                      read(y) 2
       3               x = x + 100 3
       4                                      y = y - 100 4
       5               write(x) 5
       6                                      write(y) 6
           

   Beispiel: überlappend und schädlich, nicht serialisierbar

   ::

       Zeit            Transaktion 1          Transaktion 2
       -------------------------------------------------------
       1               read(x) 1
       2                                      read(x) 2
       3                                      x = x + 100
       4                                      write(x) 4
       5               x = x + 1
       6               write(x)
           


Zusammenfassung Serialisierung
------------------------------

Pessimistische Verfahren:


-  frühzeitige Erkennung der Konflikte
-  mit Arbeitsverlust verbundene Abbrüche der Transaktionen nur bei Deadlock
-  Zur Vermeidung von SchneeballEffekten notwendige Striktheit vermindert die mögliche Nebenläufigkeit


Optimistische Verfahren:

-  NurLeseTransaktionen brauchen sich nicht um Synchronisation zu kümmern
-  Keine DeadlockGefahr
-  Beim Konflikttest können die tatsächlichen Ergebnisse der Operationen der Transaktionen berücksichtigt werden
-  Änderungen müssen bis zum Ende der Transaktion in einem lokalen Speicher gehalten werden

-  Ein großer Teil des Synchronisierungsaufwandes fällt gesammelt am Schluss an


Isolation-Level
---------------

Im gleichen Zusammenhang wie der Begriff der Serialisierung ist der Begriff des Isolation-Level zu sehen. Je nach Grad des Isolation-Level können damit verschiedene Probleme desMulti-User-Zugriffes gelöst werden.

::
	

	+----------------+------------------------------------------+
	| Isolationsgrad | Effekte                                  |
	+================|==========================================+
	| 0              | Dirty Read, Non-Repeatable-Read, Phantom |
	+----------------+------------------------------------------+
	| 1              | Non-Repeatable-Read, Phantom             |
	+----------------+------------------------------------------+
	| 2              | Phantom                                  |
	+----------------+------------------------------------------+
	| 3              |  -                                       |
	+----------------+------------------------------------------+


Standard ist der Isolation-Level 3. Geringere Level sind manchmal aus Performancegründen sinnvoll. Die Umsetzung der Serialisierung bzw. des Isolation-Level erfolgt normalerweise pessimistisch, und zwar mit Hilfe des **2- Phasen-Sperrprotokolls**.

Siehe dazu auch
`folgenden link <http://incubator.apache.org/derby/manuals/develop/develop71.html>`_


Beispiel zu Transaktionen in MySQL
----------------------------------

.. image:: figure/Transaction.jpg


Das folgende Beispiel geht von der Datenbank **test_transaktion** aus. Diese besitzt eine Tabelle **Studio**.

.. code-block:: sql

	--------------------------------------------------------
	--
	-- Tabellenstruktur für Tabelle `Studio`
	--

	create datasbase test_transaktion;
	use test_transaktion:
	
	CREATE TABLE IF NOT EXISTS `Studio` (
		`studio_id` int(11) NOT NULL,
		`studio_name` varchar(30) DEFAULT NULL,
		PRIMARY KEY (`studio_id`)
	) ENGINE=InnoDB DEFAULT CHARSET=latin1;


	
	
- Wir beginnen mit demn Einfügen von 2 Datensätzen

	.. code-block:: sql
	
		START TRANSACTION;
		INSERT INTO Studio VALUES (101, 'MGM Studios');
		INSERT INTO Studio VALUES (102, 'Wannabe Studios');
		COMMIT;
		SELECT * FROM Studio;
		
- Ein Rollback würde zuvor eingefügte Datensätze wieder entfernen:

	.. code-block:: sql

		START TRANSACTION;
		UPDATE Studio SET studio_name = 'Temporary Studios' WHERE studio_id = 101;
		UPDATE Studio SET studio_name = 'Studio with no buildings'  WHERE studio_id = 102;
		SELECT * FROM Studio;
		ROLLBACK;
		SELECT * FROM Studio;


	
	
- SavePoints sind Haltepunkte innerhalb einer Transaktion, die dazu führen, dass auch bei einem Rollbakc bestimmte Anweisungen innerhalb einer Transaktion bestehen bleiben.

.. image:: figure/Savepoint.jpg

   .. code-block:: sql
	
		START TRANSACTION;
		INSERT INTO Studio VALUES (103, 'Hell\'s Angels Horror Shows');
		INSERT INTO Studio VALUES (104, 'Black Dog Entertainment');
		SAVEPOINT savepoint1;
		SELECT * FROM Studio;
		
  Wenn nun weitere Datensätze innerhalb der Transaktion eingefügt werden, dann könnte man durch ein **Rollback to Savepoint name** zumindest Teile der Transaktion sichern.
  
  .. code-block:: sql
		
  	INSERT INTO Studio VALUES (105, 'Noncomformant Studios');
  	INSERT INTO Studio VALUES (106, 'Studio Cartel');
  	SELECT * FROM Studio;

  	ROLLBACK TO SAVEPOINT savepoint1;

  	INSERT INTO Studio VALUES (105, 'Moneymaking Studios');
  	INSERT INTO Studio VALUES (106, 'Studio Mob');
  	SELECT * FROM Studio;

  	COMMIT;
  
	
		
DirtyReads in MySQL
----------------------------------	
	
Das folgende Beispiel geht von der Datenbank **sakila** aus. Es werden zum Nachvollziehen zwei unterschiedliche Sessions auf die Datenbank benötigt.



.. code-block:: sql

	use sakila;
	
	-- Session 1
	START TRANSACTION;
	-- wir ändern die Kategory des Horror film "ZHIVAGO CORE" zu "Children"
	-- natürlich völlig unabsichtlich :-)
	UPDATE film_category SET category_id = 3 WHERE film_id = 998;
	
	
	
	-- session 2
	-- session 2 holt sich jetzt eine Liste aller Kinderfilme
	-- wichtig ist dass der T-Level auf READ UNCOMMITTED steht
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SELECT * FROM film f, film_category cat
	WHERE f.film_id = cat.film_id
	AND cat.category_id = 3;
	
	
	-- session 1
	-- wir bemerken den Fehler und machen ein Rollback in session 1
	ROLLBACK;
	SELECT * FROM film WHERE film_id = 998;
	
.. image:: figure/DirtyRead.jpg


PhantomRows in MySQL
---------------------


Sie brauchen 3 verschiedene Sessions zu einem DB-Server

.. code-block:: sql

	--session 1
	-- wir fügen 2 Datensätze hinzu
	START TRANSACTION;
	SELECT * FROM Studio;
	INSERT INTO Studio VALUES (105, 'Noncomformant Studios');
	INSERT INTO Studio VALUES (106, 'Studio Cartel');
	SELECT * FROM Studio;
	
	-- session2
	-- sieht die neu eingefügten Datensätze 
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SELECT * FROM studio;
	
	
	--session 1
	-- Rücknahme der Inserts
	ROLLBACK;
	SELECT * FROM studio;
	
	
	--session3
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SELECT * FROM studio;
	

	
	
	
Zusatzbeispiel IsolationLevel
-----------------------------

Die Isolationsebene wird mit dem Befehl 

.. code-block:: sql

	SET SESSION TRANSACTION ISOLATION LEVEL isolationsebene;

definiert. Zur Zeit sind die folgenden 4 Ebenen spezifiziert: 	 READ UNCOMMITTED, READ
COMMITTED, REPEATABLE READ und SERIALIZABLE.

READ UNCOMMITTED
^^^^^^^^^^^^^^^^^

Es handelt sich eigentlich nicht um eine Isolationsebene, da die anderen Transaktionen
oder Sessions den Zwischenstand lesen können. Da dabei auch inkonsistente Zustände
auftreten können, wird dieser auch dirty read genannt.

::
	
    SESSION 1                               SESSION 2
    1 SET AUTOCOMMIT = 0;                   1 SET SESSION TRANSACTION ISOLATION LEVEL
    2                                       2 READ UNCOMMITTED;
    3                                       3
    4 SELECT artikel_id , menge_aktuell     4 SELECT artikel_id , menge_aktuell
    5 FROM lagerbestand                     5 FROM lagerbestand
    6 WHERE artikel_id = 9015;              6 WHERE artikel_id = 9015;
    7 + -----------+--------------+         7 + ----------- + ------------- +
    8 | artikel_id | menge_aktuell|         8 | artikel_id  | menge_aktuell |
    9 + -----------+--------------+         9 + ----------- + ------------- +
   10 | 9015       | 16.000000    |        10 | 9015        | 16.000000     |
   11 + ---------- + ------------ +        11 + ----------- + ------------- +
   12                                      12
   13 UPDATE lagerbestand                  13
   14 SET menge_aktuell = menge_aktuell-1  14
   15 WHERE artikel_id = 9015;             15
   16                                      16
   17 SELECT artikel_id , menge_aktuell    17 SELECT artikel_id , menge_aktuell
   18 FROM lagerbestand                    18 FROM lagerbestand
   19 WHERE artikel_id = 9015;             19 WHERE artikel_id = 9015;
   20 + ---------- + ------------- +       20 + ------------ + ------------ +
   21 | artikel_id | menge_aktuell |       21 | artikel_id   | menge_aktuell|
   22 + ---------- + ------------- +       22 + ------------ + ------------ +
   23 | 9015       | 15.000000     |       23 | 9015         | 15.000000    |
   24 + ------- + ----------+              24 + ------------ + ------------ +
   25                                      25
   26 ROLLBACK;                            26
   27                                      27
   28 SELECT artikel_id , menge_aktuell    28 SELECT artikel_id , menge_aktuell
   29 FROM lagerbestand                    29 FROM lagerbestand
   30 WHERE artikel_id = 9015;             30 WHERE artikel_id = 9015;
   31 + ---------- + ------------- +       31 + ------------ + ------------ +
   32 | artikel_id | menge_aktuell |       32 | artikel_id   | menge_aktuell|
   33 + ---------- + ------------- +       33 + ------------ + ------------ +
   34 | 9015       | 16.000000     |       34 | 9015         | 16.000000    |
   35 + ---------- + ------------- +       35 + ------------ + ------------ +
                                            
	
	
Um dieses Beispiel nachzubauen, sollten Sie zwei Instanzen des MySQL-Clients gleichzeitig vor sich haben. In Session 1 wird in der Zeile 1 der AUTOCOMMIT-Modus so eingestellt, dass er sich automatisch immer in einer Transaktion befindet.
In der gleichen Zeile wird in Session 2 dafür gesorgt, dass diese einen dirty read auf die
Datenbank durchführen kann. Danach wird überprüft, dass auch in beiden Sessions die gleichen Daten vorliegen.

In Session 1 wird ab Zeile 13 der Lagerbestand der Spalten aktualisiert. Bitte beachten Sie,
dass dies in der zweiten Session nicht passiert. In Zeile 17 wird jetzt in beiden Sessions
wieder der Lagerbestand überprüft. Beide geben nun das gleiche Ergebnis aus, obwohl in
Session 1 noch kein COMMIT erfolgt ist. Session 2 arbeitet also mit inkonsistenten Daten.
An diesem Beispiel können Sie auch den Effekt von ROLLBACK beobachten. In Session 1
Zeile 26 wird der ROLLBACK angewiesen. Eine anschließende Überprüfung zeigt, dass in
beiden Sessions wieder der ursprüngliche Zustand hergestellt wurde.



READ COMMITTED
^^^^^^^^^^^^^^

Bei dieser Isolationsebene werden nur die Daten in einer Transaktion berücksichtigt, die
durch andere mit einem COMMIT bestätigt wurden.


::
	
     SESSION 1
     1 SET AUTOCOMMIT = 0;                               1 SET SESSION TRANSACTION ISOLATION LEVEL     
     2                                                   2 READ COMMITTED;
     3                                                   3
     4 SELECT artikel_id , menge_aktuell                 4 SELECT artikel_id , menge_aktuell
     5 FROM lagerbestand                                 5 FROM lagerbestand
     6 WHERE artikel_id = 9015;                          6 WHERE artikel_id = 9015;
     7 + ------------ + ---------------+                 7 + ------------ + --------------- +
     8 | artikel_id   | menge_aktuell  |                 8 | artikel_id   | menge_aktuell   |
     9 + ------------ + -------------- +                 9 + ------------ + --------------- +
    10 | 9015         | 16.000000      |                10 | 9015         | 16.000000       |
    11 + ------------ + -------------- +                11 + ------------ + --------------- +
    12                                                  12
    13 UPDATE lagerbestand                              13
    14 SET menge_aktuell = menge_aktuell - 1            14
    15 WHERE artikel_id = 9015;                         15
    16                                                  16
    17 SELECT artikel_id , menge_aktuell                17 SELECT artikel_id , menge_aktuell
    18 FROM lagerbestand                                18 FROM lagerbestand
    19 WHERE artikel_id = 9015;                         19 WHERE artikel_id = 9015;
    20 + ------------ + --------------- +               20 + ------------ + ------------- +
    21 | artikel_id   | menge_aktuell   |               21 | artikel_id   | menge_aktuell |
    22 + ------------ + --------------- +               22 + ------------ + ------------- +
    23 | 9015         | 15.000000       |               23 | 9015         | 16.000000     |
    24 + ------------ + --------------- +               24 + ------------ + ------------- +
    25                                                  25
    26 COMMIT;                                          26
    26                                                  27
    28 SELECT artikel_id , menge_aktuell                28 SELECT artikel_id , menge_aktuell
    29 FROM lagerbestand                                29 FROM lagerbestand
    30 WHERE artikel_id = 9015;                         30 WHERE artikel_id = 9015;
    31 + ------------ + --------------- +               31 + ------------ + --------------- +
    32 | artikel_id | menge_aktuell |                   32 | artikel_id   | menge_aktuell   |
    33 + ------------ + --------------- +               33 + ------------ + --------------- +
    34 | 9015 | 15.000000 |                             34 | 9015         | 15.000000       |
    35 + ------------ + --------------- +               35 + ------------ + --------------- +
                                                        

    
    
    
    



	
Aufgabe zu Transaktionen
------------------------


- Für das Sperren und Freigeben im Rahmen von Transaktionen sind die Bereiche **Lock-Manager, Scheduler und Recovery-Manager** zuständig. Informieren Sie sich im Internet über deren Aufgaben und Arbeitsweisen.
- Was sind sog. Deadlocks ?
- Was ist bzw. wie funktioniert das pessimistische Serialisieren bzw. das optimistische Serialisieren
- Wie funktioniert das 2-Phasen-Sperrprotokoll ?


