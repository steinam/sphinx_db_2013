Aufbau eines Datenbanksystems
========================================

Ein Datenbanksystem besteht aus einem Datenbankmanagementsystem (DBMS) und den
Datenbanken (DB). Beide Komponenten sind in der Praxis eng miteinander verzahnt, sollten aber gedanklich unterschieden werden.
In Bild auf der nächsten Seite ist der Aufbau eines Datenbanksystems schematisch
dargestellt. Die Datenbanken enthalten die eigentlichen Daten und unmittelbar damit verknüpfte Datenobjekte wie z.B. eine VIEW . Über eine Kommunikationsschnittstelle werden diese Datenobjekte vom DBMS verwaltet. Das
DBMS selbst besteht wiederum aus vielen kleinen Komponenten, die jeweils auf eine Aufgabe spezialisiert sind.

Die Aufgabe der Datenbank ist die logische und physische Verwaltung der Daten und damit eng verbundener Datenobjekte. Alle diese Datenobjekte können vom Programmierer angelegt, geändert und gelöscht werden. Die Änderungen beziehen sich sowohl auf Struktur als auch auf den Inhalt. So können einer Tabelle neue Spalten (z.B. zweiter Vorname bei einer Adresse) als auch neue Zeilen (z.B. eine neue Adresse) hinzugefügt werden.


.. image:: figure/dbmanagementsystem.jpg 



Die Aufgaben eines DBMS bestehen aus:

#. Sprachinterpreter: 
   Herzstück des DBMS ist der Interpreter. Dieser übersetzt die Befehle in einen ausführbaren Code. Die Sprache, die wir hier verwenden werden, ist SQL. Es gibt und gab aber auch andere Sprachen: dBase, VB für MS-Access, OO-SQL, Sequel usw.
  
#. Optimierer: 

   Die Ausführung eines SQL-Befehls kann oft auf verschiedene Art und Weise passieren. Der Optimierer versucht, anhand von Schätzungen und Algorithmen einen Plan für die Ausführung anzulegen, der möglichst schnell abgearbeitet werden kann.
   
#. Sitzungsverwaltung: 

   Wann immer ein Befehl an den Server gesendet werden soll, muss man sich in einer Sitzung (engl. session) befi nden. Dazu muss zuerst eine Sitzung geöffnet werden. Jetzt können beliebig viele SQL-Befehle gesendet und Daten empfangen werden. Zum Schluss wird die Sitzung serverseitig – z.B. durch einen Timeout – oder clientseitig beendet.
   
#. Randbedingungsprüfer: 

   Für Tabellen können Randbedingungen (engl. constraints) 
   formuliert werden, die immer gelten müssen. Würde die Ausführung eines Befehls dazu führen, dass diese Randbedingungen nicht erfüllt sind, wird die Ausführung des Befehls in der Regel verweigert.

#. Datenschutz: 
   
   Durch die Vergabe von Zugriffsrechten kann das Recht auf lesende und schreibende Zugriffe passgenau vergeben werden.
   
#. Datensicherheit: 

   Der Verlust von Daten ist der GAU schlechthin. Das DBMS muss sicherstellen, dass nicht durch Serverabsturz oder Ähnliches Daten verloren gehen.

#. Transaktionsmanagement: 

   Transaktionen ermöglichen parallelen Zugriff und eine Art undo im Fehlerfall. Das zu gewährleisten, erfordert eine Menge Mühe. Die Qualität des Transaktionsmanagements ist oft ein entscheidendes Merkmal eines DMBS.

#. API

   Die Daten werden in der Regel durch eine oder mehrere Anwendungen bearbeitet. Damit die Anwendung auf das DBMS zugreifen kann, braucht es eine Schnittstelle, über die es zu den Daten gelangt. Der MySQL-Server bietet beispielsweise APIs für C, C++, C#/.NET, PHP, Perl, Python und Tcl. Auch stehen APIs für JDBS und ODBC zur Verfügung.

#. Metadaten: 

   Verwaltungsinformationen, Statistiken etc. Eben der ganze Rest.



