SQL - SELECT
==================

.. only:: html

		.. sidebar:: Videos

			 - :download:`select_einfach <figure/database/13.24 SELECT FROM WHERE.mp4>`
			 - :download:`order by <figure/database/13.25 ORDER BY.mp4>`.
			 - :download:`LIKE <figure/database/13.30 LIKE.mp4>`.
			 - :download:`group by <figure/database/13.38 GROUP BY.mp4>`.
			 - :download:`having <figure/database/13.39 HAVING.mp4>`.
			 - :download:`join_where <figure/database/13.31 JOIN mit WHERE, ON, USING, NATURAL.mp4>`.


Eine Datenbank enthält eine Vielzahl von verschiedenen Daten. Abfragen dienen dazu, bestimmte Daten aus der Datenbank auszugeben. Dabei kann die Ergebnismenge entsprechend den Anforderungen eingegrenzt und genauer gesteuert werden. Der SQL-Befehl zum Abfragen von Daten lautet **SELECT** und besteht  aus folgenden Bestandteilen ("Klauseln" ).


::

	SELECT [DISTINCT | ALL]
	<spaltenliste> | *
	FROM <tabellenliste>
	[WHERE <bedingungsliste>]
	[GROUP BY <spaltenliste> ]
	[HAVING <bedingungsliste>]
	[UNION <select-ausdruck>]
	[ORDER BY <spaltenliste> ]
	;

Die Reihenfolge der Klauseln ist fest im SQL-Standard vorgegeben. Klauseln, die in [ ] stehen, sind nicht nötig, sondern können entfallen; der Name des Befehls und die FROM-Angaben sind unbedingt erforderlich, das Semikolon als Standard empfohlen.


.. raw:: latex

         \newpage


SELECT-Anfragen im Überblick
------------------------------------------

Aufbau
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Die SELECT-Anfrage orientiert sich grundsätzlich an folgendem Schema:

::

	===============================================================
  | Reihe |  SQL-Klausel     | Bedeutung                 | notw.  |
  ===============================================================
  |  1    | SELECT   | Auswahl der gewünschten Attribute |  ja    |
  ===============================================================
  |  2    | FROM     | Angabe der Tabelle(n) mit den     |  ja    |
  |       |          | gewünschten Information           |        |
  ===============================================================
  |  3    | WHERE    | Angabe von Bedingungen für die    |  nein  |
  |       |          | gesuchte Information              |        |
  ===============================================================
  |  4    | GROUP BY | Gruppieren der Daten nach         |  nein  |
  |       | (HAVING) | bestimmten Kategorien nein        |        |
  ===============================================================
  |  5    | ORDER BY | Sortierung des Ergebnisses        | nein   |
  ===============================================================


Die Klauseln mit den Schlüsselwörtern WHERE, GROUP BY, HAVING, ORDER BY sind optional. Die Reihenfolge der einzelnen Klauseln ist aber verbindlich.
Wird eine Anfrage an die Datenbank gestellt, so erhält man die gewünschte Information in Form einer Tabelle, der Ergebnistabelle, zurück.
Das untenstehende Syntaxdiagramm zeigt alle Möglichkeiten einer SQL-Anfrage.

.. figure:: figure/sql_select_syntaxdiagramm.jpg

Bemerkung:

Im Rahmen dieses Kapitels werden nur die wichtigsten - aber in der Regel ausreichenden - Möglichkeiten hinsichtlich der SQL-Anfragen besprochen. Die Syntaxdiagramme dagegen zeigen alle Möglichkeiten auf.


.. raw:: latex

         \newpage


Einfache SQL-Anfragen
---------------------

SELECT - FROM - Anfrage
^^^^^^^^^^^^^^^^^^^^^^^

Die einfachste Form einer SQL-Anfrage besteht aus der **SELECT** - und der **FROM** - Klausel.

.. admonition::  Aufgabe

	Gesucht wird der Inhalt der Tabelle der Fahrzeughersteller mit all ihren Spalten und Datensätzen (Zeilen).

	.. code-block:: sql

	 	SELECT * FROM Fahrzeughersteller;

Schauen wir uns das Beispiel etwas genauer an:
- Die beiden Begriffe SELECT und FROM sind SQL-speziﬁsche Bezeichner.
- Fahrzeughersteller ist der Name der Tabelle, aus der die Daten selektiert und ausgegeben werden sollen.
- Das Sternchen, Asterisk genannt, ist eine Kurzfassung für "alle Spalten".


Eingrenzen der Spalten
^^^^^^^^^^^^^^^^^^^^^^

Nun wollen wir nur bestimmte Spalten ausgeben, nämlich eine Liste aller Fahr-
zeughersteller; das Land interessiert uns dabei nicht. Dazu müssen zwischen
den SQL-Bezeichnern SELECT und FROM die auszugebenden Spalten angegeben
werden. Sind es mehrere, dann werden diese durch jeweils ein Komma getrennt.

.. admonition:: Aufgabe

	Ermitteln Sie die Namensliste aller Fahrzeughersteller !

	.. code-block:: sql

		SELECT Name FROM Fahrzeughersteller;

Folgendes Beispiel gibt die beiden Spalten für Name und Land des
Herstellers aus. Die Spalten werden durch Komma getrennt.

.. code-block:: sql

	SELECT Name, Land FROM Fahrzeughersteller;

Für die Ausgabe kann eine (abweichende) Spaltenüberschrift festgelegt werden.
Diese wird als **Spalten-Alias** bezeichnet. Der Alias kann dem Spaltennamen direkt folgen oder mit dem Bindewort AS angegeben werden. Das vorherige Beispiel kann also wie folgt mit dem Alias Hersteller für Name und dem Alias Staat für Land versehen werden:


.. code-block:: sql

	SELECT Name Hersteller, Land AS Staat FROM Fahrzeughersteller;


.. index:: DISTINCT


DISTINCT - Keine doppelten Zeilen
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. admonition:: Aufgabe

	Gesucht wird die Liste der Herstellerländer:

	.. code-block:: sql

		SELECT Land FROM Fahrzeughersteller;


Dabei stellen wir fest, dass je Hersteller eine Zeile ausgegeben wird. Somit erscheint beispielweise 'Deutschland' mehrmals. Damit keine doppelten Zeilen ausgegeben werden, wird DISTINCT vor den Spaltennamen in das SQL-Statement eingefügt:

	.. code-block:: sql

		SELECT DISTINCT Land FROM Fahrzeughersteller;

Damit erscheint jedes Herstellerland nur einmal in der Liste. Die Alternative zu DISTINCT ist übrigens das in der Syntax genannte ALL: alle Zeilen werden gewünscht, ggf. auch doppelte. Dies ist aber der Standardwert, ALL kann weggelassen werden.


Als Mengenquantiﬁzierer stehen somit DISTINCT und ALL zur Verfügung.
Außerdem ist es sehr oft möglich (wenn auch nicht im SQL-Standard vorgeschrieben), das Ergebnis auf eine bestimmte Anzahl von Zeilen zu beschränken, z. B. FIRST 3 oder LIMIT 7.



.. index:: where

WHERE − Eingrenzen der Ergebnismenge
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Fast immer soll nicht der komplette Inhalt einer Tabelle ausgegeben werden. Dazu wird die Ergebnismenge mittels Bedingungen in der WHERE-Klausel eingegrenzt, welche nach dem Tabellennamen im SELECT-Befehl steht. Eine Bedingung ist ein logischer Ausdruck, dessen Ergebnis WAHR oder FALSCH ist. In diesen logischen Ausdrücken werden die Inhalte der Spalten (vorwiegend) mit konstanten Werten verglichen. Hierbei stehen verschiedene Operatoren zur
Verfügung, vor allem:

::

	= gleich
	<> ungleich; seltener auch: !=
	< kleiner als
	<= kleiner als oder gleich
	> größer als
	>= größer als oder gleich


Bedingungen können durch die logischen Operatoren OR und AND und die
Klammern () verknüpft werden. Je komplizierter solche Verknüpfungen werden,
desto sicherer ist es, die Bedingungen durch Klammern zu gliedern. Mit diesen
Mitteln lässt sich die Abfrage entsprechend eingrenzen.


.. admonition:: Aufgabe

 	Es sollen alle Hersteller angezeigt werden, die ihren Sitz in Schweden oder Frankreich haben:

 	.. code-block:: sql

 		SELECT * FROM Fahrzeughersteller WHERE ( Land = 'Schweden' )
 		     OR ( Land = 'Frankreich' );


Hinter der WHERE-Klausel kann man also eine oder mehrere (mit einem booleschen Operator verknüpft) Bedingungen einfügen. Jede einzelne besteht aus dem Namen der Spalte, deren Inhalt überprüft werden soll, und einem Wert, wobei beide mit einem Vergleichsoperator verknüpft sind.

In einer anderen Abfrage sollen alle Fahrzeughersteller angezeigt werden, die außerhalb Deutschlands sitzen. Jetzt könnte man alle anderen Fälle einzeln in der WHERE-Klausel auﬂisten, oder man dreht einfach den Vergleichsoperator um.

.. code-block:: sql

	SELECT * FROM Fahrzeughersteller WHERE Land <> 'Deutschland';


Mit der WHERE-Klausel wird eine Suchbedingung festgelegt, welche Zeilen der Ergebnistabelle zur Auswahl gehören sollen. Dieser Filter wird zuerst erstellt; erst anschließend werden Bedingungen wie GROUP BY und ORDER BY ausgewertet.


Die Suchbedingung <search condition> ist eine Konstruktion mit einem eindeutigen Prüfergebnis: Entweder die Zeile gehört zur Auswahl, oder sie gehört nicht zur Auswahl. Es handelt sich also um eine logische Verknüpfung von einer oder mehreren booleschen Variablen.


Einzelne Suchbedingung
~~~~~~~~~~~~~~~~~~~~~~

.. index:: between, like, containing, not in, in

Eine Suchbedingung hat eine der folgenden Formen, deren Ergebnis immer WAHR oder FALSCH (TRUE bzw. FALSE) lautet:


::


	<wert1> [ NOT ] <operator> <wert2>
	<wert1> [ NOT ] BETWEEN <wert2> AND <wert3>
	<wert1> [ NOT ] LIKE <wert2> [ ESCAPE <wert3> ]
	<wert1> [ NOT ] CONTAINING <wert2>
	<wert1> [ NOT ] STARTING [ WITH ] <wert2>
	<wert1> IS [ NOT ] NULL
	<wert1> [ NOT ] IN <werteliste>
	EXISTS <select expression>
	NOT <search condition>


Anstelle eines Wertes kann auch ein Wertausdruck <value expression> stehen, also eine Unterabfrage, die genau einen Wert als Ergebnis liefert.
Anstelle einer Werteliste kann auch ein Auswahl-Ausdruck <select expression> stehen, also eine Unterabfrage, die eine Liste mehrerer Werte als Ergebnis liefert.
Als <operator> sind folgende Vergleiche möglich, und zwar sowohl für Zahlen als auch für Zeichenketten und Datumsangaben:

	= < > <= >= <>




BETWEEN AND − Werte zwischen zwei Grenzen
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Mit der Bedingung BETWEEN <wert1> AND <wert2> wird direkt mit einem Bereich verglichen; die Grenzwerte gehören meistens zum Bereich (abhängig vom DBMS). Auch dies ist möglich für Zahlen, Zeichenketten, Datumsangaben.

.. admonition:: Aufgabe

	Suche Datensätze, bei denen die PLZ außerhalb eines Bereichs 45000...45999 liegt.

	.. code-block:: sql

		select * from Versicherungsnehmer  where PLZ NOT BETWEEN '45000' AND '45999';



LIKE − Ähnlichkeiten
~~~~~~~~~~~~~~~~~~~~

Die LIKE-Bedingung vergleicht Zeichenketten „ungenau“: Der gesuchte Text soll als Wert in einer Spalte enthalten sein; dazu werden „Wildcards“ benutzt: Der Unterstrich '_' steht für ein beliebiges einzelnes Zeichen, das an der betreffenden Stelle vorkommen kann. Das Prozentzeichen '%' steht für eine beliebige Zeichenkette mit 0 oder mehr Zeichen.
Diese Bedingung wird vor allem in zwei Situationen gerne benutzt:

- Der Suchbegriff ist sehr lang; dem Anwender soll es genügen, den Anfang einzugeben.
- Der Suchbegriff ist nicht genau bekannt (z. B. nicht richtig lesbar).

.. Admonition:: Beispiele

	Der Ortsname beginnt nicht mit 'B'; der Inhalt dahinter ist beliebig.

	.. code-block:: sql

		select * from Versicherungsnehmer where Ort NOT LIKE 'B%';

	Der Ortsname enthält irgendwo 'alt' mit beliebigem Inhalt davor und dahinter.

	.. code-block:: sql

		select * from Versicherungsnehmer where Ort LIKE '%alt%';

	Der Anfangsbuchstabe des Namens ist unklar, aber danach folgen die Buchstaben 'ei' und noch etwas mehr.

	.. code-block:: sql

		select * from Versicherungsnehmer where Name LIKE '_ei%';

Ein Problem haben wir, wenn eines der Wildcard-Zeichen Teil des Suchbegriffs sein soll. Dann muss dem LIKE-Parameter mitgeteilt werden, dass '%' bzw. '_-' als "echtes" Zeichen zu verstehen ist. Das geschieht dadurch, dass ein spezielles Zeichen davor gesetzt wird und dieses Zeichen als "ESCAPE-Zeichen" angegeben
wird:

.. admonition:: Aufgabe

	Innerhalb der Beschreibung kommt die Zeichenfolge '10%' vor.

	code-block:: sql

		select * from Schadensfall where Beschreibung LIKE '%10\%%' ESCAPE '\';

Das erste und das letzte Prozentzeichen stehen dafür, dass vorher und nachher beliebige Inhalte möglich sind. Das mittlere Prozentzeichen wird mit dem Escape-Zeichen '\' verbunden und ist damit Teil der gesuchten Zeichenfolge. Diese Angabe '\%' ist als ein Zeichen zu verstehen.

Vergleichen Sie das Abfrageergebnis, wenn der ESCAPE-Parameter weggelassen wird oder wenn eines oder mehrere der Sonderzeichen im LIKE-Parameter fehlen.

.. index:: is null, null

IS NULL − null-Werte prüfen
~~~~~~~~~~~~~~~~~~~~~~~~~~~

NULL-Werte haben eine besondere Bedeutung. Mit den folgenden beiden Abfragen werden nicht alle Datensätze gefunden:

.. code-block:: sql

	select ID, Name, Vorname, Mobil from Mitarbeiter
	where Mobil <> '';
	//8 Mitarbeiter mit Mobil-Nummer


	select ID, Name, Vorname, Mobil
	from Mitarbeiter
	where Mobil = '';
	//10 Mitarbeiter ohne Mobil-Nummer

Nanu, es gibt doch 28 Mitarbeiter; wo sind die übrigen geblieben? Für diese Fälle gibt es mit IS NULL eine spezielle Abfrage:

.. code-block:: sql

	select ID, Name, Vorname, Mobil
	from Mitarbeiter
	where Mobil is null;
	//10 Mitarbeiter ohne Angabe


Der Vollständigkeit halber sei darauf hingewiesen, dass die folgende Abfrage tatsächlich die richtige Gegenprobe liefert.

.. code-block:: sql

	select ID, Name, Vorname, Mobil from Mitarbeiter
	where Mobil is not null;
	//18 Mitarbeiter mit irgendeiner Angabe (auch mit "leerer" Angabe)

Die folgende Abfrage liefert eine leere Ergebnismenge zurück, weil NULL eben kein Wert ist.

.. code-block:: sql

	select ID, Name, Vorname, Mobil
	from Mitarbeiter
	where Mobil = null;

Es gibt keine einzelne Bedingung, die alle Datensätze ohne explizite MobilAngabe auf einmal angibt. Es gibt nur die Möglichkeit, die beiden Bedingungen "IS NULL" und "ist leer" zu verknüpfen:

.. code-block:: sql

	select ID, Name, Vorname, Mobil
	from Mitarbeiter
	where ( Mobil is null ) OR ( Mobil = '' );


Beachten Sie auch bei "WHERE ... IS [NOT] NULL" die Bedeutung von NULL:
- Bei Zeichenketten ist zu unterscheiden zwischen dem "leeren" String und dem NULL-Wert.

- Bei Zahlen ist zu unterscheiden zwischen der Zahl '0' (null) und dem NULL-Wert.

- Bei Datumsangaben ist zu unterscheiden zwischen einem vorhandenen Datum und dem NULL-Wert; ein Datum, das der Zahl 0 entspräche, gibt es nicht. (Man könnte allenfalls das kleinste mögliche Datum wie '01.01.0100' benutzen, aber dies ist bereits ein Datum.)

.. index:: IN

IN − genauer Vergleich mit einer Liste
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Der IN-Parameter vergleicht, ob der Inhalt einer Spalte in der angegebenen Liste enthalten ist. Die Liste kann mit beliebigen Datentypen arbeiten.

.. admonition:: Aufgabe:

	Hole die Liste aller Fahrzeuge, deren Typen als "VW-Kleinwagen" registriert sind.

	.. code-block:: sql

		select * from Fahrzeug where Fahrzeugtyp_ID in (1, 2);

	Suche nach einem Unfall Fahrzeuge mit einer von mehreren möglichen Farben.

	.. code-block:: sql

		select * from Fahrzeug where Farbe in ('ocker', 'gelb');


	Hole die Liste aller Fahrzeuge vom Typ „Volkswagen“.

	.. code-block:: sql

		select * from Fahrzeug where Fahrzeugtyp_ID in
			( select ID from Fahrzeugtyp where Hersteller_ID = 1 );

	Dabei wird zuerst mit der Unterabfrage eine Liste aller Fahrzeugtypen-IDs für den Hersteller 1 (= Volkswagen) zusammengestellt; diese wird dann für den Vergleich über den IN-Parameter benutzt.


.. index:: EXISTS

EXISTS − schneller Vergleich mit einer Liste
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Im Gegensatz zu den anderen Parametern der WHERE-Klausel arbeitet der EXISTS-Parameter nicht mit fest vorgegebenen Werten, sondern nur mit dem Ergebnis einer Abfrage, also einer Unterabfrage. Das letzte Beispiel zum IN-Parameter kann auch so formuliert werden:

.. code-block:: sql

	//Liste aller Fahrzeuge vom Typ 'Volkswagen'
	select * from Fahrzeug fz
	where EXISTS
		( select * from Fahrzeugtyp ft
			where ft.Hersteller_ID = 1
			and fz.Fahrzeugtyp_ID = ft.ID );

Zu jedem Datensatz aus der Tabelle Fahrzeug wird zu dieser Fahrzeugtyp_ID eine Unterabfrage aus den Fahrzeugtypen erstellt: Wenn es dort einen Datensatz mit passender ID und Hersteller-ID 1 (= Volkswagen) gibt, gehört der Fahrzeug-Datensatz zur Auswahl, andernfalls nicht.

Da Unterabfragen zuerst ausgeführt werden, wird eine EXISTS-Prüfung in aller Regel schneller erledigt als die entsprechende IN-Prüfung: Bei EXISTS handelt es sich um eine Feststellung "ist überhaupt etwas vorhanden"; bei IN dagegen muss ein exakter Vergleich mit allen Werten einer Liste durchgeführt werden. Bei unserer kleinen Beispieldatenbank spielt das natürlich keine Rolle, aber bei einer "echten" Datenbank mit Millionen von Einträgen schon.


Mehrere Bedingungen
~~~~~~~~~~~~~~~~~~~

Bei der WHERE-Klausel geht es darum festzustellen, ob ein Datensatz Teil des Abfrageergebnisses ist oder nicht; bei der <search condition> handelt sich also um einen booleschen Ausdruck, d. h. einen Ausdruck, der einen der booleschen Werte WAHR oder FALSCH − TRUE bzw. FALSE − als Ergebnis hat. Nur bei einfachen Abfragen genügt dazu eine einzelne Bedingung; meistens müssen mehrere Bedingungen verknüpft werden (wie beim letzten Beispiel unter IS NULL).

Dazu gibt es die booleschen Operatoren NOT, AND, OR.


.. index:: NOT

NOT als Negation
^^^^^^^^^^^^^^^^

Dies liefert die Umkehrung: aus TRUE wird FALSE, aus FALSE wird TRUE.

.. code-block:: sql

	SELECT * FROM Versicherungsnehmer
	WHERE NOT (Fuehrerschein >= '01.01.2007');


.. index:: AND

AND als Konjunktion
^^^^^^^^^^^^^^^^^^^

Eine Bedingung, die durch eine AND-Verknüpfung gebildet wird, ist genau dann TRUE, wenn beide (bzw. alle) Bestandteile TRUE sind.

.. code-block:: sql

	//Die nach Alphabet erste Hälfte der Versicherungsnehmer eines PLZ-Bereichs

	SELECT ID, Name, Vorname, PLZ, Ort
	FROM Versicherungsnehmer
	WHERE PLZ BETWEEN '45000' AND '45999'
	AND Name < 'K';

.. index:: OR

OR als Adjunktion
^^^^^^^^^^^^^^^^^

Eine Bedingung, die durch eine OR-Verknüpfung gebildet wird, ist genau dann TRUE, wenn mindestens ein Bestandteil TRUE ist; dabei ist es gleichgültig, ob die anderen Bestandteile TRUE oder FALSE sind.


.. code-block:: sql

	//Die nach Alphabet erste Hälfte der Versicherungsnehmer und alle eines PLZ-Bereichs
	SELECT ID, Name, Vorname, PLZ, Ort
	FROM Versicherungsnehmer
	WHERE PLZ BETWEEN '45000' AND '45999'
	OR Name < 'K';


Bitte beachten Sie, dass der normale Sprachgebrauch "alle ... und alle ..." sagt.
Gemeint ist nach logischen Begriffen aber, dass <Bedingung 1> erfüllt sein muss
ODER <Bedingung 2> ODER BEIDE.Mehrere Bedingungen verknüpfen


.. indx:: XOR

XOR als Kontravalenz
^^^^^^^^^^^^^^^^^^^^

Eine Bedingung, die durch eine XOR-Verknüpfung gebildet wird, ist genau dann TRUE, wenn ein Bestandteil TRUE ist, aber der andere Bestandteil FALSE ist −
"ausschließendes oder" bzw. "entweder − oder". Diese Verknüpfung gibt es selten,
z. B. bei MySQL; hier wird es der Vollständigkeit halber erwähnt.


.. code-block:: sql

	//Die nach Alphabet erste Hälfte der Versicherungsnehmer
	//oder alle eines PLZ-Bereichs

	SELECT ID, Name, Vorname, PLZ, Ort
	FROM Versicherungsnehmer
	WHERE PLZ BETWEEN '45000' AND '45999'
	XOR Name < 'K';


Bitte beachten Sie, dass hier der normale Sprachgebrauch „oder“ sagt und „ent-
weder − oder“ gemeint ist. Anstelle von XOR kann immer eine Kombination verwendet werden:


.. code-block:: sql

	( <Bedingung 1> AND ( NOT <Bedingung 2> ) )
	OR ( <Bedingung 2> AND ( NOT <Bedingung 1> ) )


Klammern benutzen oder weglassen?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Bereits im Kapitel „Ausführliche SELECT-Struktur“ wurde die Hierarchie genannt:

#. NOT ist die engste Verbindung und wird vorrangig ausgewertet.
#. AND ist die nächststärkere Verbindung und wird danach ausgewertet.
#. OR ist die schwächste Verbindung und wird zuletzt ausgewertet.
#. Was in Klammern steht, wird vor allem anderen ausgewertet.

Bitte setzen Sie im folgenden Beispiel Klammern an anderen Stellen oder streichen Sie Klammern, und vergleichen Sie die Ergebnisse.

.. code-block:: sql

	SELECT ID, Name, Vorname, PLZ, Ort FROM Versicherungsnehmer
	WHERE not ( PLZ BETWEEN '45000' AND '45999'
	AND ( Name LIKE 'B%'
	OR Name LIKE 'K%'
	OR NOT Name CONTAINING 'ei'
	)
	)
	order by PLZ, Name;

Sie werden ziemlich unterschiedliche Ergebnisse erhalten. Es empﬁehlt sich deshalb, an allen sinnvollen Stellen Klammern zu setzen − auch dort, wo sie nicht erforderlich sind − und das, was zusammengehört, durch Einrückungen sinnvoll zu gliedern.

.. raw:: latex

         \newpage


Zusammenfassung
~~~~~~~~~~~~~~~

In diesem Kapitel lernten wir neben dem Vergleich von Werten viele Möglichkeiten kennen, mit denen Bedingungen für Abfragen festgelegt werden können:

- Mit BETWEEN AND werden Werte innerhalb eines Bereichs geprüft.
- Mit LIKE und CONTAINS werden Werte gesucht, die mit vorgegebenen Werten teilweise übereinstimmen.
- Mit IS NULL werden null-Werte gesucht.
- Mit IN und EXISTS werden Spaltenwerte mit einer Liste verglichen.
- Mit AND, OR, NOT werden Bedingungen zusammengefasst.


.. raw:: latex

         \newpage

Übungen
~~~~~~~

.. admonition:: Aufgabe

	**Auswahl nach Zeichenketten**

	Suchen Sie alle Versicherungsnehmer, die folgenden Bedingungen entsprechen:
	- Der erste Buchstabe des Nachnamens ist nicht bekannt, der zweite ist ein 'r'.
	- Der Vorname enthält ein 'a'.
	- Die Postleitzahl gehört zum Bereich Essen (PLZ 45...).

	**Auswahl nach Datumsbereich**

	Suchen Sie alle Versicherungsnehmer, die in den Jahren 1967 bis 1970 ihren
	18. Geburtstag hatten.

	**Auswahl nach Ähnlichkeit**

	Zeigen Sie alle Schadensfälle an, bei denen in der Beschreibung auf eine prozen-
	tuale Angabe hingewiesen wird.


	**Auswahl für unbekannte Werte**

	Zeigen Sie alle Dienstwagen an, die keinem Mitarbeiter persönlich zugeordnet
	sind.

	**Bedingungen verknüpfen**

	Zeigen Sie alle Mitarbeiter der Abteilungen „Vertrieb“ (= 'Vert') und „Ausbildung“
	(= 'Ausb') an.

	Hinweis: Bestimmen Sie zunächst die IDs der gesuchten Abteilungen und benut-
	zen Sie das Ergebnis für die eigentliche Abfrage.


	**Bedingungen verknüpfen**

	Gesucht werden die Versicherungsverträge für Haftpflicht (= 'HP') und Teilkasko
	(= 'TK'), die mindestens seit dem Ende des Jahres 1980 bestehen und aktuell nicht
	mit dem minimalen Prämiensatz berechnet werden.
	Hinweis: Tragen Sie ausnahmsweise nur die notwendigen Klammern ein, nicht alle sinnvollen.

.. only:: html

	Lösungen
	~~~~~~~~

	.. admonition:: Lösung

		**Auswahl nach Zeichenketten**

		Suchen Sie alle Versicherungsnehmer, die folgenden Bedingungen entsprechen:
		- Der erste Buchstabe des Nachnamens ist nicht bekannt, der zweite ist ein 'r'.
		- Der Vorname enthält ein 'a'.
		- Die Postleitzahl gehört zum Bereich Essen (PLZ 45...).

		.. code-block:: sql

			select * from Versicherungsnehmer
			where Name like '_r%' and Vorname like '%a%'
			and PLZ like '45%';

		**Auswahl nach Datumsbereich**

		Suchen Sie alle Versicherungsnehmer, die in den Jahren 1967 bis 1970 ihren
		18. Geburtstag hatten.

		.. code-block:: sql

			select * from Versicherungsnehmer
			where DATEADD(YEAR, 18, Geburtsdatum) BETWEEN '01.01.1967' AND '31.12.1970';

		**Auswahl nach Ähnlichkeit**

		Zeigen Sie alle Schadensfälle an, bei denen in der Beschreibung auf eine prozen-
		tuale Angabe hingewiesen wird.

		.. code-block:: sql

			SELECT * from Schadensfall
			where Beschreibung like '%\%%' escape '\';

		**Auswahl für unbekannte Werte**

		Zeigen Sie alle Dienstwagen an, die keinem Mitarbeiter persönlich zugeordnet
		sind.

		.. code-block:: sql

			SELECT * from Dienstwagen
			where Mitarbeiter_ID is null;



		**Bedingungen verknüpfen**

		Zeigen Sie alle Mitarbeiter der Abteilungen „Vertrieb“ (= 'Vert') und „Ausbildung“
		(= 'Ausb') an.

		Hinweis: Bestimmen Sie zunächst die IDs der gesuchten Abteilungen und benut-
		zen Sie das Ergebnis für die eigentliche Abfrage.

		.. code-block:: sql

			SELECT * from Mitarbeiter
			where Abteilung_ID in (
				select id from Abteilung
				where Kuerzel in ('Vert', 'Ausb')
				);


		**Bedingungen verknüpfen**

		Gesucht werden die Versicherungsverträge für Haftpﬂicht (= 'HP') und Teilkasko
		(= 'TK'), die mindestens seit dem Ende des Jahres 1980 bestehen und aktuell nicht
		mit dem minimalen Prämiensatz berechnet werden.
		Hinweis: Tragen Sie ausnahmsweise nur die notwendigen Klammern ein, nicht alle sinnvollen.

		.. code-block: sql

			SELECT * from Versicherungsvertrag
		where (Art = 'HP' or Art = 'TK')
		and Abschlussdatum <= '31.12.1980'
		/* ab hiergeht es nicht weil spalte praemiensatz nicht vorhanden */
		and (not Praemiensatz = 30)
		/* oder
		and Praemiensatz > 30;
		*/


.. raw:: latex

         \newpage


ORDER BY − Sortieren
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Nachdem wir nun die Zeilen und Spalten der Ergebnismenge eingrenzen können,
wollen wir die Ausgabe der Zeilen sortieren. Hierfür wird die ORDER BY-Klausel
genutzt. Diese ist die letzte im SQL-Befehl vor dem abschließenden Semikolon
und enthält die Spalten, nach denen sortiert werden soll.


.. admonition:: Aufgabe

	So lassen wir uns die Liste der Hersteller nach dem Namen sortiert ausgeben:

	.. code-block:: sql

		SELECT * FROM Fahrzeughersteller ORDER BY Name;

Anstatt des Spaltennamens kann auch die Nummer der Spalte genutzt werden.
Mit dem folgenden Statement erreichen wir also das gleiche Ergebnis, da Name
die 2. Spalte in unserer Ausgabe ist:

.. code-block:: sql

	SELECT * FROM Fahrzeughersteller ORDER BY 2;


Die Angabe nach Spaltennummer ist unüblich; sie wird eigentlich höchstens dann verwendet, wenn die Spalten genau aufgeführt werden und komplizierte Angaben − z. B. Berechnete Spalten
enthalten.

Die Sortierung erfolgt standardmäßig aufsteigend; das kann auch durch ASC ausdrücklich angegeben werden. Die Sortierreihenfolge kann mit dem DESC-Bezeichner in absteigend verändert werden.

.. code-block:: sql

	SELECT * FROM Fahrzeughersteller ORDER BY Name DESC;

In SQL kann nicht nur nach einer Spalte sortiert werden. Es können mehrere Spalten zur Sortierung herangezogen werden. Hierbei kann für jede Spalte eine eigene Regel verwendet werden. Dabei gilt, dass die Regel zu einer folgend angegebenen Spalte der Regel zu der vorig angegebenen Spalte untergeordnet ist. Bei der Sortierung nach Land und Name wird also zuerst nach dem Land und dann je Land nach Name sortiert. Eine Neusortierung nach Name, die jene Sortierung nach Land wieder verwirft, ﬁndet also nicht statt.


Der folgende Befehl liefert die Hersteller − zuerst absteigend nach
Land und dann aufsteigend sortiert nach dem Namen − zurück.

.. code-block:: sql

	SELECT * FROM Fahrzeughersteller ORDER BY Land DESC, Name ASC;

.. raw:: latex

         \newpage


.. raw:: latex

         \newpage


FROM − Mehrere Tabellen verknüpfen
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In fast allen Abfragen werden Informationen aus mehreren Tabellen zusammengefasst. Die sinnvolle Speicherung von Daten in getrennten Tabellen ist eines der Merkmale eines relationalen DBMS; deshalb müssen die Daten bei einer Abfrage nach praktischen Gesichtspunkten zusammengeführt werden.

.. note::

	Informieren Sie sich über den Begriff **Kartesisches Kreuzprodukt**



FROM und WHERE
~~~~~~~~~~~~~~~

Beim „traditonellen“ Weg werden dazu einfach alle Tabellen in der FROM-Klausel
aufgeführt und durch jeweils eine Bedingung in der WHERE-Klausel verknüpft.

.. admonition:: Aufgabe

	Ermittle die Angaben der Mitarbeiter incl. Abteilungsname, deren Abteilung ihren Sitz in Dortmund oder Bochum hat.

	.. code-block:: sql

		SELECT mi.Name, mi.Vorname, mi.Raum, ab.Ort
		FROM Mitarbeiter mi, Abteilung ab
		WHERE mi.Abteilung_ID = ab.ID
			AND ab.Ort in ('Dortmund', 'Bochum')
		ORDER BY mi.Name, mi.Vorname;


Es werden also Informationen aus den Tabellen Mitarbeiter (Name und Raum) sowie Abteilung (Ort) gesucht. Für die Verknüpfung der Tabellen werden folgende Bestandteile benötigt:

- In der FROM-Klausel stehen die benötigten Tabellen.
- Zur Vereinfachung wird jeder Tabelle ein Kürzel als Tabellen-Alias zugewiesen.
- In der Spaltenliste wird jede einzelne Spalte mit dem Namen der betreffenden
Tabelle bzw. dem Alias verbunden. (Der Tabellenname bzw. Alias kann sehr oft
weggelassen werden; aber schon wegen der Übersichtlichkeit sollte er immer
benutzt werden.)
- Die WHERE-Klausel enthält die Verknüpfungsbedingung „mi.Abteilung_ID =
ab.ID“ − zusätzlich zur Einschränkung nach dem Sitz der Abteilung.
Jede Tabelle in einer solchen Abfrage benötigt mindestens eine direkte Verknüp-
fung zu einer anderen Tabelle. Alle Tabellen müssen zumindest indirekt mitein-
ander verknüpft sein. Falsche Verknüpfungen sind eine häuﬁge Fehlerquelle.


.. index:: JOIN

JOIN ... ON
~~~~~~~~~~~

Beim „modernen“ Weg wird eine Tabelle in der FROM-Klausel aufgeführt, nämlich diejenige, die als wichtigste oder „Haupttabelle“ der Abfrage angesehen wird.
Eine weitere Tabelle wird durch JOIN und eine Bedingung in der ON-Klausel verknüpft.
Das obige Beispiel sieht dann so aus:

.. code-block:: sql


	SELECT mi.Name,mi.Vorname,mi.Raum,ab.Ort
	FROM Mitarbeiter mi
	JOIN Abteilung ab
		ON mi.Abteilung_ID = ab.ID
	WHERE ab.Ort in ('Dortmund', 'Bochum')
	ORDER BY mi.Name, mi.Vorname;

Für die Verknüpfung der Tabellen werden folgende Bestandteile benötigt:
- In der FROM-Klausel steht eine der benötigten Tabellen.
- In der JOIN-Klausel steht jeweils eine weitere Tabelle.
- Die ON-Klausel enthält die Verknüpfungsbedingung „mi.Abteilung_ID = ab.ID“.
- Die WHERE-Klausel beschränkt sich auf die wirklich gewünschten Einschränkungen für die Ergebnismenge.

Ein Tabellen-Alias ist wiederum für alle Tabellen sinnvoll. In der Spaltenliste und
auch zur Sortierung können alle Spalten aller Tabellen benutzt werden.
Vertiefte Erläuterungen sind unter „Arbeiten mit JOIN“ zu ﬁnden.


.. raw:: latex

         \newpage


Zusammenfassung
^^^^^^^^^^^^^^^


- SELECT-Befehle werden zur Abfrage von Daten aus Datenbanken genutzt.
- Die auszugebenden Spalten können festgelegt werden, indem die Liste der Spalten zwischen den Bezeichnern SELECT und FROM angegeben wird.
- Mit DISTINCT werden identische Zeilen in der Ergebnismenge nur einmal ausgegeben.
- Die Ergebnismenge wird mittels der WHERE-Klausel eingegrenzt.
- Die WHERE-Klausel enthält logische Ausdrücke. Diese können mit AND und OR verknüpft werden.
- Mittels der ORDER BY-Klausel kann die Ergebnismenge sortiert werden.

Die Reihenfolge innerhalb eines SELECT-Befehls ist zu beachten. SELECT und FROM sind hierbei Pﬂicht, das abschließende Semikolon als Standard empfohlen. Alle anderen Klauseln sind optional.

.. raw:: latex

         \newpage

Übungen
^^^^^^^

- Welche Bestandteile eines SELECT-Befehls sind unbedingt erforderlich und können nicht weggelassen werden?
- Geben Sie alle Informationen zu allen Abteilungen aus.
- Geben Sie alle Abteilungen aus, deren Standort Bochum ist.
- Geben Sie alle Abteilungen aus, deren Standort Bochum oder Essen ist. Hierbei soll nur der Name der Abteilung ausgegeben werden.
- Geben Sie nur die Kurzbezeichnungen aller Abteilungen aus. Hierbei sollen die Abteilungen nach den Standorten sortiert werden.


.. only:: html

	Lösung
	^^^^^^

	- Welche Bestandteile eines SELECT-Befehls sind unbedingt erforderlich und können nicht weggelassen werden?

	  .. code-block:: sql

		SELECT, Spaltenliste oder '*', FROM, Tabellenname

	- Geben Sie alle Informationen zu allen Abteilungen aus.

	  .. code-block:: sql

		select * from Abteilung;


	- Geben Sie alle Abteilungen aus, deren Standort Bochum ist.

	  .. code-block:: sql

		select * from Abteilung where Ort = 'Bochum';

	- Geben Sie alle Abteilungen aus, deren Standort Bochum oder Essen ist. Hierbei soll nur der Name der Abteilung ausgegeben werden.


	  .. code-block:: sql

		select Bezeichnung from Abteilung where Ort = 'Bochum' or Ort = 'Essen';
		select Bezeichnung from Abteilung where Ort in ('Bochum', 'Essen');


	- Geben Sie nur die Kurzbezeichnungen aller Abteilungen aus. Hierbei sollen die Abteilungen nach den Standorten sortiert werden.

	  .. code-block:: sql

		select Kuerzel from Abteilung order by Ort;






.. raw:: latex

         \newpage


.. index:: JOIN



.. index:: Aggregatfunktion, Spaltenfunktion


.. index:: Aggregatfunktion

Aggregatfunktionen
------------------

.. image:: figure/sqlaggregat.jpg

Die Spaltenfunktionen werden auch als Aggregatfunktionen bezeichnet, weil sie eine Menge von Werten − nämlich aus allen Zeilen einer bestimmten Spalte − zusammenfassen und einen gemeinsamen Wert bestimmen. In der Regel wird dazu eine Spalte aus einer der beteiligten Tabellen verwendet; es kann aber auch ein sonstiger gültiger SQL-Ausdruck sein, der als Ergebnis einen einzelnen Wert liefert. Das Ergebnis der Funktion ist dann ein Wert, der aus allen passenden Zeilen der Abfrage berechnet wird.
Bei Abfragen kann das Ergebnis einer Spaltenfunktion auch nach den Werten einer oder mehrerer Spalten oder Berechnungen gruppiert werden. Die Aggregatfunktionen liefern dann für jede Gruppe ein Teilergebnis.


.. index:: COUNT

**COUNT − Anzahl**

Die Funktion COUNT zählt alle Zeilen, die einen eindeutigen Wert enthalten, also nicht NULL sind. Sie kann auf alle Datentypen angewendet werden, da für jeden Datentyp NULL deﬁniert ist.

**Beispiel**

.. code-block:: sql

	SELECT COUNT(Farbe) AS Anzahl_Farbe FROM Fahrzeug;

Die Spalte Farbe ist als VARCHAR(30), also als Text variabler Länge, deﬁniert und optional. Hier werden also alle Zeilen gezählt, die in dieser Spalte einen Eintrag haben. Dasselbe funktioniert auch mit einer Spalte, die numerisch ist:

.. code-block:: sql


	SELECT COUNT(Schadenshoehe) AS Anzahl_Schadenshoehe FROM Schadensfall;

Hier ist die Spalte numerisch und optional. Die Zahl 0 ist bekanntlich nicht NULL. Wenn in der Spalte eine 0 steht, wird sie mitgezählt.

Ein Spezialfall ist der Asterisk ’*’ als Parameter. Dies bezieht sich dann nicht auf eine einzelne Spalte, sondern auf eine ganze Zeile. So wird also die Anzahl der Zeilen in der Tabelle gezählt:

.. code-block:: sql

	SELECT COUNT(*) AS Anzahl_Zeilen FROM Schadensfall;

Die Funktion COUNT liefert niemals NULL zurück, sondern immer eine Zahl; wenn alle Werte in einer Spalte NULL sind, ist das Ergebnis die Zahl 0 (es gibt 0 Zeilen mit einem Wert ungleich NULL in dieser Spalte).


.. index:: SUM

**SUM − Summe**


Die Funktion SUM kann (natürlich) nur auf numerische Datentypen angewendet werden. Im Gegensatz zu COUNT liefert SUM nur dann einen Wert zurück, wenn wenigstens ein Eingabewert nicht NULL ist. Als Parameter kann nicht nur eine einzelne numerische Spalte, sondern auch eine Berechnung übergeben werden, die als Ergebnis eine einzelne Zahl liefert. Ein Beispiel für eine einzelne numerische Spalte ist:

.. code-block:: sql

	SELECT SUM(Schadenshoehe) AS Summe_Schadenshoehe
	FROM Schadensfall;

Als Ergebnis werden alle Werte in der Spalte Schadenshoehe aufsummiert. Als Parameter kann aber auch eine Berechnung übergeben werden.

.. Admonition:: Aufgabe

	Hier werden Euro-Beträge aus Schadenshoehe zuerst in US-Dollar nach einem Tageskurs umgerechnet und danach aufsummiert.

	.. code-block:: sql

		SELECT SUM(Schadenshoehe * 1.5068) AS Summe_Schadenshoehe_Dollar FROM Schadensfall;

.. index:: MAX, MIN

**MAX, MIN − Maximum, Minimum**


Diese Funktionen können auf jeden Datentyp angewendet werden, für den ein Vergleich ein gültiges Ergebnis liefert. Dies gilt für numerische Werte, Datumswerte und Textwerte, nicht aber für z. B. BLOBs (binary large objects). Bei Textwerten ist zu bedenken, dass die Sortierreihenfolge je nach verwendetem Betriebssystem, DBMS und Zeichensatzeinstellungen der Tabelle oder Spalte unterschiedlich ist, die Funktion demnach auch unterschiedliche Ergebnisse liefern kann.

Aufgabe: Suche den kleinsten, von NULL verschiedenen Schadensfall.

.. code-block:: sql

	SELECT MIN(Schadenshoehe) AS Minimum_Schadenshoehe FROM Schadensfall;

Kommen nur NULL-Werte vor, wird NULL zurückgegeben. Gibt es mehrere Zeilen, die den kleinsten Wert haben, wird trotzdem nur ein Wert zurückgegeben. Welche Zeile diesen Wert liefert, ist nicht deﬁniert.

Für MAX gilt Entsprechendes wie für MIN.

.. index:: AVG

**AVG − Mittelwert**

AVG (average = Durchschnitt) kann nur auf numerische Werte angewendet wer-
den. Das für SUM Gesagte gilt analog auch für AVG. Um die mittlere Schadenshö-
he zu berechnen, schreibt man:

.. code-block:: sql


	SELECT AVG(Schadenshoehe) AS Mittlere_Schadenshoehe FROM Schadensfall;

NULL-Werte ﬂießen dabei nicht in die Berechnung ein, Nullen aber sehr wohl.



.. raw:: latex

         \newpage



Gruppierungen
-------------

Abfragen werden sehr häuﬁg gruppiert, weil nicht nur einzelne Informationen, sondern auch Zusammenfassungen gewünscht werden. Durch die GROUP BY-Klausel im SELECT-Befehl werden alle Zeilen, die in einer oder mehreren Spalten den gleichen Wert enthalten, in jeweils einer Gruppe zusammengefasst. Dies macht in der Regel nur dann Sinn, wenn in der Spaltenliste des SELECT-Befehls eine gruppenweise Auswertung, also eine der Spaltenfunktionen enthalten ist.

Die GROUP BY-Klausel hat folgenden allgemeinen Aufbau:

.. code-block:: sql

 	GROUP BY <Spaltenliste>


Die Spaltenliste enthält, durch Komma getrennt, die Namen von einer oder mehreren Spalten.

Für jede Spalte kann eine eigene Sortierung angegeben werden:

.. code-block:: sql

	<Spaltenname>
	-- oder
	<Spaltenname> COLLATE <Collation-Name>


Die Spalten in der Spaltenliste können meistens wahlweise mit dem Spaltennamen der Tabelle, mit dem Alias-Namen aus der Select-Liste oder mit Spaltennummer gemäß der Select-Liste (ab 1 gezählt) angegeben werden. In der Regel enthält die Abfrage eine der Aggregatfunktionen und wird durch ORDER BY nach den gleichen Spalten sortiert.

Im einfachsten Fall werden Daten nach einer Spalte gruppiert und gezählt.

Im folgenden Beispiel wird die Anzahl der Abteilungen für jeden Ort aufgeführt.

.. code-block:: sql

	SELECT Ort,
	COUNT(*) AS Anzahl
	FROM Abteilung
	GROUP BY Ort
	ORDER BY Ort;

::

	Bochum 3
	Dortmund 4
	Essen 4
	Herne 1



.. Admonition:: Aufgabe

	Die folgende Abfrage listet auf, wie viele Mitarbeiter es in den Abteilungen und Raumnummern gibt:

	.. code-block:: sql


		SELECT Abteilung_ID AS Abt,
		Raum,
		COUNT(*) AS Anzahl
		FROM Mitarbeiter
		GROUP BY Abt, Raum
		ORDER BY Abt, Raum;
		ABT RAUM ANZAHL



::

	1 112 1
	1 113 1
	2 212 2
	3 312 1
	3 316 1
	4 412 2 // usw.


Am folgenden Beispiel wird die Gruppierung besonders deutlich. Gruppierung über mehrere Tabellen

.. Admonition:: Aufgabe

	Berechne die mittlere Schadenshöhe für die Schadensfälle mit und ohne Personenschäden.

	.. code-block:: sql

		SELECT Verletzte,
		AVG(Schadenshoehe) AS Mittlere_Schadenshoehe
		FROM Schadensfall
		GROUP BY Verletzte;


	::

		VERLETZTE MITTLERE_SCHADENSHOEHE
		J                       3.903,87
		N                       1.517,45


Die Spalte Verletzte enthält entweder ’J’ oder ’N’ und ist verpﬂichtend, kann also keine NULL-Werte enthalten. Deshalb werden durch die GROUP BY-Anweisung eine oder zwei Gruppen gebildet. Für jede Gruppe wird der Mittelwert gesondert berechnet aus den Werten, die in der Gruppe vorkommen. In diesem Fall liefert die Funktion AVG also ein oder zwei Ergebnisse, abhängig davon, welche Werte in der Spalte Verletzte überhaupt vorkommen.Zeilen, bei denen einer der Werte zur Gruppierung fehlt, oder Zeilen mit NULL-Werten werden als eigene Gruppe gezählt.

Eine Gruppierung kann auch Felder aus verschiedenen Tabellen auswerten. Dafür sind zunächst die Voraussetzungen für die Verknüpfung mehrerer Tabellen zu beachten.

.. Admonition:: Beispiel

	Gesucht wird für jeden Fahrzeughersteller (mit Angabe von ID und Name) und Jahr die Summe der Schadenshöhe aus der Tabelle Schadensfall.

	.. code-block:: sql


		SELECT fh.ID AS Hersteller_ID,
		fh.Name AS Hersteller_Name,
		EXTRACT(YEAR FROM sf.Datum) AS Jahr,
		SUM(sf.Schadenshoehe) AS Schadenssumme
		FROM Schadensfall sf
		JOIN Zuordnung_SF_FZ zu ON sf.ID = zu.Schadensfall_ID
		JOIN Fahrzeug fz ON fz.ID = zu.Fahrzeug_ID
		JOIN Fahrzeugtyp ft ON ft.ID = fz.Fahrzeugtyp_ID
		JOIN Fahrzeughersteller fh ON fh.ID = ft.Hersteller_ID
		GROUP BY Hersteller_ID, Hersteller_Name, Jahr
		ORDER BY Jahr, Hersteller_ID;


	 	::

	 		HERSTELLER_ID HERSTELLER_NAME JAHR SCHADENSSUMME
	 		9             Volvo           2007 2.066,00
	 		10            Renault         2007 5.781,60
	 		11            Seat            2007 1.234,50
	 		2             Opel            2008 1.438,75
	 		11            Seat            2008 1.983,00
	 		9             Volvo           2009 4.092,15
	 		10            Renault         2009   865,00


Ausgangspunkt ist die Tabelle Schadensfall, weil aus deren Einträgen die Summe gebildet werden soll. Durch JOIN werden nacheinander die verknüpften Tabellen herangezogen, und zwar jeweils durch die ID auf die Verknüpfung: Schadensfall → Zuordnung → Fahrzeug → Fahrzeugtyp → Hersteller. Dann stehen ID und Name aus der Tabelle Fahrzeughersteller zur Verfügung, die für die Gruppierung gewünscht werden.

Zur Gruppierung genügt eigentlich die Verwendung von Hersteller_ID. Aber man möchte sich natürlich den Herstellernamen anzeigen lassen. Allerdings gibt es einen Fehler, wenn man den Namen nur in der SELECT-Liste benutzt und in der GROUP BY-Liste streicht:


.. code-block:: sql

	/*Quelltext Falsch*/
	SELECT fh.ID AS Hersteller_ID,
	fh.Name AS Hersteller_Name,
	YEAR(sf.Datum) AS Jahr,
	SUM(sf.Schadenshoehe) AS Schadenssumme
	FROM Schadensfall sf
	join ... (wie oben)
	group by Hersteller_ID, Jahr
	order by Jahr, Hersteller_ID;

	Ungültiger Ausdruck in der Select-Liste
	(fehlt entweder in einer Aggregatfunktion oder in der GROUP BY-Klausel).


**Einschränkungen**

Wie das letzte Beispiel zeigt, muss die GROUP BY-Klausel gewisse Bedingungen erfüllen. Auch dafür gilt: Jedes DBMS weicht teilweise vom Standard ab.

- Jeder Spaltenname der SELECT-Auswahl, der nicht zu einer Aggregatfunktion gehört,
 muss auch in der GROUP BY-Klausel benutzt werden.

  Diese Bedingung wird im letzten Beispiel verletzt: Hersteller_Name steht in der SELECT-Liste, aber nicht in der GROUP BY-Klausel. In diesem Fall ist eine Änderung einfach, weil ID und Name des Herstellers gleichwertig sind. Übrigens erlaubt MySQL auch die Auswahl von Feldern, die in der GROUP BY-Klausel nicht genannt sind.

  Umgekehrt ist es in der Regel möglich, eine Spalte per GROUP BY zu gruppieren, ohne die Spalte selbst in der SELECT-Liste zu verwenden.

- GROUP BY kann Spalten der Tabellen, abgeleiteter Tabellen oder VIEWs in der FROM-Klausel
  oder der JOIN-Klausel enthalten. Sie kann keiner Spalte entsprechen, die das Ergebnis einer Funktion (genauer: einer numerischen Berechnung, einer Aggregatfunktion oder einer benutzerdeﬁnierten Funktion) sind.



.. raw:: latex

        \newpage

.. index:: HAVING

HAVING
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Diese Erweiterung ist eine selbständige Klausel des SELECT-Befehls und hat eigentlich nichts mit der GROUP BY-Klausel zu tun. In der Praxis wird sie aber überwiegend als Ergänzung zur Gruppierung verwendet und folgt ggf. direkt danach.

.. code-block:: sql

	GROUP BY <spaltenliste>
	HAVING <bedingungen>



Dieser Befehl dient dazu, nicht alle Gruppierungen in die Ausgabe zu übernehmen, sondern nur diejenigen, die den zusätzlichen Bedingungen entsprechen.
Im folgenden Beispiel wird festgestellt, an welchen Orten sich genau eine Abteilung beﬁndet.

.. code-block:: sql

	SELECT Ort, COUNT(*) AS Anzahl
	FROM Abteilung
	GROUP BY Ort
	HAVING COUNT(*) = 1
	ORDER BY Ort;


Bitte beachten Sie, dass der Alias-Name nicht verwendet werden kann, sondern
die Aggregatfunktion erneut aufgeführt werden muss.

.. raw:: latex

        \newpage


Übungen
^^^^^^^^

#. Welche der folgenden Feststellungen sind wahr, welche sind falsch?

  1. GROUP BY kann nur zusammen mit (mindestens) einer Spaltenfunktion benutzt werden.
  2. GROUP BY kann nur auf „echte“ Spalten angewendet werden, nicht auf berechnete Spalten.
  3. In der GROUP BY-Klausel kann ein Spaltenname ebenso angegeben werden wie ein
     Spalten-Alias.
  4. Die WHERE-Klausel kommt vor der GROUP BY-Klausel.
  5. Folgende Gruppierung nach den ersten zwei Ziffern der PLZ ist zulässig.

     .. code-block:: sql

         select PLZ, COUNT(*)
         from Versicherungsnehmer vn
         group by SUBSTRING(vn.PLZ from 1 for 2)
         order by 1;

   6. HAVING darf nur zusammen mit einer Gruppierung verwendet werden.

#. Bestimmen Sie die Anzahl der Kunden (Versicherungsnehmer) in jedem Briefzentrum
  (d. h. die Ziffern 1 und 2 der PLZ).
#. Bestimmen Sie, wie viele Fahrzeuge in jedem Kreis angemeldet sind.
#. Bestimmen Sie, wie viele Fahrzeugtypen pro Hersteller registriert sind, und nennen
  Sie Namen und Land der Hersteller.
  Hinweis: Erstellen Sie zunächst eine Abfrage für Anzahl plus Hersteller-ID und verknüpfen Sie das Ergebnis mit der Tabelle Hersteller.
#. Bestimmen Sie – gruppiert nach Jahr des Schadensfalls und Kreis des Fahrzeugs –
  die Anzahl der Schadensfälle. Es sollen bei den Fahrzeugen nur Schadensfälle mit einem Schuldanteil von mehr als 50 [Prozent] berücksichtigt werden.

.. raw:: latex

        \newpage



.. only:: html

	Lösungen
	^^^^^^^^

	#. Lösung zu Übung 1

	   Richtig sind die Aussagen 3, 4. Falsch sind die Aussagen 1, 2, 5, 6.

	#. Lösung zu Übung 2

	   .. code-block:: sql

		select SUBSTRING(vn.PLZ from 1 for 2), COUNT(*)
		from Versicherungsnehmer vn
		group by 1
		order by 1;

	#. Lösung zu Übung 3

	   .. code-block:: sql

		select SUBSTRING(Kennzeichen from 1 for POSITION(’-’, Kennzeichen)-1 ) as Kreis,
		COUNT(*) as Anzahl
		from Fahrzeug fz
		group by 1
		order by 1;


	#. Lösung zu Übung 4

	   .. code-block:: sql

		-- Lösung mit temp-tabelle als subselect
		select Name, Land, Anzahl
		from (
		select ft.Hersteller_ID as ID, Count(ft.Hersteller_ID) as Anzahl
		from Fahrzeugtyp ft
		group by ft.Hersteller_ID
		) temp
		join Fahrzeughersteller fh
		on fh.ID = temp.ID
		order by Name;


		-- so geht es auch
		select name, land, count(*) as anzahl
		from fahrzeugtyp inner join
		fahrzeughersteller
			on fahrzeugtyp.Hersteller_ID = fahrzeughersteller.id
		group by fahrzeugtyp.Hersteller_ID





	#. Lösung zu Übung 5

	   .. code-block:: sql

		-- mysql

		select YEAR(sf.Datum) as Jahr,
			SUBSTRING(Kennzeichen from 1 for locate('-', Kennzeichen)-1 )
			as Kreis, COUNT(*)
		from Zuordnung_SF_FZ zu
		right join Fahrzeug fz on fz.ID = zu.Fahrzeug_ID
		inner join Schadensfall sf on sf.ID = zu.Schadensfall_ID
		where zu.Schuldanteil > 50
		group by 1, 2
		order by 1, 2;



		-- firebird ???
		select extract(YEAR from Datum) as Jahr,
		SUBSTRING(Kennzeichen from 1 for POSITION(’-’, Kennzeichen)-1 ) as Kreis,
		COUNT(*)
		from Zuordnung_SF_FZ zu
		right join Fahrzeug fz on fz.ID = zu.Fahrzeug_ID
		inner join Schadensfall sf on sf.ID = zu.Schadensfall_ID
		where zu.Schuldanteil > 50
		group by 1, 2
		order by 1, 2;






		Erläuterungen: Die Tabelle der Zuordnungen ist kleiner als die diejenige der Fahrzeuge, und darauf bezieht sich die WHERE-Bedingung; deshalb ist sie als Haupttabelle am sinnvollsten. Wegen der Kennzeichen benötigen wir einen JOIN auf die Tabelle Fahrzeug. Wegen des Datums des Schadensfalls für die Gruppierung nach Jahr benötigen wir einen JOIN auf die Tabelle Schadensfall.





Joins
-----

.. image:: figure/Visual_SQL_JOINS_V2.png
	:width: 400px


Ein besonderes Merkmal von relationalen Datenbanken und damit von SQL ist, dass die Informationen fast immer über mehrere Tabellen verteilt sind und bei Abfragen in der Ergebnismenge zusammengeführt werden müssen. Dieses Kapitel gibt einen Überblick über die Möglichkeiten dazu; Einzelheiten stehen in den folgenden Kapiteln.


.. raw:: latex

         \newpage


Bitte beachten Sie bei allen SELECT-Befehlen, die mehrere Tabellen verwenden:

- Wenn ein Spaltenname in Bezug auf den gesamten SQL-Befehl eindeutig ist, genügt dieser Name.
- Wenn ein Spaltenname mehrfach vorkommt (wie ID), dann muss der Tabellenname vorangesetzt werden; der Spaltenname wird nach einem Punkt angefügt.

.. code-block:: sql

	SELECT
	Personalnummer as MitNr,
	Name, Vorname,
	Dienstwagen.ID, Kennzeichen, Fahrzeugtyp_ID as Typ
	FROM Mitarbeiter, Dienstwagen;


- Wegen der Übersichtlichkeit wird die Tabelle meistens auch dann bei jeder Spalte angegeben, wenn es wegen der ersten Regel nicht erforderlich wäre.

- Anstelle des Namens einer Tabelle kann überall auch ein Tabellen-Alias benutzt werden; dieser muss einmal hinter ihrem Namen (in der FROM- oder in der JOIN-Klausel) angegeben werden.

.. code-block:: sql

	SELECT
	mi.Personalnummer AS MitNr,
	mi.Name, mi.Vorname,
	dw.ID AS DIW, dw.Kennzeichen, dw.Fahrzeugtyp_ID AS Typ
	FROM Mitarbeiter mi, Dienstwagen dw;


.. index:: EQUI JOIN

**Verknüpfung über WHERE − EQUI JOIN**

Beim einfachsten Verfahren, mehrere Tabellen gleichzeitig abzufragen, stehen alle Tabellen in der FROM-Klausel; die WHERE-Klausel enthält neben den Auswahlbedingungen auch Bedingungen zur Verknüpfung der Tabellen.


**JOINs - der moderne Weg**

Beim „modernen“ Weg, mehrere Tabellen in einer gemeinsamen Abfrage zu verknüpfen, wird jede Tabelle in einer JOIN-Klausel aufgeführt; der ON-Parameter enthält die Verknüpfungsbedingung. Die WHERE-Klausel enthält nur die Auswahlbedingungen.

**OUTER JOIN - auch null-Werte zurückgeben**

Bei Abfragen mit einem „einfachen“ JOIN werden nicht alle Datensätze aufgeführt. Zeilen, zu denen es in der einen oder anderen Tabelle keine Verknüpfung gibt, fehlen im Ergebnis. Mit einem OUTER JOIN können auch solche „fehlenden“ Zeilen aufgeführt werden.

**Weitere Möglichkeiten von JOIN**

Als SELF JOIN wird eine Tabelle mit sich selbst verknüpft. Oft kommt es vor, dass man die Daten aus einer Tabelle erst bearbeiten möchte, bevor man sie mit einer anderen Tabelle verknüpft. Dazu gibt es die Möglichkeit einer „Inline-View“.

.. raw:: latex

         \newpage

.. index:: EQUI JOIN


WHERE - EQUI JOIN
^^^^^^^^^^^^^^^^^

Der einfachste Weg, Tabellen zu verknüpfen, ist ein Befehl wie der folgende, in dem verschiedene Spalten aus zwei Tabellen zusammengefasst werden. Aber das Ergebnis sieht reichlich seltsam aus.

.. code-block:: sql

	select mi.Personalnummer as MitNr, mi.Name, mi.Vorname, dw.ID as DIW,
		dw.Kennzeichen, dw.Fahrzeugtyp_ID as Typ
	FROM Mitarbeiter mi, Dienstwagen dw;


::

	MITNR NAME       VORNAME      DIW KENNZEICHEN Typ
	10001 Müller     Kurt          1   DO-WB 421   14
	10002 Schneider  Daniela       1   DO-WB 421   14
	20001 Meyer      Walter        1   DO-WB 421   14
	20002 Schmitz    Michael       1   DO-WB 421   14
	30001 Wagner     Gaby          1   DO-WB 421   14
	30002 Feyerabend Werner        1   DO-WB 421   14
	40001 Langmann   Matthias      1   DO-WB 421   14
	40002 Peters     Michael       1   DO-WB 421   14
	/* usw. */
	10001 Müller    Kurt           2   DO-WB 422   14
	10002 Schneider Daniela        2   DO-WB 422   14
	20001 Meyer     Walter         2   DO-WB 422   14
	20002 Schmitz   Michael        2   DO-WB 422   14
	/* usw. */


Tatsächlich erzeugt dieser Befehl das „kartesische Produkt“ der beiden Tabellen:
Jeder Datensatz der einen Tabelle wird (mit den gewünschten Spalten) mit jedem
Datensatz der anderen Tabelle verbunden. Das sieht also so aus, als wenn alle
Dienstwagen zu jedem Mitarbeiter gehören würden, was natürlich Quatsch ist.
Diese Variante ist also in aller Regel sinnlos (wenn auch syntaktisch korrekt).

Sinnvoll wird die vorstehende Abfrage durch eine kleine Ergänzung. Was will man
denn eigentlich wissen?

::

	Hole Spalten der Tabelle Mitarbeiter
	sowie Spalten der Tabelle Dienstwagen
	wobei die Mitarbeiter_ID eines Dienstwagens gleich ist
	      der ID eines Mitarbeiters

Die obige Abfrage muss nur minimal erweitert werden:

.. code-block:: sql

	SELECT mi.Personalnummer AS MitNr,
	mi.Name, mi.Vorname,
	dw.ID AS DIW, dw.Kennzeichen, dw.Fahrzeugtyp_ID AS Typ
	FROM Mitarbeiter mi, Dienstwagen dw
	WHERE dw.Mitarbeiter_ID = mi.ID
	order by MitNr;

::

	MITNR    NAME         VORNAME     DIW KENNZEICHEN TYP
	100001   Grosser      Horst        10 DO-WB 4210   14
	10001    Müller       Kurt          1 DO-WB 421    14
	110001   Eggert       Louis        11 DO-WB 4211   14
	120001   Carlsen      Zacharias    12 DO-WB 4212   14
	20001    Meyer        Walter        2 DO-WB 422    14
	30001    Wagner       Gaby          3 DO-WB 423    14
	40001    Langmann     Matthias      4 DO-WB 424    14
	50001    Pohl         Helmut        5 DO-WB 425    14
	50002    Braun        Christian    14 DO-WB 352     2
	50003    Polovic      Frantisek    15 DO-WB 353     3
	50004    Kalman       Aydin        16 DO-WB 354     4
	/* usw. */




In der gleichen Weise können auch mehr als zwei Tabellen verknüpft werden.

.. admonition:: Aufgabe

	 Gesucht wird für jeden Fahrzeughersteller (mit Angabe von ID und Name) und jedes Jahr die Summe der Schadenshöhe aus der Tabelle Schadensfall.


	 .. code-block:: sql

	 	SELECT fh.ID AS Hersteller_ID,
		fh.Name AS Hersteller_Name,
		EXTRACT(YEAR FROM sf.Datum) AS Jahr,
		SUM(sf.Schadenshoehe) AS Schadenssumme
		FROM Schadensfall sf, Zuordnung_SF_FZ zu,
		Fahrzeug fz, Fahrzeugtyp ft, Fahrzeughersteller fh
		where sf.ID = zu.Schadensfall_ID
		and fz.ID = zu.Fahrzeug_ID
		and ft.ID = fz.Fahrzeugtyp_ID
		and fh.ID = ft.Hersteller_ID
		GROUP BY Hersteller_ID, Hersteller_Name, Jahr
		ORDER BY Jahr, Hersteller_ID;


Je mehr Kombinationen benötigt werden, desto unübersichtlicher wird diese Konstruktion. Dabei enthält die WHERE-Klausel bisher nur die Verknüpfungen zwischen den Tabellen, aber noch keine Suchbedingungen wie hier:

.. code-block:: sql

	select ... from ... where ...
	and Jahr in [2006, 2007, 2008]
	and fhe.Land in ['Schweden', 'Norwegen', 'Finnland']
	order by Jahr, Hersteller_ID;

Das führt außerdem dazu, dass die WHERE-Klausel sachlich gewünschte Suchbedingungen und logisch benötigte Verknüpfungsbedingungen vermischt.

.. raw:: latex

         \newpage


**Übungen**

Bei den folgenden Abfragen beziehen wir uns auf den Bestand der Beispieldatenbank im „Anfangszustand“: die Tabellen Versicherungsvertrag, Fahrzeug, Mitarbeiter mit jeweils etwa 28 Einträgen und Versicherungsnehmer mit etwa 26 Einträgen.

.. Admonition:: Aufgaben

	- Erstellen Sie eine Abfrage zur Tabelle Versicherungsvertrag, die nur die wichtigsten Informationen (einschließlich der IDs auf andere Tabellen) enthält. Wie viele Einträge zeigt die Ergebnismenge an?

	- Erweitern Sie die Abfrage von Aufgabe 1, sodass anstelle der Versicherungsnehmer_ID dessen Name und Vorname angezeigt werden, und verzichten Sie auf eine WHERE-Klausel. Wie viele Einträge zeigt die Ergebnismenge an?

	- Erweitern Sie die Abfrage von Aufgabe 2, sodass anstelle der Fahrzeug_ID das Kennzeichen und anstelle der Mitarbeiter_ID dessen Name und Vorname angezeigt werden, und verzichten Sie auf eine WHERE-Klausel. Wie viele Einträge zeigt die Ergebnismenge an?

	- Erweitern Sie die Abfrage von Aufgabe 2, sodass Name und Vorname des Versicherungsnehmers genau zu einem jeden Vertrag passen. Wie viele Einträge zeigt die Ergebnismenge an?

	- Erweitern Sie die Abfrage von Aufgabe 3, sodass Name und Vorname des Mitarbeiters sowie das Fahrzeug-Kennzeichen genau zu einem jeden Vertrag passen. Wie viele Einträge zeigt die Ergebnismenge an ?

	- Erweitern Sie die Abfrage von Aufgabe 5, sodass die ausgewählten Zeilen
          den folgenden Bedingungen entsprechen:

	  - Es geht ausschließlich um Eigene Kunden.

	  - Vollkasko-Verträge sollen immer angezeigt werden, ebenso Fahrzeuge aus dem Kreis Recklinghausen 'RE'.

	  - Teilkasko-Verträge sollen angezeigt werden, wenn sie nach 1990 abgeschlossen wurden.

	  - Haftpﬂicht-Verträge sollen angezeigt werden, wenn sie nach 1985 abgeschlossen wurden.

	  Wie viele Einträge zeigt die Ergebnismenge an?


.. only:: html

	**Lösungen**

	.. Admonition:: Lösungen

		- Erstellen Sie eine Abfrage zur Tabelle Versicherungsvertrag, die nur die wichtigsten Informationen (einschließlich der IDs auf andere Tabellen) enthält. Wie viele Einträge zeigt die Ergebnismenge an ?

		.. code-block:: sql

			/*Es werden 28 Zeilen angezeigt.*/
			SELECT Vertragsnummer, Abschlussdatum, Art,
			Versicherungsnehmer_ID, Fahrzeug_ID, Mitarbeiter_ID
			from Versicherungsvertrag

		- Erweitern Sie die Abfrage von Aufgabe 1, sodass anstelle der Versicherungsnehmer_ID dessen Name und Vorname angezeigt werden, und verzichten Sie auf eine WHERE-Klausel. Wie viele Einträge zeigt die Ergebnismenge an?

		.. code-block:: sql

			/*Es werden etwa 728 Zeilen angezeigt.*/
			SELECT vv.Vertragsnummer, vv.Abschlussdatum, vv.Art,
			vn.Name, vn.Vorname,
			Fahrzeug_ID,
			Mitarbeiter_ID
			from Versicherungsvertrag vv, Versicherungsnehmer vn;

		- Erweitern Sie die Abfrage von Aufgabe 2, sodass anstelle der Fahrzeug_ID das Kennzeichen und anstelle der Mitarbeiter_ID dessen Name und Vorname angezeigt werden, und verzichten Sie auf eine WHERE-Klausel. Wie viele Einträge zeigt die Ergebnismenge an?

		.. code-block:: sql

			/*Es werden etwa 570 752 Zeilen angezeigt.*/
			SELECT vv.Vertragsnummer, vv.Abschlussdatum, vv.Art,
			vn.Name, vn.Vorname,
			fz.Kennzeichen,
			mi.Name, mi.Vorname
			from Versicherungsvertrag vv, Versicherungsnehmer vn,
			Fahrzeug fz, Mitarbeiter mi;


		- Erweitern Sie die Abfrage von Aufgabe 2, sodass Name und Vorname des Versicherungsnehmers genau zu einem jeden Vertrag passen. Wie viele Einträge zeigt die Ergebnismenge an?

		.. code-block:: sql

			/*Es werden etwa 28 Zeilen angezeigt.*/
			SELECT vv.Vertragsnummer, vv.Abschlussdatum, vv.Art,
			vn.Name, vn.Vorname,
			Fahrzeug_ID,
			Mitarbeiter_ID
			from Versicherungsvertrag vv, Versicherungsnehmer vn
			where vn.ID = vv.Versicherungsnehmer_ID;




		- Erweitern Sie die Abfrage von Aufgabe 3, sodass Name und Vorname des Mitarbeiters sowie das Fahrzeug-Kennzeichen genau zu einem jeden Vertrag passen. Wie viele Einträge zeigt die Ergebnismenge an?

		.. code-block:: sql

			/*Es werden etwa 28 Zeilen angezeigt.*/
			SELECT vv.Vertragsnummer, vv.Abschlussdatum, vv.Art,
			vn.Name, vn.Vorname,
			fz.Kennzeichen,
			mi.Name, mi.Vorname
			from Versicherungsvertrag vv, Versicherungsnehmer vn,
			Fahrzeug fz, Mitarbeiter mi
			where vn.ID = vv.Versicherungsnehmer_ID
			and fz.ID = vv.Fahrzeug_ID
			and mi.ID = vv.Mitarbeiter_ID;

		- Erweitern Sie die Abfrage von Aufgabe 5, sodass die ausgewählten Zeilen den folgenden Bedingungen entsprechen:

		  - Es geht ausschließlich um Eigene Kunden.

		  - Vollkasko-Verträge sollen immer angezeigt werden, ebenso Fahrzeuge aus dem Kreis Recklinghausen 'RE'.

		  - Teilkasko-Verträge sollen angezeigt werden, wenn sie nach 1990 abgeschlossen wurden.

		  - Haftpﬂicht-Verträge sollen angezeigt werden, wenn sie nach 1985 abgeschlossen wurden.

		  Wie viele Einträge zeigt die Ergebnismenge an?

		  .. code-block:: sql

			/*Es werden etwa 19 Zeilen angezeigt. Die OR-Verknüpfungen könnten
			teilweise auch mit CASE geschrieben werden. */

			SELECT vv.Vertragsnummer, vv.Abschlussdatum, vv.Art,
				vn.Name, vn.Vorname,
				fz.Kennzeichen,
				mi.Name, mi.Vorname
			from Versicherungsvertrag vv, Versicherungsnehmer vn, Fahrzeug fz, Mitarbeiter mi
			where vn.ID = vv.Versicherungsnehmer_ID
			and fz.ID = vv.Fahrzeug_ID
			and mi.ID = vv.Mitarbeiter_ID
			and vn.Eigener_kunde = 'J'
			and ( ( vv.Art = 'HP' and vv.Abschlussdatum > '31.12.1985' )
			or ( vv.Art = 'TK' and vv.Abschlussdatum > '31.12.1990' )
			OR ( vv.Art = 'VK' )
			or ( fz.Kennzeichen STARTING WITH 'RE-' ) );


.. raw:: latex

         \newpage


.. index:: JOIN, INNER JOIN, OUTER JOIN

JOIN
^^^^


Um Tabellen sinnvoll miteinander zu verknüpfen (= verbinden, engl. join), wurde die JOIN-Klausel für den SELECT-Befehl mit folgender Syntax eingeführt.

.. code-block:: sql

	SELECT <spaltenliste>
	FROM <haupttabelle>
	[<join-typ>] JOIN <verknüpfte tabelle> ON <bedingung>


Als <join-typ> stehen zur Verfügung:

- **[INNER] JOIN**, auch Equi-Join genannt, ist eine Verknüpfung „innerhalb“ zweier Tabellen, d. h. ein Teil des kartesischen Produkts, bei dem ein Wert in beiden Tabellen vorhanden ist.

- **OUTER JOIN** bezeichnet Verknüpfungen, bei denen auch Datensätze geliefert
werden, für die eine Vergleichsbedingung nicht erfüllt ist.
- LEFT JOIN, RIGHT JOIN, FULL JOIN bezeichnen Spezialfälle von OUTER JOIN, je nachdem in welcher Tabelle ein gesuchter Wert fehlt.

Als <bedingung> wird normalerweise nur eine Übereinstimmung (also eine Gleichheit) zwischen zwei Tabellen geprüft, auch wenn jede Kombination von Bedingungen erlaubt ist. Genauer: es geht um die Gleichheit von Werten je einer Spalte in zwei Tabellen.

Auch mehrere Verknüpfungen sind möglich, entweder direkt hintereinander:

.. code-block:: sql

	SELECT <spaltenliste>
	FROM <haupttabelle>
	[<join-typ>] JOIN <zusatztabelle1> ON <bedingung1>
	[<join-typ>] JOIN <zusatztabelle2> ON <bedingung2>
	[<join-typ>] JOIN <zusatztabelle3> ON <bedingung3>


oder durch Klammern gegliedert:

.. code-block:: sql


	SELECT <spaltenliste>
	FROM <haupttabelle>
	[<join-typ>] JOIN
	( <zusatztabelle1>
	[<join-typ>] JOIN
	( <zusatztabelle2>
	[<join-typ>] JOIN <zusatztabelle3> ON <bedingung3>
	) ON <bedingung2>
	) ON <bedingung1>

Bitte beachten Sie dabei genau, wo und wie die Klammern und die dazugehörigen ON-Bedingungen gesetzt werden. Beide Varianten können unterschiedliche Ergebnisse liefern − abhängig vom JOIN-Typ und dem Zusammenhang zwischen den Tabellen.


.. raw:: latex

         \newpage

.. index:: INNER JOIN


INNER JOIN
~~~~~~~~~~

Der INNER JOIN gibt lediglich die Datensätze aus, bei denen die Werte in den Verknüpfungsspalten übereinstimmen, d.h. er bezieht sich auf die Schnittmenge

.. Admonition:: Aufgabe

	Zeige alle Mitarbeiter mit den dazugehörigen Dienstwagen

	.. figure:: figure/inner_join_dienstwagen_mitarbeiter.png


	.. code-block:: sql

		SELECT mi.Personalnummer as MitNr,mi.Name, mi.Vorname,
			dw.ID, dw.Kennzeichen, dw.Fahrzeugtyp_ID as Typ
		FROM Mitarbeiter mi JOIN Dienstwagen dw
			ON dw.Mitarbeiter_ID = mi.ID
		ORDER BY MitNr;


Er kann mit beliebigen WHERE-Statements erweitert werden.

.. Admonition:: Aufgabe

	Zeige alle Mitarbeiter und deren Autokennzeichen, die als Denstwagen einen Mercedes fahren.

	.. code-block:: sql

                /*Vermischung von join und where; geht, ist aber nicht schön*/
		select mi.Personalnummer as MitNr,
		mi.Name, mi.Vorname,
		dw.ID, dw.Kennzeichen, dw.Fahrzeugtyp_ID as Typ
		from Mitarbeiter mi
		join Dienstwagen dw
		on mi.ID = dw.Mitarbeiter_ID
		and dw.Fahrzeugtyp_ID in ( SELECT ft.ID
		from Fahrzeugtyp ft
		join Fahrzeughersteller fh
		on ft.Hersteller_ID = fh.ID
		and fh.Name = 'Mercedes-Benz' );


		/*Trennung von join und where */
		select mi.Personalnummer as MitNr,vmi.Name, mi.Vorname,
			dw.ID, dw.Kennzeichen, dw.Fahrzeugtyp_ID as Typ
		from Mitarbeiter mi join Dienstwagen dw
			on mi.ID = dw.Mitarbeiter_ID
		where dw.Fahrzeugtyp_ID in ( SELECT ft.ID
				from Fahrzeugtyp ft join Fahrzeughersteller fh
					on ft.Hersteller_ID = fh.ID
				where fh.Name = 'Mercedes-Benz');




 In den bisherigen Beispielen können die beiden Tabellen ohne weiteres vertauscht werden, da lediglich die Schnitmenge gesucht wird:

 .. code-block:: sql

	 	select mi.Personalnummer as MitNr, mi.Name, mi.Vorname,
		dw.ID, dw.Kennzeic hen, dw.Fahrzeugtyp_ID as Typ
		from Dienstwagen dw
		join Mitarbeiter mi
		on mi.ID = dw.Mitarbeiter_ID
		where dw.Fahrzeugtyp_ID in ( SELECT ft.ID
		from Fahrzeugtyp ft
		join Fahrzeughersteller fh
		on ft.Hersteller_ID = fh.ID
		where fh.Name = 'Mercedes-Benz')
		and mi.Name like 'M%';

Die Haupttabelle kann nach folgenden Überlegungen gewählt werden:
- Es sollte die Tabelle sein, die die „wichtigste“ bei der Abfrage ist.
- Es sollte diejenige mit den größten Einschränkungen sein; das beschleunigt die Abfrage besonders stark.


**Verknüpfungen über mehr als zwei Tabellen**

Wenn mehrere Tabellen miteinander verbunden werden, werden die einzelnen JOIN-Anweisungen und deren ON-Bedingungen hintereinander in der passenden Verknüpfung angeordnet.

.. Admonition:: Beispiel

	Gesucht wird für jeden Fahrzeughersteller (mit Angabe von ID und Name) und jedes Jahr die Summe der Schadenshöhe aus der Tabelle Schadensfall.

	.. code-block:: sql


			SELECT fh.ID AS Hersteller_ID,
			fh.Name AS Hersteller_Name,
			EXTRACT(YEAR FROM sf.Datum) AS Jahr,
			SUM(sf.Schadenshoehe) AS Schadenssumme
			FROM Schadensfall sf
			JOIN Zuordnung_SF_FZ zu ON sf.ID = zu.Schadensfall_ID
			JOIN Fahrzeug fz ON fz.ID = zu.Fahrzeug_ID
			JOIN Fahrzeugtyp ft ON ft.ID = fz.Fahrzeugtyp_ID
			JOIN Fahrzeughersteller fh ON fh.ID = ft.Hersteller_ID
			GROUP BY Hersteller_ID, Hersteller_Name, Jahr
			ORDER BY Jahr, Hersteller_ID;

.. raw:: latex

         \newpage

**Übungen zu Inner Join**

.. Admonition:: Aufgabe

	#. Welche der folgenden Aussagen sind wahr, welche falsch, welche sinnvoll?

	   1. Der INNER JOIN liefert das kartesische Produkt zwischen den Tabellen.
	   2. LEFT JOIN ist ein Spezialfall von OUTER JOIN.
	   3. Für einen JOIN ist dies eine zulässige Verknüpfungsbedingung:
	   	.. code-block:: sql

	   		ON Fahrzeug.ID >= Versicherungsvertrag.Fahrzeug_ID
	   4. Eine Einschränkung auf die mit JOIN verknüpfte Tabelle gehört in die ON- Klausel:
	   	.. code-block:: sql

	   		... FROM Zuordnung_SF_FZ zu JOIN Schadensfall sf
	   		ON sf.ID = zu.Schadensfall_ID AND YEAR(sf.Datum) = 2008;

	#. Erläutern Sie, was am folgenden Befehl falsch oder äußerst ungünstig ist.
           Es handelt sich um diese Abfrage:

           Gesucht sind die Schadensfälle des Jahres 2008. Zu jedem Schadensfall sind die beteiligten Fahrzeuge, der Schadensanteil sowie die Versicherungsdaten des Fahrzeugs (einschließlich Name des Halters) anzugeben.

           .. code-block:: sql
           		:linenos:

           	SELECT Datum, SUBSTRING(Ort from 1 for 30) as Ort, Schadenshoehe,
           	zu.Schadenshoehe,
           	fz.Kennzeichen,
           	Vertragsnummer as Vertrag, Abschlussdatum, Art,
           	vn.Name as VN-Name, vn.Vorname as VN-Vorname
           	from Schadensfall sf
           	join Zuordnung_SF_FZ zu on ID = zu.Schadensfall_ID
           	join Fahrzeug fz on ID = zu.Fahrzeug_ID
           	join Versicherungsnehmer vn on ID = vv.Versicherungsnehmer_ID
           	join Versicherungsvertrag vv on vv.Fahrzeug_ID = zu.Fahrzeug_ID
           	where YEAR (Datum) = 2008
           	order by Schadensfall_ID, Fahrzeug_ID;

        #. Erstellen Sie eine Abfrage zur Tabelle Versicherungsvertrag mit den wichtigsten
           Informationen (einschließlich der IDs auf andere Tabellen). Beim Versicherungsnehmer sollen dessen Name und Vorname angezeigt werden. Es werden nur Verträge ab 1990 gesucht.

        #. Erweitern Sie die obige Abfrage, sodass Name und Vorname des Mitarbeiters sowie
           das Fahrzeug-Kennzeichen eines jeden Vertrags angezeigt werden.

        #. Ändern Sie die obige Abfrage so, dass die ausgewählten Zeilen den folgenden
           Bedingungen entsprechen:

      	   - Es geht ausschließlich um Eigene Kunden.
      	   - Vollkasko-Verträge sollen immer angezeigt werden, ebenso Fahrzeuge aus dem Kreis Recklinghausen 'RE'.
      	   - Teilkasko-Verträge sollen angezeigt werden, wenn sie nach 1990 abgeschlossen wurden.
      	   - Haftpﬂicht-Verträge sollen angezeigt werden, wenn sie nach 1985 abgeschlossen wurden.


.. only:: html


	**Lösungen zu Inner Join**


	Lösung zu Übung 1
		1. Falsch; es liefert einen Teil des kartesischen Produkts, der durch die ON- Bedingung bestimmt wird.
		2. Richtig.
		3. Diese Bedingung ist zulässig, aber nicht sinnvoll. JOIN-ON passt in der Regel nur für Gleichheiten.
		4. Diese Bedingung ist zulässig. Besser ist es aber, eine Einschränkung der Auswahl in die WHERE-Klausel zu setzen.

	Lösung zu Übung 2

	Richtig ist beispielsweise die folgende Version. Als Haupttabelle wurde wegen der WHERE-Klausel die Tabelle Schadensfall gewählt; wegen der Reihenfolge der Verknüpfungen wäre auch Zuordnung_SF_FZ als Haupttabelle geeignet.

	.. code-block:: sql

		SELECT sf.Datum, SUBSTRING(sf.Ort from 1 for 30) as Ort, sf.Schadenshoehe,
		zu.Schadenshoehe as Teilschaden,
		fz.Kennzeichen,
		vv.Vertragsnummer as Vertrag, vv.Abschlussdatum, vv.Art,
		vn.Name as VN_Name, vn.Vorname as VN_Vorname
		from Schadensfall sf
		join Zuordnung_SF_FZ zu on sf.ID = zu.Schadensfall_ID
		join Fahrzeug fz on fz.ID = zu.Fahrzeug_ID
		join Versicherungsvertrag vv on fz.ID = vv.Fahrzeug_ID
		join Versicherungsnehmer vn on vn.ID = vv.Versicherungsnehmer_ID
		where EXTRACT(YEAR from sf.Datum) = 2008
		order by zu.Schadensfall_ID, zu.Fahrzeug_ID;

	Die Variante aus der Aufgabenstellung enthält folgende Problemstellen:

	- Zeile 1: Der Tabellen-Alias sf fehlt bei Schadenshoehe und bei Ort. Bei Datum fehlt
		   er auch, aber das ist kein Problem, weil es diese Spalte nur bei dieser Tabelle gibt.
	- Zeile 2: Diese Spalte sollte einen Spalten-Alias bekommen wegen der abweichenden
		   Bedeutung zu sf.Schadenshoehe.
	- Zeile 4: Es ist schöner, auch hier mit einem Tabellen-Alias zu arbeiten.
	- Zeile 5: Der Bindestrich in der Bezeichnung des Spalten-Alias wird nicht bei allen
		   DBMS akzeptiert.
	- Zeile 7, 8, 9: Zur Spalte ID ist jeweils die Tabelle anzugeben, ggf. mit dem Alias.
		   Die JOIN-ON-Bedingung bezieht sich nicht automatisch auf diese Spalte und diese Tabelle.
	- Zeile 9, 10: In Zeile 9 ist die Tabelle Versicherungsvertrag vv noch nicht bekannt.
		   Wegen der Verknüpfungen ist zuerst Zeile 10 zu verwenden, danach Zeile 9.
		   Die Verknüpfung über vv.Fahrzeug_ID = zu.Fahrzeug_ID ist nicht glücklich (wenn auch korrekt); besser ist der Bezug auf die direkt zugeordnete Tabelle Fahrzeug und deren PrimaryKey, nämlich ID.
	- Zeile 11: Es ist klarer, auch hier den Tabellen-Alias sf zu verwenden.
	- Zeile 12: Der Tabellen-Alias zu fehlt bei beiden Spalten. Bei Fahrzeug_ID ist er
		    erforderlich (doppelte Verwendung bei vv), bei Schadensfall_ID sinnvoll.

	Lösung zu Übung 3

	.. code-block:: sql

		SELECT Vertragsnummer, Abschlussdatum, Art,
		Name, Vorname,
		Fahrzeug_ID,
		Mitarbeiter_ID
		from Versicherungsvertrag vv
		join Versicherungsnehmer vn on vn.ID = vv.Versicherungsnehmer_ID
		where vv.Abschlussdatum >= '01.01.1990';

	Lösung zu Übung 4

	.. code-block:: sql

		SELECT vv.Vertragsnummer as Vertrag, vv.Abschlussdatum, vv.Art,
		vn.Name as VN_Name, vn.Vorname as VN_Vorname,
		fz.Kennzeichen,
		mi.Name as MI_Name, mi.Vorname as MI_Vorname
		from Versicherungsvertrag vv
		join Versicherungsnehmer vn on vn.ID = vv.Versicherungsnehmer_ID
		join Fahrzeug fz on fz.ID = vv.Fahrzeug_ID
		join Mitarbeiter mi on mi.ID = vv.Mitarbeiter_ID
		where vv.Abschlussdatum >= '01.01.1990';

	Lösung zu Übung 5

	.. code-block:: sql

		SELECT vv.Vertragsnummer as Vertrag, vv.Abschlussdatum, vv.Art,
		vn.Name as VN_Name, vn.Vorname as VN_Vorname,
		fz.Kennzeichen,
		mi.Name as MI_Name, mi.Vorname as MI_Vorname
		from Versicherungsvertrag vv
		join Versicherungsnehmer vn on vn.ID = vv.Versicherungsnehmer_ID
		join Fahrzeug fz on fz.ID = vv.Fahrzeug_ID
		join Mitarbeiter mi on mi.ID = vv.Mitarbeiter_ID
		where vn.Eigener_kunde = 'J'
		and ( ( vv.Art = 'HP' and vv.Abschlussdatum > '31.12.1985' )
		or ( vv.Art = 'TK' and vv.Abschlussdatum > '31.12.1990' )
		OR ( vv.Art = 'VK' )
		or ( fz.Kennzeichen LIKE 'RE-%' ) );



.. raw:: latex

         \newpage

.. index:: OUTER JOIN

OUTER JOIN
~~~~~~~~~~

Bei den Abfragen im vorigen Kapitel nach "alle Mitarbeiter und ihre Dienstwagen" werden nicht alle Mitarbeiter aufgeführt, weil in der Datenbank nicht für alle Mitarbeiter ein Dienstwagen registriert ist. Ebenso gibt es einen Dienstwagen, der keinem bestimmten Mitarbeiter zugeordnet ist. Mit einem OUTER JOIN werden auch Mitarbeiter ohne Dienstwagen oder Dienst-
wagen ohne Mitarbeiter aufgeführt.


Die Syntax entspricht derjenigen von JOIN allgemein. Wegen der speziellen Bedeutung sind die Tabellen nicht gleichberechtigt, sondern werden begrifﬂich unterschieden:

.. code-block:: sql

		SELECT <spaltenliste>
		FROM <linke tabelle>
		[<join-typ>] JOIN <rechte tabelle> ON <bedingung>

Das Wort OUTER kann entfallen und wird üblicherweise nicht benutzt, weil durch die Begriffe LEFT, RIGHT, FULL bereits ein OUTER JOIN gekennzeichnet wird. Die Begriffe <linke tabelle> und <rechte tabelle> beziehen sich auf die beiden Tabellen bezüglich der normalen Lesefolge: Wir lesen von links nach rechts, also ist die unter FROM genannte Tabelle die <linke Tabelle> (bisher <Haupttabelle> genannt) und die unter JOIN genannte Tabelle die <rechte Tabelle> (bisher <Zusatztabelle> genannt). Bei Verknüpfungen mit mehreren Tabellen ist ebenfalls die unter JOIN genannte Tabelle die <rechte Tabelle>; die unmittelbar vorhergehende Tabelle ist die <linke Tabelle>.


.. raw:: latex

         \newpage

**LEFT JOIN**

Dieser JOIN liefert alle Datensätze der linken Tabelle, ggf. unter Berücksichtigung der WHERE-Klausel. Aus der rechten Tabelle werden nur diejenigen Datensätze übernommen, die nach der Verknüpfungsbedingung passen.

.. code-block:: sql

	SELECT <spaltenliste>
	FROM <linke Tabelle>
	LEFT [OUTER] JOIN <rechte Tabelle> ON <bedingung>;

Für unser Beispiel sieht das dann so aus:


.. Admonition:: Aufgabe

	Hole alle Mitarbeiter und (sofern vorhanden) die Angaben zum Dienstwagen.

	.. code-block:: sql

		SELECT mi.Personalnummer AS MitNr,
		mi.Name, mi.Vorname,
		dw.ID AS DIW, dw.Kennzeichen, dw.Fahrzeugtyp_ID AS Typ
		FROM Mitarbeiter mi
		LEFT JOIN Dienstwagen dw ON dw.Mitarbeiter_ID = mi.ID;

	::

             MITNR NAME          VORNAME      DIW KENNZEICHEN TYP
             30001 Wagner         Gaby         3  DO-WB 423   14
             30002 Feyerabend     Werner
             40001 Langmann       Matthias     4 DO-WB 424    14
             40002 Peters         Michael
             50001 Pohl           Helmut       5 DO-WB 425    14
             50002 Braun          Christian   14 DO-WB 352     2
             50003 Polovic        Frantisek   15 DO-WB 353     3
             50004 Kalman         Aydin       16 DO-WB 354     4
             60001 Aagenau        Karolin      6 DO-WB 426    14
             60002 Pinkart        Petra

Beim Vertauschen der beiden Tabellen erhält man  alle Dienstwagen und dazu die passenden Mitarbeiter.

.. code-block:: sql


	SELECT mi.Personalnummer AS MitNr,
	mi.Name, mi.Vorname,
	dw.ID AS DIW, dw.Kennzeichen, dw.Fahrzeugtyp_ID AS Typ
	FROM Dienstwagen dw
	LEFT JOIN Mitarbeiter mi ON dw.Mitarbeiter_ID = mi.ID;



::

	MITNR NAME      VORNAME       DIW KENNZEICHEN   TYP
        80001 Schindler Christina      8  DO-WB 428      14
        90001 Janssen   Bernhard       9  DO-WB 429      14
        10001 Eggert    Louis         11  DO-WB 4211     14
        120001 Carlsen   Zacharias    12  DO-WB 4212     14
			                                13  DO-WB 111      16
        50002 Braun     Christian     14  DO-WB 352       2
        50003 Polovic   Frantisek     15  DO-WB 353       3
        50004 Kalman    Aydin         16  DO-WB 354       4


.. raw:: latex

         \newpage

**RIGHT OUTER JOIN**

Dieser JOIN liefert alle Datensätze der rechten Tabelle, ggf. unter Berücksichtigung der WHERE-Klausel. Aus der linken Tabelle werden nur diejenigen Datensätze übernommen, die nach der Verknüpfungsbedingung passen.

.. code-block:: sql

	SELECT <spaltenliste>
	FROM <linke Tabelle>
	RIGHT [OUTER] JOIN <rechte Tabelle> ON <bedingung>;

Für unser Beispiel „Mitarbeiter und Dienstwagen“ sieht das dann so aus:

.. code-block:: sql

	SELECT mi.Personalnummer AS MitNr,
	mi.Name, mi.Vorname,
	dw.ID AS DIW, dw.Kennzeichen, dw.Fahrzeugtyp_ID AS Typ
	FROM Mitarbeiter mi
	RIGHT JOIN Dienstwagen dw ON dw.Mitarbeiter_ID = mi.ID;


::

	MITNR NAME      VORNAME     DIW KENNZEICHEN TYP
        80001 Schindler Christina    8  DO-WB 428    14
        90001 Janssen   Bernhard     9  DO-WB 429    14
       100001 Grosser   Horst       10  DO-WB 4210   14
       110001 Eggert    Louis       11  DO-WB 4211   14
       120001 Carlsen   Zacharias   12  DO-WB 4212   14
                                    13  DO-WB 111    16
        50002 Braun     Christian   14  DO-WB 352     2
        50003 Polovic   Frantisek   15  DO-WB 353     3
        50004 Kalman    Aydin       16  DO-WB 354     4


Das Ergebnis ist das Gleich wie beim LEFT JOIN. Bei genauerem Überlegen wird klar: Beim LEFT JOIN gibt es alle Datensätze der linken Tabelle mit Informationen der rechten Tabelle; nun haben wir die beiden Tabellen vertauscht. Beim RIGHT JOIN werden alle Datensätze der rechten Tabelle mit Daten der linken Tabelle verknüpft; das entspricht diesem Beispiel.



.. raw:: latex

         \newpage

**FULL OUTER JOIN**

Dieser JOIN liefert alle Datensätze beider Tabellen, ggf. unter Berücksichtigung der WHERE-Klausel. Wenn Datensätze nach der Verknüpfungsbedingung zusammenpassen, werden sie in einer Zeile angegeben; wo es keinen „Partner“ gibt, wird ein NULL-Wert angezeigt.

.. code-block:: sql

	SELECT <spaltenliste>
	FROM <linke Tabelle>
	FULL [OUTER] JOIN <rechte Tabelle> ON <bedingung>;

Für unser Beispiel sieht das dann so aus:

.. code-block:: sql

	SELECT mi.Personalnummer AS MitNr,
	mi.Name, mi.Vorname,
	dw.ID AS DIW, dw.Kennzeichen, dw.Fahrzeugtyp_ID AS Typ
	FROM Mitarbeiter mi
	FULL JOIN Dienstwagen dw ON dw.Mitarbeiter_ID = mi.ID;


::

        MITNR NAME       VORNAME   DIW KENNZEICHEN TYP
       100001 Grosser    Horst      10 DO-WB 4210   14
       110001 Eggert     Louis      11 DO-WB 4211   14
       120001 Carlsen    Zacharias  12 DO-WB 4212   14
                                    13 DO-WB 111    16
        50002 Braun      Christian  14 DO-WB 352     2
        50003 Polovic    Frantisek  15 DO-WB 353     3
        50004 Kalman     Aydin      16 DO-WB 354     4
        80002 Aliman     Zafer      17 DO-WB 382     2
        80003 Langer     Norbert    18 DO-WB 383     3
        80004 Kolic      Ivana      19 DO-WB 384     4
        10002 Schneider  Daniela
        20002 Schmitz    Michael
        30002 Feyerabend Werner
        40002 Peters     Michael

Auch hier wollen wir wieder die beiden Tabellen vertauschen:

.. code-block:: sql

	SELECT mi.Personalnummer AS MitNr,
	mi.Name, mi.Vorname,
	dw.ID AS DIW, dw.Kennzeichen, dw.Fahrzeugtyp_ID AS Typ
	FROM Dienstwagen dw
	FULL JOIN Mitarbeiter mi ON dw.Mitarbeiter_ID = mi.ID;


::

	MITNR   NAME         VORNAME    DIW KENNZEICHEN TYP
	80001   Schindler    Christina    8 DO-WB 428    14
	80002   Aliman       Zafer       17 DO-WB 382     2
	80003   Langer       Norbert     18 DO-WB 383     3
	80004   Kolic        Ivana       19 DO-WB 384     4
	90001   Janssen      Bernhard     9 DO-WB 429    14
	90002   Hinkel       Martina
	100001  Grosser      Horst       10 DO-WB 4210   14
	100002  Friedrichsen Angelina
	110001  Eggert       Louis       11 DO-WB 4211   14
	110002  Deiters      Gisela
	120001  Carlsen      Zacharias   12 DO-WB 4212   14
	120002  Baber        Yvonne
	                                 13 DO-WB 111    16


Bei detailliertem Vergleich des vollständigen Ergebnisses ergibt sich: Es ist gleich,
nur in anderer Reihenfolge. Das sollte nicht mehr verwundern.


.. raw:: latex

         \newpage

**Verknüpfung mehrerer Tabellen**

Alle bisherigen Beispiele kranken daran, dass als Typ des Dienstwagens nur die ID angegeben ist. Selbstverständlich möchte man die Typbezeichnung und den Hersteller lesen. Dazu müssen die beiden Tabellen Fahrzeugtyp und Fahrzeughersteller eingebunden werden. Beim INNER JOIN war das kein Problem; beim OUTER JOIN kann es anders aussehen.

.. Admonition:: Beispiel

	Erweitern wir dazu die Aufstellung "alle Dienstwagen zusammen mit den zugeordneten Mitarbeitern" um die Angabe zu den Fahrzeugen.

	.. code-block:: sql

		SELECT mi.Personalnummer AS MitNr,
		mi.Name, mi.Vorname,
		dw.ID AS DIW, dw.Kennzeichen, dw.Fahrzeugtyp_ID AS TypID,
		ft.Bezeichnung as Typ, ft.Hersteller_ID as FheID
		FROM Dienstwagen dw
		left JOIN Mitarbeiter mi ON dw.Mitarbeiter_ID = mi.ID
		join Fahrzeugtyp ft on dw.Fahrzeugtyp_ID = ft.ID;


::

       MITNR  NAME       VORNAME    DIW  KENNZEICHEN TYPID TYP    FHEID
       100001 Grosser     Horst      10  DO-WB 4210   14   A160     6
       110001 Eggert      Louis      11  DO-WB 4211   14   A160     6
       120001 Carlsen     Zacharias  12  DO-WB 4212   14   A160     6
                                     13  DO-WB 111    16   W211     6
        50002 Braun        Christian 14  DO-WB 352     2   Golf     1
        50003 Polovic      Frantisek 15  DO-WB 353     3   Passat   1
        50004 Kalman       Aydin     16  DO-WB 354     4   Kadett   2


Der zweite JOIN wurde nicht genauer bezeichnet, ist also ein INNER JOIN. Das gleiche Ergebnis erhalten wir, wenn wir die Tabelle Fahrzeugtyp ausdrücklich als LEFT JOIN verknüpfen (bitte selbst ausprobieren!). Anders sieht es beim Versuch mit RIGHT JOIN oder FULL JOIN aus:

.. code-block:: sql

	SELECT mi.Personalnummer AS MitNr,
	mi.Name, mi.Vorname,
	dw.ID AS DIW, dw.Kennzeichen, dw.Fahrzeugtyp_ID AS TypID,
	ft.Bezeichnung as Typ, ft.Hersteller_ID as FheID
	FROM Dienstwagen dw
	left JOIN Mitarbeiter mi ON dw.Mitarbeiter_ID = mi.ID
	right | full join Fahrzeugtyp ft on dw.Fahrzeugtyp_ID = ft.ID;



::


      MITNR  NAME      VORNAME     DIW KENNZEICHEN TYPID TYP      FHEID
      80001  Schindler Christina    8  DO-WB 428     14  A160       6
      90001  Janssen   Bernhard     9  DO-WB 429     14  A160       6
     100001  Grosser   Horst       10  DO-WB 4210    14  A160       6
     110001  Eggert    Louis       11  DO-WB 4211    14  A160       6
     120001  Carlsen   Zacharias   12  DO-WB 4212    14  A160       6
                                                         W204       6
     .                             13  DO-WB 111     16  W211       6
                                                         Saab 9-3   8
                                                         S40        9
                                                         C30        9


Versuchen wir eine Erklärung:

Die beiden JOINs stehen sozusagen auf der gleichen Ebene; jede JOIN-Klausel wird für sich mit der Tabelle Dienstwagen verknüpft. An der Verknüpfung zwischen Dienstwagen und Mitarbeiter ändert sich nichts. Aber für die Fahrzeugtypen gilt:

- Das erste Beispiel benutzt einen INNER JOIN, nimmt also für jeden vorhandenen Dienstwagen genau "seinen" Typ.
- Wenn man stattdessen einen LEFT JOIN verwendet, erhält man alle vorhandenen Dienstwagen, zusammen mit den passenden Typen. Das ist faktisch identisch mit dem Ergebnis des INNER JOIN.
- Das zweite Beispiel benutzt einen RIGHT JOIN, das liefert alle registrierten Fahrzeugtypen und (soweit vorhanden) die passenden Dienstwagen.
- Wenn man stattdessen einen FULL JOIN verwendet, erhält man alle Kombinationen von Dienstwagen und Mitarbeitern, zusammen mit allen registrierten Fahrzeugtypen. Das ist faktisch identisch mit dem Ergebnis des RIGHT JOIN.

	

**Wann welcher JOIN**

Im untenstehender Grafik sind zwei Tabellen vorhanden:
Paare (also Eltern) und Kinder. Es gibt kinderlose Paare,
Paare mit Kindern und Waisenkinder. Wir wollen die Eltern und Kinder in Abfragen verknüpfen; bei den Symbolen steht der linke Kreis für die Tabelle Paare und der rechte Kreis für die Tabelle Kinder. Die schware Fläche beschreibt die jeweils gewünschte Ergebnismenge.


Schreiben Sie in die links leerstehenden Felder den jeweiligen Join-Begriff; in die rechts stehenden Felder das entsprechende SQl-Statement


.. figure:: figure/join_alle_fragen.png




Lösung:


.. figure:: figure/join_wann.png


Auswertungsreihenfolge von joins
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Gegeben sei folgende Datenbank:


.. code-block:: sql


	create table kunden(
		kundennr integer not null primary KEY,
	    nachname varchar(20),
	    vorname varchar(20),
	    strasse varchar(20),
	    plz varchar(5),
	    ort varchar(20)
	) engine = innodb;

	insert into kunden(
		kundennr, nachname, vorname, strasse, PLZ, ort)
	    values(123456, 'mustermann', 'max', 'musterweg 1', '12345', 'musterstadt');


	insert into kunden(
		kundennr, nachname, vorname, strasse, PLZ, ort)
	    values(123457, 'musterfrau', 'katrin', 'musterstrasse 1', '12345', 'musterstadt');

	insert into kunden(
		kundennr, nachname, vorname, strasse, PLZ, ort)
	    values(123458, 'Müller', 'lieschen', 'beispielweg 3', '23987', 'irgendwo');

	insert into kunden(
		kundennr, nachname, vorname, strasse, PLZ,  ort)
	    values(123459, 'Schmidt', 'hans', 'hauptstraße 2', '98765', 'anderswo') ;

	insert into kunden(
		kundennr, nachname, vorname, strasse, PLZ, ort)
	    values(123460, 'Becker', 'heinz', 'mustergasse 4', '12543', 'musterdorf') ;


	create table kreditkarten(
		kartennr integer not null,
	    firma varchar(20),
	    kundennr integer,
	    Ablaufdatum varchar(20)
	) engine=innodb;


	insert into kreditkarten(kartennr, firma, kundennr, ablaufdatum)
	 values(12345, 'Visa', 123457, '05/2011' );

	insert into kreditkarten(kartennr, firma, kundennr, ablaufdatum)
	 values(12346, 'Mastercard', 123459, '01/2012' );

	insert into kreditkarten(kartennr, firma, kundennr, ablaufdatum)
	 values(12347, 'American Express', 123459, '01/20111' );

	insert into kreditkarten(kartennr, firma, kundennr, ablaufdatum)
	 values(12348, 'Diners Club', 123458, '03/2012');

	 insert into kreditkarten(kartennr, firma, kundennr, ablaufdatum)
	 values(12349, 'Visa', 123458, '07/2011');


	 create table bestellungen_oktober(
	 kundennr integer,
	 bestellungsnr integer,
	 datum date
	 ) engine = innodb;

	 insert into bestellungen_oktober
	 (kundennr, bestellungsnr, datum)
	 values(123456, 987654, '2009-10-15');

	 insert into bestellungen_oktober
	 (kundennr, bestellungsnr, datum)
	 values(123456, 987655, '2009-10-16');

	 insert into bestellungen_oktober
	 (kundennr, bestellungsnr, datum)
	 values(123457, 987656, '2009-10-16');


	 create table positionen(
	 positionsnr integer,
	 bestellungsnr integer,
	 artikel varchar(20),
	 anzahl integer,
	 preis double

	 ) engine = innodb;

	 insert into positionen (
	    positionsnr,bestellungsnr, artikel ,
	    anzahl, preis)
	    values(10241, 987654, 'CD-Player', 2, 49.95);

	 insert into positionen (
	    positionsnr,bestellungsnr, artikel ,
	    anzahl, preis)
	    values(10242, 987654, 'DVD-Player', 3, 59.95);

	 insert into positionen (
	    positionsnr,bestellungsnr, artikel ,
	    anzahl, preis)
	    values(10243, 987654, 'CD xyz', 10, 19.95);

	 insert into positionen (
	    positionsnr,bestellungsnr, artikel ,
	    anzahl, preis)
	    values(10244, 987654, 'DVD abc', 5, 9.95);

	 insert into positionen (
	    positionsnr,bestellungsnr, artikel ,
	    anzahl, preis)
	    values(10245, 987655, 'CD-Player', 1, 51.20);

	 insert into positionen (
	    positionsnr,bestellungsnr, artikel ,
	    anzahl, preis)
	    values(10246, 987655, 'CD xyz extra', 20, 16.25);

	 insert into positionen (
	    positionsnr,bestellungsnr, artikel,
	    anzahl, preis)
	    values(10247, 987656, 'DVD-Player', 1, 64.95);



	create table vorteilsclub(
	 kndnr INTEGER,
	 clubnr integer,
	 kategorie integer
	) engine = innodb;

	insert into vorteilsclub(
		kndnr, clubnr, kategorie)
	    values(123458, 1214, 3);


	insert into vorteilsclub(
		kndnr, clubnr, kategorie)
	    values(123456, 1415, 1);


	insert into vorteilsclub(
		kndnr, clubnr, kategorie)
	    values(123460, 1616, 1);


siehe http://wiki.selfhtml.org/wiki/Artikel:DBMS_und_SQL/Fortgeschrittene_Jointechniken#Mehrere_Tabellen_mit_JOIN_verkn.C3.BCpfen_-_gleiche_Joinspalten








**Zusammenfassung**


- Mit dieser Verknüpfung werden auch Datensätze abgefragt und angezeigt, bei denen es in einer der Tabellen keinen zugeordneten Datensatz gibt.
- Mit einem LEFT JOIN erhält man alle Datensätze der linken Tabelle, ergänzt durch passende Angaben aus der rechten Tabelle.
- Mit einem RIGHT JOIN erhält man alle Datensätze der rechten Tabelle, ergänzt durch passende Angaben aus der linken Tabelle.
- Mit einem FULL JOIN erhält man alle Datensätze beider Tabellen, wenn möglich ergänzt durch passende Angaben aus der jeweils anderen Tabelle.
- Bei der Verknüpfung mehrerer Tabellen ist genau auf den JOIN-Typ und ggf. auf Klammerung zu achten.


.. raw:: latex

         \newpage

**Übungen OUTER JOIN**

#. Welche der folgenden Aussagen sind wahr, welche sind falsch?

   1. Um alle Mitarbeiter mit Dienstwagen aufzulisten, benötigt man einen LEFT OUTER JOIN.
   2. LEFT JOIN ist nur eine Kurzschreibweise für LEFT OUTER JOIN und hat keine zusätzliche
      inhaltliche Bedeutung.
   3. Ein LEFT JOIN von zwei Tabellen enthält alle Zeilen, die nach Auswahlbedingung in der
      linken Tabelle enthalten sind.
   4. Ein RIGHT JOIN von zwei Tabellen enthält nur noch diejenigen Zeilen, die nach der
      Verknüpfungsbedingung in der linken Tabelle enthalten sind.
   5. Wenn wir bei einer LEFT JOIN-Abfrage mit zwei Tabellen die beiden Tabellen vertauschen
      und stattdessen einen RIGHT JOIN verwenden, erhalten wir dieselben Zeilen in der Ergebnismenge.
   6. Wir erhalten dabei nicht nur dieselben Zeilen, sondern auch dieselbe Reihenfolge.


#. Was ist am folgenden SELECT-Befehl falsch und warum? Die Aufgabe dazu lautet:

    Gesucht werden Kombinationen von Fahrzeug-Kennzeichen und Fahrzeugtypen, wobei alle Typen aufgeführt werden sollen; es werden nur die ersten 20 Fahrzeuge nach ID benötigt.

    .. code-block:: sql

    	select Kennzeichen, Bezeichnung
    	from Fahrzeug fz
    	left join Fahrzeugtyp ft on fz.Fahrzeugtyp_ID = ft.ID
    	where fz.ID <= 20 ;

#. Gesucht werden alle registrierten Versicherungsgesellschaften und (soweit vorhanden) deren Kunden mit Name, Vorname.

#. Gesucht werden die Dienstwagen, deren Fahrzeugtypen sowie die Hersteller. Die Liste der 	 Typen soll vollständig sein.

#. Gesucht werden Kombinationen von Mitarbeitern und ihren Dienstwagen (einschl. Typ). Es geht um die Abteilungen 1 bis 5; auch nicht-persönliche Dienstwagen sollen aufgeführt werden.

#. Gesucht werden alle registrierten Versicherungsgesellschaften sowie alle Kunden mit Name, Vorname, soweit der Nachname mit 'S' beginnt.

#. Vertauschen Sie in der Lösung von Übung 5 die beiden Tabellen Mitarbeiter und Dienstwagen und erläutern Sie:
   1. Warum werden jetzt mehr Mitarbeiter angezeigt, und zwar auch solche ohne Dienstwagen?
   2. Warum fehlt jetzt der „nicht-persönliche“ Dienstwagen?

#. Gesucht werden Angaben zu den Mitarbeitern und den Dienstwagen. Beim Mitarbeiter sollen Name und Vorname angegeben werden, bei den Dienstwagen Bezeichnung und Name des Herstellers. Die Liste aller Fahrzeugtypen soll vollständig sein.

#. Ergänzen Sie die Lösung zur obenstehenden Übung insofern, dass nur Mitarbeiter der Abteilungen 1 bis 5 angezeigt werden; die Zeilen ohne Mitarbeiter sollen unverändert ausgegeben werden.


.. only:: html


	**Lösung OUTER JOIN**

	#. Die Aussagen 2, 3, 5 sind richtig, die Aussagen 1, 4, 6 sind falsch.
	#. Richtig ist folgender Befehl:

	   .. code-block:: sql

		select Kennzeichen, Bezeichnung
		from Fahrzeug fz
		right join Fahrzeugtyp ft on fz.Fahrzeugtyp_ID = ft.ID
		where fz.ID <= 20 or fz.ID is null;

	   Weil alle Typen aufgeführt werden sollen, wird ein RIGHT JOIN benötigt. Damit auch der Vermerk „es gibt zu einem Typ keine Fahrzeuge“ erscheint, muss die WHERE-Klausel um die IS NULL-Prüfung erweitert werden.
	#. Lösung

	   .. code-block:: sql

		select Bezeichnung, Name, Vorname
		from Versicherungsgesellschaft vg
		left join Versicherungsnehmer vn on vg.ID = vn.Versicherungsgesellschaft_ID
		order by Bezeichnung, Name, Vorname;

	#. Lösung

	   .. code-block:: sql

		select Kennzeichen, Bezeichnung, Name
		from Dienstwagen dw
		right join Fahrzeugtyp ft on ft.ID = dw.Fahrzeugtyp_ID
		inner join Fahrzeughersteller fh on fh.ID = ft.Hersteller_ID
		order by Name, Bezeichnung, Kennzeichen;

	#. Lösung

	   .. code-block:: sql

		SELECT mi.Name, mi.Vorname,
		dw.ID AS DIW, dw.Kennzeichen, dw.Fahrzeugtyp_ID AS Typ
		FROM Mitarbeiter mi
		RIGHT JOIN Dienstwagen dw ON dw.Mitarbeiter_ID = mi.ID
		where mi.Abteilung_ID <= 5 or mi.ID is null;

	   Die IS NULL-Prüfung wird wegen der „nicht-persönlichen“ Dienstwagen benötigt.

	#. Lösung

	   .. code-block:: sql

		SELECT Bezeichnung, Name, Vorname
		FROM Versicherungsgesellschaft vg
		full JOIN Versicherungsnehmer vn ON vg.id = vn.Versicherungsgesellschaft_id
		where vn.Name like 'S%' or vn.id is null
		ORDER BY Bezeichnung, Name, Vorname;

	#. Lösung

	   1. Bei einem RIGHT JOIN werden alle Einträge der rechten Tabelle angezeigt. "Rechts"
	      stehen jetzt die Mitarbeiter, also werden alle Mitarbeiter der betreffenden Abteilungen angezeigt.
	   2. Bei einem RIGHT JOIN werden die Einträge der linken Tabelle nur dann angezeigt, wenn
	      sie zu einem Eintrag der rechten Tabelle gehören. Der „nicht-persönliche“ Dienstwagen aus der linken Tabelle gehört aber zu keinem der Mitarbeiter.

	#. Lösung

	   .. code-block:: sql


		SELECT mi.Name, mi.Vorname,
		dw.Kennzeichen, ft.Bezeichnung, fh.Name as HST
		FROM Dienstwagen dw
		left join Mitarbeiter mi on mi.id = dw.Mitarbeiter_id
		right JOIN Fahrzeugtyp ft on ft.Id = dw.Fahrzeugtyp_id
		inner join Fahrzeughersteller fh on fh.Id = ft.Hersteller_id;

	   Hinweise: Die Reihenfolge der JOINs ist nicht eindeutig; der LEFT JOIN kann auch später kommen. Wichtig ist, dass die Verbindung Dienstwagen --- Fahrzeugtyp ein RIGHT JOIN ist (oder bei Vertauschung der Tabellen ein LEFT JOIN).

	#. Lösung

	   .. code-block:: sql

		SELECT mi.Name, mi.Vorname,
		dw.Kennzeichen, ft.Bezeichnung, fh.Name as HST
		FROM Dienstwagen dw
		left join Mitarbeiter mi on mi.id = dw.Mitarbeiter_id
		right JOIN Fahrzeugtyp ft on ft.Id = dw.Fahrzeugtyp_id
		inner join Fahrzeughersteller fh on fh.Id = ft.Hersteller_id
		where (mi.Abteilung_id <= 5) or (mi.id is null);




.. raw:: latex

         \newpage




.. index:: GROUP BY



.. index:: SUBSELECT

Unterabfragen
-----------------------

Immer wieder werden zur Durchführung einer Abfrage oder eines anderen Befehls Informationen benötigt, die zuerst durch eine eigene Abfrage geholt werden müssen. Solche „Unterabfragen“ werden in diesem Kapitel behandelt.

- Wenn eine Abfrage als Ergebnis einen einzelnen Wert liefert, kann sie anstelle eines Wertes benutzt werden.

- Wenn eine Abfrage als Ergebnis eine Liste von Werten liefert, kann sie anstelle einer solchen Liste benutzt werden.

- Wenn eine Abfrage eine Ergebnismenge, also etwas in Form einer Tabelle liefert, kann sie anstelle einer Tabelle benutzt werden.

Bitte beachten Sie, dass die Unterabfrage schon aus Übersichtlichkeitsgründen immer in Klammern gesetzt werden sollte, auch wenn ein DBMS das nicht verlangen immer sollte

Allgemeiner Hinweis: Unterabfragen arbeiten in vielen Fällen langsamer als andere Verfahren. Soweit es irgend möglich ist, versuchen Sie, eine der JOIN-Varianten vorzuziehen.

.. Admonition:: Beispiel

	Nenne alle Mitarbeiter der Abteilung „Schadensabwicklung“.

        - Lösung Teil 1: Hole die ID dieser Abteilung anhand des Namens.

        - Lösung Teil 2: Hole die Mitarbeiter dieser Abteilung unter Benutzung der
          gefundenen ID.

        .. code-block:: sql

        	select Personalnummer, Name, Vorname
		from Mitarbeiter
		where Abteilung_ID =
		( select ID from Abteilung
		where Kuerzel = ’ScAb’ );

	::


		PERSONALNUMMER NAME      VORNAME
		80001          Schindler Christina
		80002          Aliman    Zafer
		80003          Langer    Norbert
		80004          Kolic     Ivana

.. Admonition:: Beispiel 2

	 Nenne alle anderen Mitarbeiter der Abteilung, deren Leiterin Christina Schatzing ist.

	 - Lösung Teil 1: Hole die Abteilung_ID, die bei Christina Schatzing registriert ist.

	 - Lösung Teil 2: Hole die Mitarbeiter dieser Abteilung unter Benutzung der gefundenen Abteilung_ID.


         .. code-block:: sql

         	select Personalnummer, Name, Vorname
		from Mitarbeiter
		where ( Abteilung_ID = (
			SELECT Abteilung_ID
			from Mitarbeiter
			where ( Name = 'Schindler' )
			and ( Vorname = 'Christina' )
			and ( Ist_Leiter = 'J' )
			)
		) and ( Ist_Leiter = 'N' );


.. raw:: latex

         \newpage


Ergebnisse von Spaltenfunktionen
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Häuﬁg werden Ergebnisse von Aggregatfunktionen als Teil der WHERE-Klausel
benötigt.

.. Admonition:: Beispiel

	Hole die Schadensfälle mit unterdurchschnittlicher Schadenshöhe.

        - Lösung Teil 1: Berechne die durchschnittliche Schadenshöhe aller Schadensfälle.

        - Lösung Teil 2: Übernimm das Ergebnis als Vergleichswert in die eigentliche Abfrage.

        .. code-block:: sql

        	SELECT ID, Datum, Ort, Schadenshoehe
        	from Schadensfall
        	where Schadenshoehe < (
        		select AVG(Schadenshoehe) from Schadensfall
        	);


        Bestimme alle Schadensfälle, die von der durchschnittlichen Schadenshöhe eines Jahres maximal 300 € abweichen.


        - Lösung Teil 1: Bestimme den Durchschnitt aller Schadensfälle innerhalb eines Jahres.

        - Lösung Teil 2: Hole alle Schadensfälle, deren Schadenshöhe im betreffenden Jahr
              innerhalb des Bereichs „Durchschnitt plus/minus 300“ liegen.

        .. code-block:: sql

              select sf.ID, sf.Datum, sf.Schadenshoehe, EXTRACT(YEAR from sf.Datum) AS Jahr
              from Schadensfall sf
              where ABS(Schadenshoehe - (
              			select AVG(sf2.Schadenshoehe)
              			from Schadensfall sf2
              			where YEAR(sf2.Datum) = YEAR(sf.Datum)
              			)
              		) <= 300;


        Zuerst muss für jeden einzelnen Schadensfall aus sf das Jahr bestimmt werden. In der Unterabfrage, die in der inneren Klammer steht, wird für alle Schadensfälle des betreffenden Jahres die durchschnittliche Schadenshöhe bestimmt. Dieser Wert wird mit der aktuellen Schadenshöhe verglichen; dazu wird die ABS-Funktion benutzt, also der absolute Betrag der Differenz der beiden Werte.


        Dies ist ein Paradebeispiel dafür, wie Unterabfragen nicht benutzt werden sollen.
        Für jeden einzelnen Datensatz muss in der WHERE-Bedingung eine neue Unterabfrage gestartet werden − mit eigener WHERE-Klausel und Durchschnittsberechnung. Viel besser wäre eine der JOIN-Varianten.

        Weitere mögliche Lösungen (Lutz, 11FI1, 2013/14

        .. code-block:: sql


        	select beschreibung, schadenshoehe
		from schadensfall where
		schadenshoehe <= (
		select avg(schadenshoehe)
		from schadensfall) + 300
		and schadenshoehe >= (select avg(schadenshoehe)
		from schadensfall) - 300


		select beschreibung, schadenshoehe
		from schadensfall where
		schadenshoehe between (
		select avg(schadenshoehe)
		from schadensfall) - 300
		and (select avg(schadenshoehe)
		from schadensfall) + 300


		select @average:=avg(schadenshoehe) from schadensfall;
		select id from schadensfall where abs(schadenshoehe - @average) <= 300;





.. raw:: latex

         \newpage


Ergebnis als Liste mehrerer Werte
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Das Ergebnis einer Abfrage kann als Filter für die eigentliche Abfrage benutzt werden.

.. Admonition:: Beispiel

	Bestimme alle Fahrzeuge eines bestimmten Herstellers.

	- Lösung Teil 1: Hole die ID des gewünschten Herstellers.

	- Lösung Teil 2: Hole alle IDs der Tabelle Fahrzeugtyp zu dieser Hersteller-ID.

	- Lösung Teil 3: Hole alle Fahrzeuge, die zu dieser Liste von Fahrzeugtypen-IDs passen.

	.. code-block:: sql

		select ID, Kennzeichen, Fahrzeugtyp_ID as TypID
		from Fahrzeug
		where Fahrzeugtyp_ID in(
			select ID
			from Fahrzeugtyp
			where Hersteller_ID = (
				select ID
				from Fahrzeughersteller
				where Name = 'Volkswagen' ) );


	::

		ID KENNZEICHEN TYPID
		22 BOR-PQ 567    3
		23 BOR-RS 890    2

	Teil 1 der Lösung ist die „innere“ Klammer; dies ist das gleiche Verfahren wie im Abschnitt „Ergebnisse einfacher Abfragen“. Teil 2 der Lösung ist die "äußere" Klammer; Ergebnis ist eine Liste von IDs der Tabelle Fahrzeugtyp, die als Werte für den Vergleich der WHERE-IN-Klausel verwendet werden.

	Wenn im Ergebnis der Fahrzeugtyp als Text angezeigt werden soll, muss die Abfrage erweitert werden, weil die Bezeichnung in der Tabelle Fahrzeugtyp zu ﬁnden ist. Dafür kann diese Tabelle ein zweites Mal benutzt werde; es ist auch ein Verfahren möglich, wie es weiter unten erläutert wird.



.. Admonition:: Aufgabe

	Gib alle Informationen zu den Schadensfällen des Jahres 2008, die von der durchschnittlichen Schadenshöhe 2008 maximal 300 € abweichen.


	- Lösung Teil 1: Bestimme den Durchschnitt aller Schadensfälle innerhalb von 2008.

	- Lösung Teil 2: Hole alle IDs von Schadensfällen, deren Schadenshöhe innerhalb des
	                 Bereichs „Durchschnitt plus/minus 300“ liegen.

	- Lösung Teil 3: Hole alle anderen Informationen zu diesen IDs.

	.. code-block:: sql

		select *
		from Schadensfall
		where ID in ( SELECT ID
		from Schadensfall
		where ( ABS(Schadenshoehe - (
			   select AVG(sf2.Schadenshoehe)
			   from Schadensfall sf2
			   where YEAR(sf2.Datum) = 2008
			   )
		      ) <= 300 )
		and ( YEAR(Datum) = 2008 )
		);

	Diese Situation wird dadurch einfacher, dass das Jahr 2008 fest vorgegeben ist. Die innerste Klammer bestimmt als Teil 1 der Lösung die durchschnittliche Schadenshöhe dieses Jahres. Die nächste Klammer vergleicht diesen Wert (absolut gesehen) mit der Schadenshöhe eines jeden einzelnen Schadensfalls im Jahr 2008; alle „passenden“ IDs werden in der äußersten Klammer als Teil 2 der Lösung in einer weiteren Unterabfrage zusammengestellt. Diese Liste liefert die Werte für die eigentliche Abfrage.


.. raw:: latex

         \newpage


Ergebnis in Form einer Tabelle
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Das Ergebnis einer Abfrage kann in der Hauptabfrage überall dort eingesetzt werden, wo eine Tabelle vorgesehen ist. Die Struktur dieser Situation sieht so aus:

.. code-block:: sql


	SELECT <spaltenliste>
	FROM <haupttabelle>,
	( SELECT <spaltenliste>
	FROM <zusatztabellen>
	<weitere Bestandteile der Unterabfrage>
	) <name>
	<weitere Bestandteile der Hauptabfrage>

Eine solche Unterabfrage kann grundsätzlich alle SELECT-Bestandteile enthalten.
Bitte beachten Sie dabei:
- Nach der schließenden Klammer muss ein Name als Tabellen-Alias angegeben
werden, der als Ergebnistabelle in der Hauptabfrage verwendet wird.

- Die Unterabfrage kann eine oder mehrere Tabellen umfassen − wie jede andere
Abfrage auch.

- In der Spaltenliste sollte jeweils ein Name als Spalten-Alias vor allem dann vor-
gesehen werden, wenn mehrere Tabellen verknüpft werden; andernfalls erzeugt
SQL selbständig Namen wie ID, ID1 usw., die man nicht ohne Weiteres versteht
und zuordnen kann.

- ORDER BY kann nicht sinnvoll genutzt werden, weil das Ergebnis der Unterab-
frage als Tabelle behandelt wird und mit der Haupttabelle oder einer anderen
Tabelle verknüpft wird, wodurch eine Sortierung sowieso verlorenginge.

Wie gesagt: Eine solche Unterabfrage kann überall stehen, wo eine Tabelle vorgesehen ist. In der vorstehenden Syntax steht sie nur beispielhaft innerhalb der FROM-Klausel.


.. Admonition:: Beispiel

	Bestimme alle Schadensfälle, die von der durchschnittlichen Schadenshöhe eines Jahres maximal 300 € abweichen.


	- Lösung Teil 1: Stelle alle Jahre zusammen und bestimme den Durchschnitt aller
	                 Schadensfälle innerhalb eines Jahres.

	- Lösung Teil 2: Hole alle Schadensfälle, deren Schadenshöhe im jeweiligen Jahr
                         innerhalb des Bereichs „Durchschnitt plus/minus 300“ liegen.

        .. code-block:: sql

		SELECT sf.ID, sf.Datum, sf.Schadenshoehe, temp.Jahr, temp.Durchschnitt
		FROM Schadensfall sf,
		( SELECT AVG(sf2.Schadenshoehe) AS Durchschnitt,
		EXTRACT(YEAR FROM sf2.Datum) as Jahr
		FROM Schadensfall sf2
		group by EXTRACT(YEAR FROM sf2.Datum)
		) temp
		WHERE temp.Jahr = EXTRACT(YEAR FROM sf.Datum)
		and ABS(Schadenshoehe - temp.Durchschnitt) <= 300;

	Zuerst stellen wir durch eine Gruppierung alle Jahreszahlen und die durchschnittlichen Schadenshöhen zusammen (Teil 1 der Lösung). Für Teil 2 der Lösung muss für jeden Schadensfall nur noch Jahr und Schadenshöhe mit dem betreffenden Eintrag in der Ergebnistabelle temp verglichen werden.

	Das ist der wesentliche Unterschied und entscheidende Vorteil zu anderen Lösungen: Die Durchschnittswerte werden einmalig zusammengestellt und nur noch abgerufen; sie müssen nicht bei jedem Datensatz neu (und ständig wiederholt) berechnet werden.


.. Admonition:: Beispiel 2

	Bestimme alle Fahrzeuge eines bestimmten Herstellers mit Angabe des Typs.

	- Lösung Teil 1: Hole die ID des gewünschten Herstellers.

	- Lösung Teil 2: Hole alle IDs und Bezeichnungen der Tabelle Fahrzeugtyp, die zu dieser Hersteller-ID gehören.

	- Lösung Teil 3: Hole alle Fahrzeuge, die zu dieser Liste von Fahrzeugtyp-IDs gehören.


	.. code-block:: sql


		SELECT Fahrzeug.ID, Kennzeichen, Typen.ID As TYP, Typen.Bezeichnung
		FROM Fahrzeug,
		( SELECT ID, Bezeichnung
		FROM Fahrzeugtyp
		WHERE Hersteller_ID =
		( SELECT ID
		FROM Fahrzeughersteller
		WHERE Name = ’Volkswagen’ )
		) Typen
		WHERE Fahrzeugtyp_ID = Typen.ID;

	::

		ID KENNZEICHEN TYP BEZEICHNUNG
		23 BOR-RS 890   2    Golf
		22 BOR-PQ 567   3    Passat


	Teil 1 der Lösung ist die „innere“ Klammer; dies entspricht dem obigen Verfahren. Teil 2 der Lösung ist die „äußere“ Klammer; Ergebnis ist eine Tabelle von IDs und Bezeichnungen, also ein Teil der Tabelle Fahrzeugtyp, deren Werte für den Vergleich der WHERE-Klausel und außerdem für die Ausgabe verwendet werden.


.. raw:: latex

         \newpage


Übungen
^^^^^^^^

#. Übung 1
   Welche der folgenden Feststellungen sind richtig, welche sind falsch?

   1. Das Ergebnis einer Unterabfrage kann verwendet werden, wenn es ein einzelner Wert oder
      eine Liste in Form einer Tabelle ist. Andere Ergebnisse sind nicht möglich.
   2. Ein einzelner Wert als Ergebnis kann durch eine direkte Abfrage oder durch
      eine Spaltenfunktion erhalten werden.
   3. Unterabfragen sollten nicht verwendet werden, wenn die WHERE-Bedingung
      für jede Zeile der Hauptabfrage einen anderen Wert erhält und deshalb die Unterabfrage neu ausgeführt werden muss.
   4. Mehrere Unterabfragen können verschachtelt werden.
   5. Für die Arbeitsgeschwindigkeit ist es gleichgültig, ob mehrere Unterabfragen
      oder JOINs verwendet werden.
   6. Eine Unterabfrage mit einer Tabelle als Ergebnis kann GROUP BY nicht sinnvoll nutzen.
   7. Eine Unterabfrage mit einer Tabelle als Ergebnis kann ORDER BY nicht sinnvoll nutzen.
   8. Bei einer Unterabfrage mit einer Tabelle als Ergebnis ist ein Alias-Name für die
      Tabelle sinnvoll, aber nicht notwendig.
   9. Bei einer Unterabfrage mit einer Tabelle als Ergebnis sind Alias-Namen für
      die Spalten sinnvoll, aber nicht notwendig.

#. Übung 2

   Welche Verträge (mit einigen Angaben) hat der Mitarbeiter „Braun, Christian“ abgeschlossen? Ignorieren Sie die Möglichkeit, dass es mehrere Mitarbeiter dieses Namens geben könnte.

#. Übung 3

   Zeigen Sie alle Verträge, die zum Kunden „Heckel Obsthandel GmbH“ gehören. Ignorieren Sie die Möglichkeit, dass der Kunde mehrfach gespeichert sein könnte.

#. Übung 4

   Ändern Sie die Lösung von Übung 3, sodass auch mehrere Kunden mit diesem Namen als Ergebnis denkbar sind.

#. Übung 5

   Zeigen Sie alle Fahrzeuge, die im Jahr 2008 an einem Schadensfall beteiligt waren.

#. Übung 6

   Zeigen Sie alle Fahrzeugtypen (mit ID, Bezeichnung und Name des Herstellers), die im Jahr 2008 an einem Schadensfall beteiligt waren.

#. Übung 7

   Bestimmen Sie alle Fahrzeuge eines bestimmten Herstellers mit Angabe des Typs.

#. Übung 8

   Zeigen Sie zu jedem Mitarbeiter der Abteilung „Vertrieb“ den ersten Vertrag (mit einigen Angaben) an, den er abgeschlossen hat. Der Mitarbeiter soll mit ID und Name/Vorname angezeigt werden.

#. Übung 9

   Von der Deutschen Post AG wird eine Tabelle PLZ_Aenderung mit folgenden Inhalten geliefert:

   ::


   	   ID PLZalt Ortalt          PLZneu Ortneu
   	   1 45658   Recklinghausen  45659  Recklinghausen
   	   2 45721   Hamm-Bossendorf 45721  Haltern OT Hamm
   	   3 45772   Marl            45770  Marl
   	   4 45701   Herten          45699  Herten

   Ändern Sie die Tabelle Versicherungsnehmer so, dass bei allen Adressen, bei denen PLZ/Ort mit PLZalt/Ortalt übereinstimmen, diese Angaben durch PLZneu/Ortneu geändert werden.

   Hinweise: Beschränken Sie sich auf die Änderung mit der ID=3. (Die vollständige Lösung ist erst mit SQL-Programmierung möglich.) Bei dieser Änderungsdatei handelt es sich nur um ﬁktive Daten, keine echten Änderungen.


.. only:: html

	Lösungen
	^^^^^^^^^

	- Lösung zu Übung 1
	  Richtig sind 2, 3, 4, 7, 9; falsch sind 1, 5, 6, 8.

	- Lösung zu Übung 2

	  .. code-block:: sql


		select ID, Vertragsnummer, Abschlussdatum, Art
		from Versicherungsvertrag
		where Mitarbeiter_ID
		in ( select ID
		from Mitarbeiter
		where Name = ’Braun’
		and Vorname = ’Christian’ );

	- Lösung zu Übung 3

	  .. code-block:: sql


		select ID, Vertragsnummer, Abschlussdatum, Art
		from Versicherungsvertrag
		where Versicherungsnehmer_ID
		= ( select ID from Versicherungsnehmer
		where Name =’Heckel Obsthandel GmbH’ );

	- Lösung zu Übung 4

	  .. code-block:: sql

		select ID, Vertragsnummer, Abschlussdatum, Art
		from Versicherungsvertrag
		where Versicherungsnehmer_ID
		in ( 	select ID from Versicherungsnehmer
			where Name = 'Heckel Obsthandel GmbH' );

	- Lösung zu Übung 5

	  .. code-block:: sql

		select ID, Kennzeichen, Fahrzeugtyp_ID as TypID
		from Fahrzeug fz
		where ID in ( select Fahrzeug_ID
		from Zuordnung_sf_fz zu
		join Schadensfall sf on sf.ID = zu.Schadensfall_ID
		where EXTRACT(YEAR from sf.Datum) = 2008 );

	- Lösung zu Übung 6

	.. code-block:: sql

		SELECT distinct ft.ID as TypID, ft.Bezeichnung as Typ, fh.Name as Hersteller
		FROM Fahrzeugtyp ft
		inner join Fahrzeughersteller fh on fh.ID = ft.Hersteller_ID
		right join Fahrzeug fz on ft.ID = fz.Fahrzeugtyp_ID
		WHERE fz.ID IN ( SELECT Fahrzeug_ID
		FROM Zuordnung_sf_fz zu
		JOIN Schadensfall sf ON sf.ID = zu.Schadensfall_ID
		WHERE EXTRACT(YEAR FROM sf.Datum) = 2008 );

		Beachten Sie vor allem, dass die WHERE-Bedingung übernommen werden konnte, aber die Tabellen anders zu verknüpfen sind. Die Bedingung könnte in die ON-Klausel einbezogen werden; da sie aber die Auswahl beschränken soll, ist die WHERE-Klausel vorzuziehen.


	- Lösung zu Übung 7

	  .. code-block:: sql

		SELECT fz.ID, fz.Kennzeichen, Typen.ID AS TYP, Typen.Bezeichnung
		FROM Fahrzeug fz
		join ( SELECT ID, Bezeichnung
		FROM Fahrzeugtyp
		WHERE Hersteller_ID =
		( SELECT ID FROM Fahrzeughersteller
		WHERE Name = ’Volkswagen’ )
		) Typen on fz.Fahrzeugtyp_ID = Typen.ID;

	- Lösung zu Übung 8

	  .. code-block:: sql

		SELECT vv.ID as VV, vv.Vertragsnummer, vv.Abschlussdatum, vv.Art,
		mi.ID as MI, mi.Name, mi.Vorname
		from Versicherungsvertrag vv
		right join ( select MIN(vv2.ID) as ID, vv2.Mitarbeiter_ID
		from Versicherungsvertrag vv2
		group by vv2.Mitarbeiter_id ) Temp
		on Temp.ID = vv.ID
		right join Mitarbeiter mi on mi.ID = vv.Mitarbeiter_ID
		where mi.Abteilung_ID = ( select ID from Abteilung
		where Bezeichnung = ’Vertrieb’ );


		Erläuterungen: Wir benötigen eine einfache Unterabfrage, um die Liste der Mitarbeiter für "Vertrieb" zu erhalten, und wir benötigen eine Unterabfrage, die uns zur Mitarbeiter-ID die kleinste Vertrags-ID liefert. Wegen der Aufgabenstellung "zu jedem Mitarbeiter" sowie "mit einigen Angaben" muss es sich bei beiden Verknüpfungen um einen RIGHT JOIN handeln.

	- Lösung zu Übung 9

	  .. code-block:: sql

		UPDATE Versicherungsnehmer
		set PLZ, Ort
		= ( Select PLZneu, Ortneu
		from PLZ_Aenderg
		where ID = 3 )
		where PLZ = ( Select PLZalt
		from PLZ_Aenderg
		where ID = 3 )
		and Ort = ( Select Ortalt
		from PLZ_Aenderg
		where ID = 3 );


		Vielleicht funktioniert diese Variante bei Ihrem DBMS nicht; dann ist die folgende
		Version nötig:

		UPDATE Versicherungsnehmer
		set PLZ = ( Select PLZneu
		from PLZ_Aenderg
		where ID = 3 ),
		Ort = ( Select Ortneu
		from PLZ_Aenderg
		where ID = 3 )
		where PLZ = ( Select PLZalt
		from PLZ_Aenderg
		where ID = 3 )
		and Ort = ( Select Ortalt
		from PLZ_Aenderg
		where ID = 3 );
