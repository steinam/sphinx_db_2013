Mysql als Beispiel für Datenbank
=================================

Die schwedische Firma MySQL AB hat MySQL von 1994 bis 2008 entwickelt. Die ursprüngliche Intention war eine verbesserte und beschleunigte Verarbeitung eines selbst entwickelten Tabellensystems mit dem Namen ISAM. Dazu wurde mSQL
genutzt. Von 2008 bis 2010 wurde MySQL AB von Sun Microsystems übernommen, und seit Januar 2010 wird MySQL unter dem Schirm von Oracle weiterentwickelt. 



Neben der Open Source-Lizenz ist es vor allem die Erweiterbarkeit mithilfe von eigenen Engines, die MySQL so beliebt gemacht hat. MySQL ist ein Client-/Server-Datenbanksystem. Ein Server stellt alleine oder im Verbund mit anderen Servern den Anwendungen (Clients) die Datenbankdienste zur Verfügung.

Der MySQL-Server besteht – wie jedes DBMS – aus vielen Komponenten, die hier kurz angerissen werden.

.. image:: figure/mysql_komponenten.jpg 



#. Konnektoren (Verbindung): 

   Der Client baut über die Konnektoren eine Sitzung zum MySQL-Server auf. Dies erfolgt über das TCP/IP-Protokoll und in der Regel über den Port 3306.
   
#. Connection-Pool: 
 
   Hier werden die Verbindungen zu den Clients verwaltet. Beim Verbindungsaufbau wird anhand des Benutzernamens und des Passworts die Verbindungsanfrage authentifiziert. Ist die Anzahl der maximal verfügbaren Verbindungen (Connections Limits) nicht überschritten, wird ein Verbindungsthread eingerichtet. Ebenso werden die anderen Grenzwerte für die Verbindung überwacht: Datenübertragungsvolumen, Timeout etc.
   
#. SQL Schnittstelle: 

   Hier werden die SQL-Befehle entgegengenommen. Sie werden dann zum Parser weitergereicht.

#. Parser: 

   Der Parser überprüft die Syntax eines Befehls und ob man die Ausführungsrechte für diesen Befehl hat.

#. Optimizer (Optimierer): 

   Anhand von Schätzungen, Statistiken und Algorithmen wird für nicht triviale Befehle ein Ausführungsplan erstellt. Dieser Ausführungsplan berücksichtigt ggf. im Cache vorhandene Ergebnisse.

#. Caches und Buffers (Zwischenspeicher): 

   Daten und Ergebnisse können in Zwischenspeichern aufgehoben werden. Diese sind nur in der Sitzung verfügbar, die diese erstellt (lokal), oder allen Sitzungen (global).

#. Storage Engines: 

   Die Motoren des Servers. Hier werden die Daten tatsächlich verarbeitet. Jede Engine ist dabei für bestimmte Aufgabenstellungen besonders gut geeignet. Der große Vorteil von MySQL ist, dass jeder mit einem besonderen Bedarf eine Engine bauen kann. Er muss nur die Schnittstellen beachten. 

#. File System (Dateisystem): 

   Je nach Betriebssystem werden hier Daten in unterschiedlichen Dateisystemen (NTFS, BTRFS, ETX4 etc.) abgelegt. Bis auf die Frage, ob das Dateisystem zwischen Groß- und Kleinschreibung unterscheidet, spielt dieses für die SQL- Programmierung keine Rolle.

#. Management Services & Utilities: 
   
   Parallel dazu gibt es die vielen kleinen Helferlein, ohne die nichts geht: für das Sichern und Wiederherstellen von Daten, Datenreplikation, Administration und Konfi guration, Datenmigration und Metadaten.


