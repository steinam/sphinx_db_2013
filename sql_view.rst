Views
=====


VIEWs sind Abfragen, die in der Datenbank als Objekt fest gespeichert sind. Sie können als virtuelle Tabellen verstanden werden, deren Inhalt und Struktur auf anderen Tabellen oder Views basieren, und können in (fast) jedem SELECT-Befehl anstelle einer „echten“ Tabelle verwendet werden.





.. figure:: figure/Views.png


Die Abfragen, auf denen Views basieren, können grundsätzlich alle Klauseln wie eine normale Abfrage enthalten. Somit ist es möglich, bestimmte Daten in einer View zu selektieren und zu gruppieren. Hierbei können die Daten aus mehreren Tabellen oder Views selektiert werden.


Je nach DBMS und Situation kann eine einzelne Klausel der View unwirksam sein
oder zu unklaren Ergebnissen führen.

- Eine ORDER BY-Klausel der View wird ignoriert, wenn der SELECT-Befehl, der sie benutzt, selbst eine Sortierung verwendet.
- Bei einer Beschränkung durch LIMIT o. ä. weiß das DBMS oft nicht, nach welchen Regeln diese Beschränkung verwirklicht werden soll.
- WHERE-Bedingungen können nur fest eingebaut werden, aber nicht mit variablen Parametern.



.. code-block:: sql
        
    CREATE VIEW name    //Name des Views
    [(view_col      //Spaltennamen
    [, view_col ...])]  //des Views
    AS
    select .....        //Select-Statement
    [WITH CHECK OPTION];
    
    z.B.
    
    CREATE VIEW PHONE_LIST(
	    EMP_NO,
	    FIRST_NAME,
	    LAST_NAME,
	    PHONE_EXT,
	    LOCATION,
	    PHONE_NO)
	    AS
	    
	    SELECT emp_no, first_name, last_name,
	    phone_ext, location, phone_no
	    FROM employee, department
	    WHERE employee.dept_no = department.dept_no;
    
     
	    
Die View wird dann wie jede Tabelle benutzt, z. B. einfach:

.. code-block:: sql

	SELECT * FROM <View-Name>

	Oder auch als Teil einer komplexen Abfrage:

	SELECT <irgendwas>
	FROM <Tabelle>
	JOIN <View-Name> ON /* usw. */	    
	    
	    
- Ein View ist grundsätzliche  eine Art von SELECT - Abfrage, die Datensätze zurückgibt und sich in vielen Fällen wie eine normale Tabelle verhält.
- Normaler Tabellentyp im Sinne von SQL-89 und SQL-92
- Wird auch viewed table bzw. virtual table bezeichnet
- Es wird keine echte Tabelle angelegt, sondern nur die Metadaten
   werden gespeichert. 
   
   -  den eindeutigen Namen (identifier),
   -  eine Liste der Spaltennamen
   -  den eindeutigen Namen (identifier),
   -  das SQL-Statement, mit dem die Daten aus den eigentlichen
      Basistabellen gewonnen werden


-  Kann Inhalte aus mehreren Tabellen anzeigen
-  In manchen Fällen sind Views updatebar, d.h. Änderungen am View wirken sich unmittelbar auf die zugrundeliegenden Tabellen aus.


.. Admonition:: Beispiel

	Erstelle eine View, die eine Liste aller Fahrzeugtypen deutscher Hersteller anzeigt.
	
	.. code-block:: sql
	
		CREATE VIEW Deutscher_Fahrzeugtyp
		AS SELECT DISTINCT ft.Bezeichnung AS Fahrzeugtyp, fh.Name AS Hersteller
		FROM Fahrzeugtyp ft
		join Fahrzeughersteller fh on ft.Hersteller_ID = fh.ID
		WHERE fh.Land = ’Deutschland’;

        Die Abfrage basiert auf den beiden Tabellen Fahrzeugtyp und Fahrzeughersteller.
        Es werden nur die Spalten Bezeichnung und Name abgefragt; durch die WHERE-Klausel wird das Ergebnis auf Fahrzeuge deutscher Hersteller beschränkt. Für die Spalten werden Spalten-Aliase genutzt.

        Diese View wird dann wie eine „normale“ Tabellen in Abfragen genutzt.

        .. code-block:: sql
        
        	SELECT * FROM Deutscher_Fahrzeugtyp ORDER BY Hersteller;


Gerade in Verbindung mit Joins kann ein View wertvolle Zeit sparen, da er beispielsweise häufig benötigte Verknüpfungen bereits zur Verfügung stellen kann, ohne dass diese immer wieder neu definiert werden müssen.

.. Admonition:: Beispiel

	*Bereite eine (ﬁktive) Tabelle Fahrzeugart vor mit allen relevanten Informationen aus den Tabellen Fahrzeugtyp und Fahrzeughersteller.*
	
	.. code-block:: sql
	
		CREATE VIEW Fahrzeugart
			( ID, Bezeichnung, Hersteller, Land )
		AS 
			SELECT ft.ID, ft.Bezeichnung, fh.Name, fh.Land
			FROM Fahrzeugtyp ft
			join Fahrzeughersteller fh on ft.Hersteller_ID = fh.ID;
			
	
	Damit kann das nun folgene Beispiel vereinfacht werden, weil der letzte JOIN wegfällt.
	
	*Hole alle Dienstwagen (ggf. mit den zugehörigen Mitarbeitern) und nenne dazu alle Fahrzeugdaten.*
	
	.. code-block:: sql
	
		SELECT
			mi.Personalnummer AS MitNr,
			mi.Name, mi.Vorname,
			dw.ID AS DIW, dw.Kennzeichen, dw.Fahrzeugtyp_ID AS TypID,
			fa.Bezeichnung AS Typ, fa.Hersteller
		FROM Dienstwagen dw
			LEFT JOIN Mitarbeiter mi ON mi.ID = dw.Mitarbeiter_ID
			INNER JOIN Fahrzeugart fa ON fa.ID = dw.Fahrzeugtyp_ID;




**Vorteil**

- Vereinfachter Zugriff auf Daten, Wiederverwendbarkeit
- Situationsspezifischer Zugriff auf Daten
- Datenunabhängigkeit durch Abschirmen der User- Anwendungen von den Nebenwirkungen einer Änderung des Datenbankdesigns.

  So könnte man z.B. eine Tabelle in zwei Teiltabellen aufsplitten. Bisherige SQL-Statements wären damit unbrauchbar. Auf der anderen Seite kann aber nun ein View implementiert werden, der einen Join beider neuen Tabellen darstellt. Die User-Applikationen können nun ihre SQL-Statements weiterbenutzen. Sie müssen nur den Tabellennamen durch den Viewnamen ändern.

- Datensicherheit

  Views können den Zugriff auf sensitive oder irrelevante Teile einer Tabelle verweigern. So könnte z.B. ein Benutzer das Recht haben, sich Job-Informationen bzgl. der EMPLOYEE-Tabelle über einen View zu besorgen. Der Zugriff auf die Gehaltsinformation der Datensätze kann ihm aber dennoch verwehrt werden.



	
Aufgabe zu Views
------------------------

#. Welche der folgenden Feststellungen sind richtig, welche sind falsch?
   
   1. Eine View ist wie eine „normale“ Abfrage, deren Bestandteile in der Datenbank fest 
      gespeichert werden.
   2. Das Ergebnis dieser Abfrage wird gleichzeitig gespeichert und steht damit beim 
      nächsten Aufruf der View sofort zur Verfügung.
   3. Eine ORDER BY-Klausel kann in einer View immer benutzt werden
   4. Eine ORDER BY-Klausel ist in einer View nicht erforderlich.
   5. Wenn diese Klausel in einer View benutzt wird, hat diese Sortierung Vorrang vor 
      einer ORDER BY-Klausel in dem SELECT-Befehl, der die View benutzt.
   6. Wenn ein SELECT-Befehl komplexe JOINs oder andere Klauseln benutzt und häuﬁger benutzt wird, 
      ist es sinnvoll, ihn in einer View zu kapseln.
   7. Wenn ein Anwender nicht alle Daten sehen darf, ist es notwendig, die Zugriffsrechte 
      auf die Spalten zu beschränken; diese Beschränkung kann nicht über eine View gesteuert werden.
   8. Eine View kann in einem SELECT-Befehl in der FROM-Klausel anstatt einer 
      Tabelle aufgerufen werden.
   9. Eine View kann nicht in einem JOIN benutzt werden.

   
#. Bei der Suche nach Dienstwagen sollen mit der View Dienstwagen_Anzeige immer auch 
   angezeigt werden:
   	
   - Name und Vorname des Mitarbeiters
   - ID und Bezeichnung seiner Abteilung
   - der Fahrzeugtyp (nur als ID)

   Stellen Sie sicher, dass auch nicht-persönliche Dienstwagen immer angezeigt werden, und kontrollieren Sie das Ergebnis durch eine Abfrage ähnlich diesem Muster:
   
   .. code-block:: sql
	   
	SELECT * FROM Dienstwagen_Anzeige
	WHERE ( Abt_ID BETWEEN 5 AND 8 ) or ( Mi_Name is null );   
	   
#. Erweitern Sie die vorstehende View so, dass mit Hilfe der View Fahrzeugart auch Bezeichnung,
   Hersteller und Land angezeigt werden. Kontrollieren Sie das Ergebnis durch die o. g. Abfrage.

#. Erstellen Sie eine Sicht Vertrag_Anzeige, die zu jedem Vertrag anzeigt:

   - ID, Vertragsnummer, Abschlussdatum, Art (als Text)
   - Name, Vorname des Mitarbeiters
   - Name, Vorname des Versicherungsnehmers
   - Kennzeichen des Fahrzeugs
   
#. Erweitern Sie die vorstehende View so, dass mit Hilfe der View Fahrzeugart auch
   Bezeichnung, Hersteller und Land angezeigt werden.  
   
#. Erstellen Sie eine Sicht Schaden_Anzeige, bei der zu jedem an einem Schadensfall beteiligten Fahrzeug angezeigt werden:

   - ID, Datum, Gesamthöhe eines Schadensfalls
   - Kennzeichen und Typ des beteiligten Fahrzeugs
   - Anteiliger Schaden
   - ID des Versicherungsvertrags
   
#. Erweitern Sie die vorstehende View so, dass mit Hilfe der View Fahrzeugart auch Bezeichnung, Hersteller und Land sowie Vertragsnummer und ID des Versicherungsnehmers angezeigt werden.  
   
#. Erstellen Sie eine weitere View so, dass die vorstehende View für alle Schadensfälle des
   aktuellen Jahres benutzt wird.  
   

   
Lösung zu Views
------------------------
   
#. Richtig sind die Aussagen 1, 3, 4, 6, 8. Falsch sind die Aussagen 2, 5, 7, 9.

#. View Dienstwagen_Anzeige

   .. code-block:: sql
   
   	create view Dienstwagen_Anzeige
		( Kennzeichen, TypId,
		Mi_Name, Mi_Vorname,
		Ab_ID, Ab_Name )
	    as select dw.Kennzeichen, dw.Fahrzeugtyp_ID,
		mi.Name, mi.Vorname,
		mi.Abteilung_ID,
		ab.Bezeichnung
	    from Dienstwagen dw
		left join Mitarbeiter mi
		on mi.ID = dw.Mitarbeiter_ID
		left join Abteilung ab
		on ab.ID = mi.Abteilung_ID;

   Erläuterung: LEFT JOIN in beiden Fällen wird benötigt, damit auch NULL-Werte, nämlich die 
                nicht-persönlichen Dienstwagen angezeigt werden.

                
#. Erweitern Dienstwagen_Anzeige

   .. code-block:: sql
   
   	alter view Dienstwagen_Anzeige
		( Kennzeichen, TypId,
		Typ, Fz_Hersteller, Fz_Land,
		Mi_Name, Mi_Vorname,
		Ab_ID, Ab_Name )
	   as select dw.Kennzeichen, dw.Fahrzeugtyp_ID,
		fa.Bezeichnung, fa.Hersteller, fa.Land,
		mi.Name, mi.Vorname,
		mi.Abteilung_ID,
		ab.Bezeichnung
	   from Dienstwagen dw
		left join Mitarbeiter mi
		on mi.ID = dw.Mitarbeiter_ID
		left join Abteilung ab
		on ab.ID = mi.Abteilung_ID
		inner join Fahrzeugart fa
		on fa.ID = dw.Fahrzeugtyp_ID;
		
		
#. View Vertrag_Anzeige

   .. code-block:: sql
   
   	create view Vertrag_Anzeige
		( ID, Vertragsnummer, Abschlussdatum, Art,
		Mi_Name, Mi_Vorname,
		Vn_Name, Vn_Vorname,
		Kennzeichen )
	   as select vv.ID, vv.Vertragsnummer, vv.Abschlussdatum,
		CASE vv.Art
		WHEN ’TK’ THEN ’Teilkasko’
		WHEN ’VK’ THEN ’Vollkasko’
		ELSE ’Haftpflicht’
		END,
		mi.Name, mi.Vorname,
		vn.Name, vn.Vorname,
		fz.Kennzeichen
	   from Versicherungsvertrag vv
		join Mitarbeiter mi
		on mi.ID = vv.Mitarbeiter_ID
		join Versicherungsnehmer vn
		on vn.ID = vv.Versicherungsnehmer_ID
		join Fahrzeug fz
		on fz.ID = vv.Fahrzeug_ID;
		
		
#. Erweitern Vertrag_Anzeige

   .. code-block:: sql
   
   	alter view Vertrag_Anzeige
		( ID, Vertragsnummer, Abschlussdatum, Art,
		Mi_Name, Mi_Vorname,
		Vn_Name, Vn_Vorname,
		Kennzeichen, Typ, Hersteller, Land )
	   as select vv.ID, vv.Vertragsnummer, vv.Abschlussdatum,
		CASE vv.Art
		WHEN ’TK’ THEN ’Teilkasko’
		WHEN ’VK’ THEN ’Vollkasko’
		ELSE ’Haftpflicht’
		END,
		mi.Name, mi.Vorname,
		vn.Name, vn.Vorname,
		fz.Kennzeichen, fa.Bezeichnung, fa.Hersteller, fa.Land
	   from Versicherungsvertrag vv
		join Mitarbeiter mi
		on mi.ID = vv.Mitarbeiter_ID
		join Versicherungsnehmer vn
		on vn.ID = vv.Versicherungsnehmer_ID
		join Fahrzeug fz
		on fz.ID = vv.Fahrzeug_ID
		join Fahrzeugart fa
		on fa.ID = fz.Fahrzeugtyp_ID;
		
#. View Schaden_Anzeige

   .. code-block:: sql
   
   	create view Schaden_Anzeige
	( ID, Datum, Gesamtschaden,
		Kennzeichen, Typ,
		Schadensanteil,
		VV_ID )
	   as select sf.ID, sf.Datum, sf.Schadenshoehe,
		fz.Kennzeichen, fz.Fahrzeugtyp_ID,
		zu.Schadenshoehe,
		vv.ID
	   from Zuordnung_SF_FZ zu
		join Schadensfall sf
		on sf.ID = zu.Schadensfall_ID
		join Fahrzeug fz
		on fz.ID = zu.Fahrzeug_ID
		join Versicherungsvertrag vv
		on fz.ID = vv.Fahrzeug_ID;
		
#. Erweiterung Schaden_Anzeige

   .. code-block:: sql
   
   	alter view Schaden_Anzeige
		( ID, Datum, Gesamtschaden,
		Kennzeichen, Typ, Hersteller, Land,
		Schadensanteil,
		VV_ID, Vertragsnummer, VN_ID )
	   as select sf.ID, sf.Datum, sf.Schadenshoehe,
		fz.Kennzeichen, fa.Bezeichnung, fa.Hersteller, fa.Land,
		zu.Schadenshoehe,
		vv.ID, vv.Vertragsnummer, vv.Versicherungsnehmer_ID
	   from Zuordnung_SF_FZ zu
		join Schadensfall sf
		on sf.ID = zu.Schadensfall_ID
		join Fahrzeug fz
		on fz.ID = zu.Fahrzeug_ID
		join Versicherungsvertrag vv
		on fz.ID = vv.Fahrzeug_ID
		join Fahrzeugart fa
		on fa.ID = fz.Fahrzeugtyp_ID;
		
#. View

   .. code-block:: sql
   
   	create view Schaden_Anzeige_Jahr
   		as select *
   		from Schaden_Anzeige
   		where YEAR(Datum) =YEAR(NOW());
   		
   		
   		
