Aufgaben zu SQL
========================


LUNA
--------

#. Verschaffen sie sich einen Ãœberblick Ã¼ber die Tabelle tblabteilung
#. Lassen sie sich alle Mitarbeiter mit Namen und Vorname anzeigen
#. Finden Sie den Namen und Nummer aller Abteilungen, die ihren Sitz in München haben.
#. Nennen Sie die Namen und Vornamen aller Mitarbeiter, deren Personalnummer mindestens 15000 ist
#. Finden Sie alle Projekte, deren Finanzmittel mehr als 6000 US$ betragen. Der derzeitige Kurs beträgt: 1 US$ = 0,9875 €
#. Gesucht werden Personalnummer, Projektnummer und Aufgabe der Mitarbeiter, die im Projekt p2 Sachbearbeiter sind.
#. Gesucht wird die Personalnummer der Mitarbeiter, die entweder im Projekt p1 oder p2 oder in beiden tätig sind
#. Gesucht wird die Personalnummer der Mitarbeiter, die entweder im Projekt p1 oder p2 oder in beiden tätig sind. Jeder Treffer soll nur einmal erscheinen.
#. Nennen Sie Personalnummer und Nachnamen der Mitarbeiter, die nicht in der Abteilung a2 arbeiten
#. Finden Sie alle Mitarbeiter, deren Personalnummer entweder 29346, 28559 oder 25348 ist
#. Nennen Sie alle Mitarbeiter, deren Personalnummer weder 10102 noch 9031 ist
#. Nennen Sie Namen und Mittel aller Projekte, deren Etat zwischen 95000,00 € und 120000,00 € liegt.
#. Nennen Sie die Personalnummer aller Mitarbeiter, die Projektleiter sind und vor oder nach 1988 eingestellt worden sind. (d.h. nicht im Jahr 1988)
#. Finden Sie die Personal- und Projektnummer aller Mitarbeiter, die im Projekt p1 arbeiten und deren Aufgabe noch nicht festgelegt ist.
#. Finden Sie Namen und Personalnummer aller Mitarbeiter, deren Name mit "K" beginnt.
#. Finden Sie Namen,Vornamen und Personalnummer aller Mitarbeiter, deren Vornamen als zweiten Buchstaben ein "a" hat
#. Finden Sie die Abteilungsnummer und Standorte aller Abteilungen, die sich in den Orten, die mit einem Buchstaben zwischen E und N beginnen, befinden.
#. Finden Sie Namen, Vornamen und Personalnummer aller Mitarbeiter, deren Namen nicht mit den Buchstaben M, N, O und P, und deren Vornamen nicht mit H beginnt.
#. Nennen Sie alle Mitarbeiter, deren Name nicht mit "mann" endet
#. Nennen Sie die Abteilungsnummer des Mitarbeiters, dessen Personalnummer 2581 ist und der im Projekt p3 arbeitet.
#. Nennen Sie die Nummern aller Projekte, in welchen Mitarbeiter arbeiten, deren Personalnummer kleiner als die Nummer des Mitarbeiters Müller ist.
#. Nennen Sie die Daten aller Mitarbeiter, die in München arbeiten.
#. Nennen Sie die Namen aller Mitarbeiter, die im Projekt Apollo arbeiten
#. Gruppieren Sie die Mitarbeiter nach Projektnummer und Aufgabe
#. Nennen Sie die kleinste Personalnummer eines Mitarbeiters
#. Nennen Sie Personalnummer und Namen des Mitarbeiters mit der kleinsten Personalnummer
#. Finden Sie die Personalnummer des Projektleiters, der in dieser Position als letzter eingestellt wurde
#. Berechnen Sie die Summe der finanziellen Mittel aller Projekte
#. Berechnen Sie das arithmetische Mittel der Geldbeträge, die höher als 100000 € sind.
#. Finden Sie heraus, wie viele verschiedene Aufgaben in jedem Projekt ausgeübt werden
#. Finden Sie heraus, wie viele Mitarbeiter in jedem Projekt arbeiten
#. Erstellen Sie eine Ãœbersicht, wie viele Mitarbeiter welcher Tätigkeit nachgehen.
#. Nennen Sie alle Projekte, in denen weniger als vier Mitarbeiter beschäftigt sind.
#. Finden Sie für jeden Mitarbeiter, zusätzlich zu seiner Personalnummer, Namen und Vornamen, auch die Abteilungsnummer und den Standort der Abteilung
#  Finden Sie die Daten der Mitarbeiter, die im Projekt Gemini arbeiten.
#. Nennen Sie die Abteilungsnummern aller Mitarbeiter, die am 15.10.1989 eingestellt wurden.
#. Nennen Sie Namen und Vornamen aller Projektleiter, deren Abteilung den Standort Stuttgart hat.
#. Nennen Sie die Namen der Projekte, in denen Mitarbeiter tätig sind, die zur Abteilung Diagnose gehören.
#. Finden Sie alle Abteilungen, an deren Standorten sich weitere Abteilungen befinden.
#. Betrachten Sie folgende DDL- und SELECT-Statements. Welches Ergebnis erhalten Sie für die SELECT-Statements

   .. code-block:: sql


		drop table if exists nulltest ;
		create table nulltest (
		c1 int null
		);

		insert into nulltest(c1) values(1);
		insert into nulltest(c1) values(null);
		insert into nulltest(c1) values (3);

		select avg(c1) from nulltest;

		select sum(c1)/count(*) from nulltest;
		select sum(c1)/count(c1) from nulltest;



Luna_Lösung
^^^^^^^^^^^^

#. Verschaffen sie sich einen Überblick über die Tabelle tblabteilung

   .. code-block:: sql


   	SELECT * from tblabteilung



#. Lassen sie sich alle Mitarbeiter mit Namen und Vorname anzeigen


   .. code-block:: sql

     	SELECT M_Name, M_Vorname
     	FROM tblmitarbeiter



#. Finden Sie den Namen und Nummer aller Abteilungen, die ihren Sitz in München haben.


   .. code-block:: sql

     	SELECT A_Name, A_Stadt
     	FROM tblabteilung
     	WHERE A_Stadt = 'München'



#. Nennen Sie die Namen und Vornamen aller Mitarbeiter, deren Personalnummer mindestens 15000 ist


   .. code-block:: sql

     	SELECT M_Name, M_Vorname
     	FROM tblmitarbeiter
     	WHERE M_Nr >= 15000



#. Finden Sie alle Projekte, deren Finanzmittel mehr als 6000 US$ betragen. Der derzeitige Kurs beträgt: 1 US$ = 0,9875 €


   .. code-block:: sql

     	SELECT P_Name
     	FROM tblprojekt
     	WHERE P_Mittel * 0.9875 > 60000



#. Gesucht werden Personalnummer, Projektnummer und Aufgabe der Mitarbeiter, die im Projekt p2 Sachbearbeiter sind.


   .. code-block:: sql

     	SELECT M_Nr, P_Nr, T_Taetigkeit
     	FROM tbltaetigkeit
     	WHERE P_Nr = 'p1'
		AND T_Taetigkeit = 'Sachbearbeiter'



#. Gesucht wird die Personalnummer der Mitarbeiter, die entweder im Projekt p1 oder p2 oder in beiden tätig sind


   .. code-block:: sql

     		SELECT M_Nr
     		FROM tbltaetigkeit
     		WHERE P_Nr = 'p1'
     		OR P_Nr = 'p2'



#. Gesucht wird die Personalnummer der Mitarbeiter, die entweder im Projekt p1 oder p2 oder in beiden tätig sind. Jeder Treffer soll nur einmal erscheinen.


   .. code-block:: sql

     	SELECT DISTINCT M_Nr
     	FROM tbltaetigkeit
     	WHERE P_Nr = 'p1'
	OR P_Nr = 'p2'



#. Nennen Sie Personalnummer und Nachnamen der Mitarbeiter, die nicht in der Abteilung a2 arbeiten


   .. code-block:: sql

     	SELECT M_nr, M_Name
     	FROM tblmitarbeiter
     	WHERE A_Nr <> 'a2'



#. Finden Sie alle Mitarbeiter, deren Personalnummer entweder 29346, 28559 oder 25348 ist


   .. code-block:: sql

     	SELECT *
     	FROM tblmitarbeiter
     	WHERE M_Nr in (29346, 28559, 25348)



#. Nennen Sie alle Mitarbeiter, deren Personalnummer weder 10102 noch 9031 ist


   .. code-block:: sql

     	SELECT *
     	FROM tblmitarbeiter
     	WHERE M_Nr NOT IN (10102, 9031)



#. Nennen Sie Namen und Mittel aller Projekte, deren Etat zwischen 95000,00 € und 120000,00 € liegt.


   .. code-block:: sql

     	SELECT P_Name, P_Mittel
     	FROM tblprojekt
     	WHERE P_Mittel BETWEEN 95000 AND 1200000



#. Nennen Sie die Personalnummer aller Mitarbeiter, die Projektleiter sind und vor oder nach 1988 eingestellt worden sind. (d.h. nicht im Jahr 1988)


   .. code-block:: sql

     	SELECT M_Nr
     	FROM tbltaetigkeit
     	WHERE T_Taetigkeit = 'Projektleiter'
	AND T_Einstellungsdatum NOT BETWEEN '01.01.1988' and '31.12.1988'



#. Finden Sie die Personal- und Projektnummer aller Mitarbeiter, die im Projekt p1 arbeiten und deren Aufgabe noch nicht festgelegt ist.


   .. code-block:: sql

     	SELECT M_nr, P_Nr
     	FROM tbltaetigkeit
     	WHERE P_Nr = 'p1'
	AND T_Taetigkeit is NULL



#. Finden Sie Namen und Personalnummer aller Mitarbeiter, deren Name mit "K" beginnt.


   .. code-block:: sql

     	SELECT M_Name, M_Vorname, M_Nr
     	FROM tblmitarbeiter
     	WHERE M_Name LIKE 'K%'



#. Finden Sie Namen,Vornamen und Personalnummer aller Mitarbeiter, deren Vornamen als zweiten Buchstaben ein "a" hat.


   .. code-block:: sql

     	SELECT M_Name, M_Vorname, M_Nr
     	FROM tblmitarbeiter
     	WHERE M_Vorname LIKE '_a%'



#. Finden Sie die Abteilungsnummer und Standorte aller Abteilungen, die sich in den Orten, die mit einem Buchstaben zwischen E und N beginnen, befinden.


   .. code-block:: sql



	SELECT A_Nr, A_Stadt
	FROM tblabteilung
	WHERE A_Stadt regexp '^[E-N]'



#. Finden Sie Namen, Vornamen und Personalnummer aller Mitarbeiter, deren Namen nicht mit den Buchstaben M, N, O und P, und deren Vornamen nicht mit H beginnt.


   .. code-block:: sql

     	SELECT M_Name, M_Vorname, M_Nr
     	FROM tblmitarbeiter
     	WHERE M_Name regexp '[^M-P]%'
	AND M_Vorname regexp '[^H]%'



#. Nennen Sie alle Mitarbeiter, deren Name nicht mit "mann" endet.


   .. code-block:: sql

     	SELECT *
     	FROM tblmitarbeiter
     	WHERE M_Name NOT LIKE '%mann'



#. Nennen Sie die Abteilungsnummer des Mitarbeiters, dessen Personalnummer 2581 ist und der im Projekt p3 arbeitet.


   .. code-block:: sql

   	SELECT A_Nr
   	FROM tblmitarbeiter
   	WHERE M_Nr =
	(SELECT M_Nr
	 FROM tbltaetigkeit
	 WHERE M_Nr = 2581
	 	AND P_Nr = 'p3')




#. Nennen Sie die Nummern aller Projekte, in welchen Mitarbeiter arbeiten, deren Personalnummer kleiner als die Nummer des Mitarbeiters Müller ist.


   .. code-block:: sql

     	SELECT DISTINCT P_Nr
     	FROM tbltaetigkeit
     	WHERE M_Nr <
	(SELECT M_Nr
		FROM tblmitarbeiter
		WHERE M_Name = 'Müller')



#. Nennen Sie die Daten aller Mitarbeiter, die in München arbeiten.


   .. code-block:: sql


   	SELECT *
   	FROM tblmitarbeiter
   	WHERE A_Nr IN
		(SELECT A-Nr
		FROM tblabteilung
		WHERE A_Stadt = 'München'





#. Nennen Sie die Namen aller Mitarbeiter, die im Projekt Apollo arbeiten.


   .. code-block:: sql

   	SELECT M_Name
   	FROM tblmitarbeiter
   	WHERE M_Nr IN
		(SELECT M_Nr
		FROM tbltaetigkeit
		WHERE P_Nr =
			(SELECT P_Nr
			FROM tblprojekt
			WHERE P_Name = 'Apollo'
			)
		)



#. Gruppieren Sie die Mitarbeiter nach Projektnummer und Aufgabe.


   .. code-block:: sql

     	SELECT P_Nr, T_Taetigkeit
     	FROM tbltaetigkeit
     	GROUP BY P_Nr, T_Taetigkeit



#. Nennen Sie die kleinste Personalnummer eines Mitarbeiters.


   .. code-block:: sql

     	SELECT MIN(M_Nr) as Kleinster
     	FROM tblmitarbeiter



#. Nennen Sie Personalnummer und Namen des Mitarbeiters mit der kleinsten Personalnummer


   .. code-block:: sql

     	SELECT M_Nr, M_Name
     	FROM tblmitarbeiter
     	WHERE M_Nr = (
     		SELECT MIN(M_Nr)
		FROM tblmitarbeiter
		)



#. Finden Sie die Personalnummer des Projektleiters, der in dieser Position als letzter eingestellt wurde


   .. code-block:: sql

     	SELECT M_Nr
     	FROM tblmitarbeiter
     	WHERE M_Nr = (
     		SELECT MAX(T_Einstellungsdatum)
     		FROM tbltaetigkeit
     		WHERE T_Taetigkeit = 'Projektleiter'
     		)




#. Berechnen Sie die Summe der finanziellen Mittel aller Projekte.


   .. code-block:: sql

     	SELECT SUM(P_Mittel)
     	FROM tblprojekt



#. Berechnen Sie das arithmetische Mittel der Geldbeträge, die höher als 100000 € sind.


   .. code-block:: sql

     	SELECT AVG(P_Mittel)
     	FROM tblprojekt
     	WHERE P_Mittel > 100000



#. Finden Sie heraus, wie viele verschiedene Aufgaben in jedem Projekt ausgeübt werden.


   .. code-block:: sql

     	SELECT P_Nr, COUNT(DISTINCT T_Taetigkeit) as Anzahl
     	FROM tbltaetigkeit
     	GROUP BY P_Nr




#. Finden Sie heraus, wie viele Mitarbeiter in jedem Projekt arbeiten.


   .. code-block:: sql

     	SELECT P_Nr, COUNT(*)
     	FROM tbltaetigkeit
     	GROUP BY P_NR

     	-- Hinweis: (*) berücksichtigt auch NULL-Werte --




#. Erstellen Sie eine Ãœbersicht, wie viele Mitarbeiter welcher Tätigkeit nachgehen.


   .. code-block:: sql

     	SELECT T_Taetigkeit, COUNT(*) as Mitarbeiteranzahl
     	FROM tbltaetigkeit
     	GROUP BY T_Taetigkeit



#. Nennen Sie alle Projekte, in denen weniger als vier Mitarbeiter beschäftigt sind.


   .. code-block:: sql





#. Finden Sie für jeden Mitarbeiter, zusätzlich zu seiner Personalnummer, Namen und Vornamen, auch die Abteilungsnummer und den Standort der Abteilung.


   .. code-block:: sql

     	SELECT tblmitarbeiter.*, tblabteilung.*
     	FROM tblmitarbeiter, tblabteilung
     	WHERE tblmitarbeiter.A_Nr = tblabteilung.A_Nr



# Finden Sie die Daten der Mitarbeiter, die im Projekt Gemini arbeiten.


   .. code-block:: sql

     	SELECT *
     	FROM tblmitarbeiter, tbltaetigkeit, tblprojekt
     	WHERE tblmitarbeiter.M_Nr = tbltaetigkeit.M_Nr
     	AND tbltaetigkeit.P_Nr = tblprojekt.P_Nr
     	and P_Name = 'Gemini'



#. Nennen Sie die Abteilungsnummern aller Mitarbeiter, die am 15.10.1989 eingestellt wurden.


   .. code-block:: sql

     	SELECT A_Nr
     	FROM tblmitarbeiter, tbltaetigkeit
     	WHERE tbltaetigkeit.M_Nr = tblmitarbeiter.M_Nr
     	AND T_Einstellungsdatum = '1989-10-15'




#. Nennen Sie Namen und Vornamen aller Projektleiter, deren Abteilung den Standort Stuttgart hat.


   .. code-block:: sql

	SELECT M_Name, M_Vorname
     	FROM tblmitarbeiter, tbltaetigkeit, tblabteilung
     	WHERE tbltaetigkeit.M_Nr = tblmitarbeiter.M_Nr
     	AND tblmitarbeiter.A_Nr = tblabteilung.A_Nr
     	AND T_TAetigkeit = 'Projektleiter'
     	AND A_Stadt = 'Stuttgart'



#. Nennen Sie die Namen der Projekte, in denen Mitarbeiter tätig sind, die zur Abteilung Diagnose gehören.


   .. code-block:: sql

       SELECT DISTINCT P_Name
       FROM tblabteilung, tblmitarbeiter, tblprojekt, tbltaetigkeit
       WHERE tblprojekt.P_Nr = tbltaetigkeit.P_Nr
       AND tbltaetigkeit.M_Nr = tblmitarbeiter.M_Nr
       AND tblmitarbeiter.A_Nr = tblabteilung.A_Nr
       AND A_Name = 'Diagnose'


#. Finden Sie alle Abteilungen, an deren Standorten sich weitere Abteilungen befinden.


   .. code-block:: sql

     	SELECT Temp1.A_Nr, Temp2.A_Name, Temp1.A_Stadt
     	FROM tblabteilung Temp1, tblabteilung Temp2
     	WHERE Temp1.A_Stadt = Temp2.A_Stadt
     	AND Temp1.A_Nr <> Temp2.A_Nr



#. Betrachten Sie folgende DDL- und SELECT-Statements. Welches Ergebnis erhalten Sie für die SELECT-Statements

   .. code-block:: sql


		drop table if exists nulltest ;
		create table nulltest (
		c1 int null
		);

		insert into nulltest(c1) values(1);
		insert into nulltest(c1) values(null);
		insert into nulltest(c1) values (3);

		select avg(c1) from nulltest;

		select sum(c1)/count(*) from nulltest;
		select sum(c1)/count(c1) from nulltest;



    Während die AVG-Funktion NULL-Werte in Feldern berücksichtigt, gibt es bei der COUNT()-Funktion zwei unterschiedliche Verhaltensweisen.

    COUNT(*) berücksichtigt auch NULL-Werte und gibt als Ergebnis die Anzahl 3 zurück (4/3 = 1.33)

    COUNT(c1) klammert NULL-Werte aus und gibt als Ergebnis die Anzahl 2 zurück (4/2 = 2)



Nordwind
---------

.. figure:: figure/nordwind.png
	:width: 600px



#. Erstellen Sie eine Liste aller Mitarbeiter, die jünger als 40 Jahre sind
#. Erstellen Sie eine Liste aller Mitarbeiterinnen aus London
#. Erstellen Sie eine Liste aller Kunden, wo der Inhaber gleichzeitig Kontaktperson ist
#. Erstellen Sie eine Liste aller Kunden nach Ländern angeordnet, die Städte in alphabetischer Reihenfolge.
#. Erstellen Sie eine Liste aller Lieferanten, die noch keine Homepage haben.
#. Erstellen Sie eine Liste aller Kunden, die irgendeine Art von Camembert gekauft haben
#. Ermitteln Sie unsere drei umsatzstärksten Kunden. Beginnen Sie zunächst mit dem umsatzstärksten Kunden.
#. Mit welcher Lieferfirma machen wir den geringsten Umsatz
#. Wer hat Gnocchi gekauft, deren Preis über dem durchschnittlichen Verkaufspreis lag.
#. Welcher Kunde musste am längsten auf seine Lieferung warten. Wie lange ?
#. Wie lange dauert der Versand im Schnitt bei unseren Versandfirmen
#. Erstellen Sie eine Liste, wie hoch die Frachtkosten unserer Versandfirmen 1996 waren
#. Erstellen Sie eine Liste der Kunden, deren Waren noch nicht versendet wurden
#. Ermitteln Sie, wie oft unsere Kunden 1998 im Mittel bestellt haben.
#. Stellen Sie fest, wie oft LILA Supermercado 1996 über 1000,00 € bestellt hat
#. Stellen Sie fest, wie viele Sendungen in die USA gingen
#. Ermitteln Sie, wie viele verschiedenen Länder wir beliefern
#. Berechnen Sie, wie viel Umsatz wir mit skandinavischen Lieferanten machen
#. Erstellen Sie eine Liste, welche Kunden 1995 länger als 10 Tage auf ihre Lieferung mit SpeedExpress warten mussten.
#. Erstellen Sie eine Liste mit dem Umsatz je Kategorie, die mit dem Minimum beginnt



Lösung
^^^^^^



#. Erstellen Sie eine Liste aller Mitarbeiter, die jünger als 40 Jahre sind

   .. code-block:: sql

	SELECT
	`personal`.`Geburtsdatum`, `personal`.`Vorname`,`personal`.`Nachname`
	FROM `personal`
	WHERE NOW() <= DATE_ADD(`personal`.`Geburtsdatum`, INTERVAL "40" YEAR);
	================ oder etwas unflexibler
	SELECT .....
	FROM Personal
	WHERE Personal.Geburtsdatum > YYYY/MM/DD (Jahr/Monat/Tag einsetzen)



#. Erstellen Sie eine Liste aller Mitarbeiterinnen aus London

   .. code-block:: sql

	SELECT Vorname, Nachname
	FROM Personal
	where Personal.Ort = 'London'
	and Perdsonal.Anrede LIKE 'Frau'


#. Erstellen Sie eine Liste aller Kunden, wo der Inhaber gleichzeitig Kontaktperson ist

   .. code-block:: sql

	SELECT Firma from Kunden where Position LIKE 'Inhaber'


#. Erstellen Sie eine Liste aller Kunden nach Ländern angeordnet, die Städte in alphabetischer Reihenfolge.

   .. code-block:: sql

   	Select Firma, Land, Ort from Kunden order by Land, Ort


#. Erstellen Sie eine Liste aller Lieferanten, die noch keine Homepage haben.

   .. code-block:: sql

	Select Firma, Homepage FROM LIEFERANTEN
	WHERE Homepage is null
	or Hoempage = ''

#. Erstellen Sie eine Liste aller Kunden, die irgendeine Art von Camembert gekauft haben

   .. code-block:: sql

	SELECT DISTINCT Firma, Artikelname
	FROM Kunden, Bestellungen, Bestelldetails, Artikel
	WHERE Kunden.KundenCode = Bestellungen.KundenCode
	AND Bestellungen.BestellNr = Bestelldetails.BestellNr
	AND bestelldetails.ArtikelNr = Artikel.ArtikelNr
	AND Artikelname like '%Camembert%'

	============== inner join
	SELECT DISTINCT Firma, Artikelname
	from Kunden
	INNER JOIN bestellungen on (kunden.kundencode = bestellungen.kundencode)
	INNER JOIN bestelldetails on (bestellungen.bestellnr = bestelldetails.bestellnr)
	INNER JOIN artikel on bestelldetails.artikelnr = artikel.artikelnr
	WHERE artikel.artikelname LIKE '%Camembert%'



#. Ermitteln Sie unsere drei umsatzstärksten Kunden. Beginnen Sie zunächst mit dem umsatzstärksten Kunden.

   .. code-block:: sql

	SELECT
	kunden.firma,
	SUM(bestelldetails.Einzelpreis * bestelldetails.Anzahl) as Umsatz
	FROM
	bestelldetails
	INNER JOIN bestellungen ON (bestelldetails.bestellnr = bestellungen.bestellnr)
	INNER JOIN kunden ON (bestellungen.KundenCode = kunden.KundenCode)
	GROUP BY Kunden.Firma
	ORDER BY Umsatz DESC
	LIMIT 0,3


#. Mit welcher Lieferfirma machen wir den geringsten Umsatz

   .. code-block:: sql

	SELECT
	lieferanten.Firma,
	SUM(bestelldetails.einzelpreis * bestelldetails.anzahl) as Umsatz
	FROM bestelldetails
	INNER JOIN bestellungen ON (bestelldetails.BestellNr = bestellungen.BestellNr)
	INNER JOIN artikel ON (bestelldetails.Artikelnr = artikel.artikelnr)
	INNER JOIN lieferanten ON (Artikel.lieferantenNr = Lieferanten.LieferantenNr)
	GROUP BY lieferanten.Firma
	ORDER BY Umsatz
	LIMIT 0,1


#. Wer hat Gnocchi gekauft, deren Preis über dem durchschnittlichen Verkaufspreis lag.

   .. code-block:: sql

	Select distinct Firma, Artikelname,bestelldetails.einzelpreis
	from Kunden, Bestellungen, Bestelldetails, Artikel
	where Kunden.KundenCode = Bestellungen.KundenCode
	and Bestellungen.BestellNr = Bestelldetails.BestellNr
	and bestelldetails.ArtikelNr = Artikel.ArtikelNr
	and Artikelname like 'Gnocchi%'
	and bestelldetails.einzelpreis > (select avg(bestelldetails.einzelpreis) from bestelldetails, artikel
			where bestelldetails.artikelnr = artikel.artikelnr
			and Artikelname like 'Gnocchi%')
	#######################################################
	Lösung geht nicht
	SELECT
	AVG(bestelldetails.einzelpreis) as Durchschnittspreis,
	artikel.artikelname,
	kunden.firma
	FROM bestellungen
	INNER JOIN bestelldetails ON (bestellungen.bestellnr = bestelldetails.bestellnr)
	INNER JOIN artikel ON (bestelldetails.artikelnr = artikel.Artikelnr)
	INNER JOIN kunden ON (bestellungen.kundencode = kunden.kundencode)
	WHERE
	artikel.artikelname LIKE 'Gnocchi%'
	AND (SELECT AVG(bestelldetails.eizelpreis AS Durchschnitt2) > (bestelldetails.einzelpreis)
	GROUP BY artikel.artikelname



#. Welcher Kunde musste am längsten auf seine Lieferung warten. Wie lange ?

   .. code-block:: sql

	SELECT distinct Firma, Lieferdatum-Bestelldatum as wartezeit
	FROM Kunden, bestellungen
	WHERE Kunden.Kundencode = bestellungen.kundencode
	AND lieferdatum-bestelldatum =
		(SELECT MAX(Lieferdatum - bestelldatum) FROM Kunden, bestellungen
		where kunden.kundencode = bestellungen.kundencode)


#. Wie lange dauert der Versand im Schnitt bei unseren Versandfirmen

   .. code-block:: sql

	select firma, avg(Lieferdatum-Versanddatum) from versandfirmen, bestellungen
	where versandfirmen.firmennr = bestellungen.versandueber
	and year(Lieferdatum) = 1995
	group by firma

#. Erstellen Sie eine Liste, wie hoch die Frachtkosten unserer Versandfirmen 1996 waren

   .. code-block:: sql

	Select firma, sum(frachtkosten) from versandfirmen, bestellungen
	where versandfirmen.firmennr = bestellungen.versandueber
	group by firma


#. Erstellen Sie eine Liste der Kunden, deren Waren noch nicht versendet wurden

   .. code-block:: sql

	select distinct firma
	from kunden, bestellungen
	where kunden.kundencode = bestellungen.kundencode
	and versanddatum is null


#. Ermitteln Sie, wie oft unsere Kunden 1998 im Mittel bestellt haben.

   .. code-block:: sql

	select Firma, count(bestellnr) from kunden, bestellungen
	where kunden.kundencode = bestellungen.kundencode
	and year(bestelldatum) = 1994
	group by Firma


#. Stellen Sie fest, wie oft LILA Supermercado 1996 über 1000,00 € bestellt hat

   .. code-block:: sql

	select sum(einzelpreis*anzahl)-(einzelpreis*anzahl*rabatt) as bestellwert
	from bestelldetails, bestellungen, kunden
	where bestelldetails.bestellnr = bestellungen.bestellnr
	and bestellungen.kundencode = kunden.kundencode
	and firma like 'lila%'
	and year(Bestelldatum) = 1998
	group by bestellungen.bestellnr
	having sum((einzelpreis*anzahl)-(einzelpreis*anzahl*Rabatt)) > 1000

#. Stellen Sie fest, wie viele Sendungen in die USA gingen

   .. code-block:: sql

	select count(Bestimmungsland) as Lieferungen_USA from bestellungen where Bestimmungsland= 'USA'

#. Ermitteln Sie, wie viele verschiedenen Länder wir beliefern

   .. code-block:: sql

	select distinct bestimmungsland as länderliste from bestellungen

#. Berechnen Sie, wie viel Umsatz wir mit skandinavischen Lieferanten machen

   .. code-block:: sql

	SELECT SUM(Einzelpreis*Anzahl) - (Einzelpreis*Anzahl*Rabatt) as Umsatz_Skandinavien
	FROM bestelldetails, bestellungen, kunden
	WHERE bestelldetails.bestellnr = bestellungen.bestellnr
	AND bestellungen.kundencode = kunden.kundencode
	AND kunden.land in ('Schweden','Finnland','Dänemark')

#. Erstellen Sie eine Liste, welche Kunden 1995 länger als 10 Tage auf ihre Lieferung mit SpeedExpress warten mussten.

   .. code-block:: sql

	SELECT DISTINCT kunden.firma
	FROM kunden, bestellungen, versandfirmen
	WHERE kunden.kundencode = bestellungen.kundencode
	AND bestellungen.versandueber = versandfirmen.firmennr
	AND lieferdatum-bestelldatum > 10
	AND YEAR(Lieferdatum) = 1996
	AND Versandfirmen.firma LIKE 'Speed%'

#. Erstellen Sie eine Liste mit dem Umsatz je Kategorie, die mit dem Minimum beginnt

   .. code-block:: sql

	SELECT Kategoriename, SUM((bestelldetails.einzelpreis * anzahl) - (bestelldetails.einzelpreis * Anzahl * Rabatt)) AS Umsatz
	FROM bestellungen,bestelldetails, artikel, kategorien
	WHERE bestellungen.bestellnr = bestelldetails.bestellnr
	AND bestelldetails.artikelnr = artikel.artikelnr
	AND artikel.kategorienr = kategorien.kategorienr
	GROUP BY kategoriename
	ORDER BY 2
