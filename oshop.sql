# SQL Manager Lite for MySQL 5.4.3.43929
# ---------------------------------------
# Host     : localhost
# Port     : 3306
# Database : oshop


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES latin1 */;

SET FOREIGN_KEY_CHECKS=0;

CREATE DATABASE `oshop_test`
    CHARACTER SET 'utf8'
    COLLATE 'utf8_unicode_ci';

USE `oshop_test`;

#
# Structure for the `adresse` table : 
#

CREATE TABLE `adresse` (
  `adresse_id` INTEGER(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `strasse` VARCHAR(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `hnr` VARCHAR(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `lkz` CHAR(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `plz` CHAR(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ort` VARCHAR(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `deleted` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY USING BTREE (`adresse_id`)  ,
  UNIQUE INDEX `idx_adresse_dublette` USING BTREE (`strasse`(100), `hnr`(100), `plz`)  
)ENGINE=InnoDB
AUTO_INCREMENT=12 AVG_ROW_LENGTH=2340 CHARACTER SET 'utf8'
;

#
# Structure for the `artikel` table : 
#

CREATE TABLE `artikel` (
  `artikel_id` INTEGER(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `bezeichnung` VARCHAR(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `einzelpreis` DECIMAL(14,6) NOT NULL DEFAULT 0.000000,
  `waehrung` CHAR(3) COLLATE utf8_unicode_ci DEFAULT 'EUR',
  `deleted` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY USING BTREE (`artikel_id`)  ,
   INDEX `idx_artikel_bezeichnung` USING BTREE (`bezeichnung`)  
)ENGINE=InnoDB
AUTO_INCREMENT=9016 AVG_ROW_LENGTH=1820 CHARACTER SET 'utf8' ;

#
# Structure for the `lieferant` table : 
#

CREATE TABLE `lieferant` (
  `lieferant_id` INTEGER(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `firmenname` VARCHAR(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `adresse_id` INTEGER(10) UNSIGNED NOT NULL DEFAULT 0,
  `deleted` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY USING BTREE (`lieferant_id`)  ,
   INDEX `adresse_id` USING BTREE (`adresse_id`)  ,
   INDEX `idx_lieferant_firmenname` USING BTREE (`firmenname`)  ,
  CONSTRAINT `lieferant_ibfk_1` FOREIGN KEY (`adresse_id`) REFERENCES `adresse` (`adresse_id`)
)ENGINE=InnoDB
AUTO_INCREMENT=1 CHARACTER SET 'utf8' ;

#
# Structure for the `artikel_nm_lieferant` table : 
#

CREATE TABLE `artikel_nm_lieferant` (
  `lieferant_id` INTEGER(10) UNSIGNED NOT NULL DEFAULT 0,
  `artikel_id` INTEGER(10) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY USING BTREE (`lieferant_id`, `artikel_id`)  ,
   INDEX `artikel_id` USING BTREE (`artikel_id`)  ,
  CONSTRAINT `artikel_nm_lieferant_ibfk_1` FOREIGN KEY (`lieferant_id`) REFERENCES `lieferant` (`lieferant_id`),
  CONSTRAINT `artikel_nm_lieferant_ibfk_2` FOREIGN KEY (`artikel_id`) REFERENCES `artikel` (`artikel_id`)
)ENGINE=InnoDB
CHARACTER SET 'utf8' 
;

#
# Structure for the `warengruppe` table : 
#

CREATE TABLE `warengruppe` (
  `warengruppe_id` INTEGER(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `bezeichnung` VARCHAR(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `deleted` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY USING BTREE (`warengruppe_id`)  ,
   INDEX `idx_warengruppe_bezeichnung` USING BTREE (`bezeichnung`)  
)ENGINE=InnoDB
AUTO_INCREMENT=5 AVG_ROW_LENGTH=4096 CHARACTER SET 'utf8' 
;

#
# Structure for the `artikel_nm_warengruppe` table : 
#

CREATE TABLE `artikel_nm_warengruppe` (
  `warengruppe_id` INTEGER(10) UNSIGNED NOT NULL DEFAULT 0,
  `artikel_id` INTEGER(10) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY USING BTREE (`warengruppe_id`, `artikel_id`)  ,
   INDEX `artikel_id` USING BTREE (`artikel_id`)  ,
  CONSTRAINT `artikel_nm_warengruppe_ibfk_1` FOREIGN KEY (`warengruppe_id`) REFERENCES `warengruppe` (`warengruppe_id`),
  CONSTRAINT `artikel_nm_warengruppe_ibfk_2` FOREIGN KEY (`artikel_id`) REFERENCES `artikel` (`artikel_id`)
)ENGINE=InnoDB
AVG_ROW_LENGTH=1260 CHARACTER SET 'utf8' 
;

#
# Structure for the `bank` table : 
#

CREATE TABLE `bank` (
  `bank_id` CHAR(12) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `bankname` VARCHAR(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `lkz` CHAR(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'DE',
  `deleted` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY USING BTREE (`bank_id`)  ,
   INDEX `idx_bank_bankname` USING BTREE (`bankname`)  
)ENGINE=InnoDB
CHARACTER SET 'utf8' 
;

#
# Structure for the `kunde` table : 
#

CREATE TABLE `kunde` (
  `kunde_id` INTEGER(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nachname` VARCHAR(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `vorname` VARCHAR(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `rechnung_adresse_id` INTEGER(10) UNSIGNED DEFAULT NULL,
  `liefer_adresse_id` INTEGER(10) UNSIGNED DEFAULT NULL,
  `bezahlart` INTEGER(10) UNSIGNED NOT NULL DEFAULT 0,
  `art` ENUM('unb','prv','gsch') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'unb',
  `deleted` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY USING BTREE (`kunde_id`)  ,
   INDEX `rechnung_adresse_id` USING BTREE (`rechnung_adresse_id`)  ,
   INDEX `liefer_adresse_id` USING BTREE (`liefer_adresse_id`)  ,
   INDEX `idx_kunde_nachname_vorname` USING BTREE (`nachname`, `vorname`)  ,
  CONSTRAINT `kunde_ibfk_1` FOREIGN KEY (`rechnung_adresse_id`) REFERENCES `adresse` (`adresse_id`) ON UPDATE CASCADE,
  CONSTRAINT `kunde_ibfk_2` FOREIGN KEY (`liefer_adresse_id`) REFERENCES `adresse` (`adresse_id`) ON DELETE SET NULL ON UPDATE CASCADE
)ENGINE=InnoDB
AUTO_INCREMENT=6 AVG_ROW_LENGTH=3276 CHARACTER SET 'utf8' 
;

#
# Structure for the `bankverbindung` table : 
#

CREATE TABLE `bankverbindung` (
  `kunde_id` INTEGER(10) UNSIGNED NOT NULL DEFAULT 0,
  `bankverbindung_nr` INTEGER(10) UNSIGNED NOT NULL DEFAULT 0,
  `bank_id` CHAR(12) COLLATE utf8_unicode_ci NOT NULL,
  `kontonummer` CHAR(25) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `iban` CHAR(34) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `deleted` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY USING BTREE (`kunde_id`, `bankverbindung_nr`)  ,
  UNIQUE INDEX `idx_bankverbindung_iban` USING BTREE (`iban`)  ,
   INDEX `idx_bankverbindung_bankid_kontonummer` USING BTREE (`bank_id`, `kontonummer`)  ,
  CONSTRAINT `bankverbindung_ibfk_1` FOREIGN KEY (`kunde_id`) REFERENCES `kunde` (`kunde_id`),
  CONSTRAINT `bankverbindung_ibfk_2` FOREIGN KEY (`bank_id`) REFERENCES `bank` (`bank_id`)
)ENGINE=InnoDB
CHARACTER SET 'utf8' 
;

#
# Structure for the `bestellung` table : 
#

CREATE TABLE `bestellung` (
  `bestellung_id` INTEGER(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `kunde_id` INTEGER(10) UNSIGNED NOT NULL DEFAULT 0,
  `adresse_id` INTEGER(10) UNSIGNED NOT NULL DEFAULT 0,
  `datum` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` ENUM('offen','versendet','angekommen','retour','bezahlt') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'offen',
  `deleted` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY USING BTREE (`bestellung_id`)  ,
   INDEX `adresse_id` USING BTREE (`adresse_id`)  ,
   INDEX `idx_bestellung_kundeid_datum` USING BTREE (`kunde_id`, `datum`)  ,
  CONSTRAINT `bestellung_ibfk_1` FOREIGN KEY (`kunde_id`) REFERENCES `kunde` (`kunde_id`),
  CONSTRAINT `bestellung_ibfk_2` FOREIGN KEY (`adresse_id`) REFERENCES `adresse` (`adresse_id`)
)ENGINE=InnoDB
AUTO_INCREMENT=3 AVG_ROW_LENGTH=8192 CHARACTER SET 'utf8' 
;

#
# Structure for the `bestellung_position` table : 
#

CREATE TABLE `bestellung_position` (
  `bestellung_id` INTEGER(10) UNSIGNED NOT NULL DEFAULT 0,
  `position_nr` INTEGER(10) UNSIGNED NOT NULL DEFAULT 0,
  `artikel_id` INTEGER(10) UNSIGNED NOT NULL DEFAULT 0,
  `menge` DECIMAL(14,6) NOT NULL DEFAULT 0.000000,
  `deleted` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY USING BTREE (`bestellung_id`, `position_nr`)  ,
   INDEX `artikel_id` USING BTREE (`artikel_id`)  ,
  CONSTRAINT `bestellung_position_ibfk_1` FOREIGN KEY (`bestellung_id`) REFERENCES `bestellung` (`bestellung_id`),
  CONSTRAINT `bestellung_position_ibfk_2` FOREIGN KEY (`artikel_id`) REFERENCES `artikel` (`artikel_id`)
)ENGINE=InnoDB
AVG_ROW_LENGTH=3276 CHARACTER SET 'utf8' 
;

#
# Structure for the `rechnung` table : 
#

CREATE TABLE `rechnung` (
  `rechnung_id` INTEGER(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `kunde_id` INTEGER(10) UNSIGNED NOT NULL DEFAULT 0,
  `bestellung_id` INTEGER(10) UNSIGNED NOT NULL DEFAULT 0,
  `adresse_id` INTEGER(10) UNSIGNED NOT NULL DEFAULT 0,
  `bezahlart` ENUM('lastschrift','rechnung','kredit','bar') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'lastschrift',
  `datum` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` ENUM('offen','gestellt','mahnung1','inkasso','storno','beglichen') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'offen',
  `rabatt` DECIMAL(6,2) NOT NULL DEFAULT 0.00,
  `skonto` DECIMAL(6,2) NOT NULL DEFAULT 0.00,
  `deleted` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY USING BTREE (`rechnung_id`)  ,
   INDEX `kunde_id` USING BTREE (`kunde_id`)  ,
   INDEX `adresse_id` USING BTREE (`adresse_id`)  ,
  CONSTRAINT `rechnung_ibfk_1` FOREIGN KEY (`kunde_id`) REFERENCES `kunde` (`kunde_id`),
  CONSTRAINT `rechnung_ibfk_2` FOREIGN KEY (`adresse_id`) REFERENCES `adresse` (`adresse_id`)
)ENGINE=InnoDB
AUTO_INCREMENT=1 CHARACTER SET 'utf8' 
;

#
# Structure for the `rechnung_position` table : 
#

CREATE TABLE `rechnung_position` (
  `rechnung_id` INTEGER(10) UNSIGNED NOT NULL DEFAULT 0,
  `position_nr` INTEGER(10) UNSIGNED NOT NULL DEFAULT 0,
  `artikel_id` INTEGER(10) UNSIGNED NOT NULL DEFAULT 0,
  `steuer` DECIMAL(14,6) NOT NULL DEFAULT 19.000000,
  `menge` DECIMAL(14,6) NOT NULL DEFAULT 0.000000,
  `deleted` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY USING BTREE (`rechnung_id`, `position_nr`)  ,
   INDEX `artikel_id` USING BTREE (`artikel_id`)  ,
  CONSTRAINT `rechnung_position_ibfk_1` FOREIGN KEY (`rechnung_id`) REFERENCES `rechnung` (`rechnung_id`),
  CONSTRAINT `rechnung_position_ibfk_2` FOREIGN KEY (`artikel_id`) REFERENCES `artikel` (`artikel_id`)
)ENGINE=InnoDB
CHARACTER SET 'utf8' 
;

#
# Data for the `adresse` table  (LIMIT -492,500)
#

INSERT INTO `adresse` (`adresse_id`, `strasse`, `hnr`, `lkz`, `plz`, `ort`, `deleted`) VALUES

  (1,'Beutelhaldenweg','5','AL','67676','Hobbingen',0),
  (2,'Beutelhaldenweg','1','AL','67676','Hobbingen',0),
  (3,'Auf der Feste','1','GO','54786','Minas Tirith',0),
  (4,'Letztes Haus','4','ER','87567','Bruchtal',0),
  (5,'Baradur','1','MO','62519','Lugburz',0),
  (10,'Hochstrasse','4a','DE','44879','Bochum',0),
  (11,'Industriegebiet','8','DE','44878','Bochum',0);
COMMIT;

#
# Data for the `artikel` table  (LIMIT -490,500)
#

INSERT INTO `artikel` (`artikel_id`, `bezeichnung`, `einzelpreis`, `waehrung`, `deleted`) VALUES

  (3001,'Papier (100)',2.300000,'EUR',0),
  (3005,'Tinte (gold)',55.700000,'EUR',0),
  (3006,'Tinte (rot)',6.200000,'EUR',0),
  (3007,'Tinte (blau)',4.130000,'EUR',0),
  (3010,'Feder',5.000000,'EUR',0),
  (7856,'Silberzwiebel',0.510000,'EUR',0),
  (7863,'Tulpenzwiebel',3.390000,'EUR',0),
  (9010,'Schaufel',14.950000,'USD',0),
  (9015,'Spaten',19.900000,'EUR',0);
COMMIT;

#
# Data for the `warengruppe` table  (LIMIT -495,500)
#

INSERT INTO `warengruppe` (`warengruppe_id`, `bezeichnung`, `deleted`) VALUES

  (1,'Bürobedarf',0),
  (2,'Pflanzen',0),
  (3,'Gartenbedarf',0),
  (4,'Werkzeug',0);
COMMIT;

#
# Data for the `artikel_nm_warengruppe` table  (LIMIT -486,500)
#

INSERT INTO `artikel_nm_warengruppe` (`warengruppe_id`, `artikel_id`) VALUES

  (1,3001),
  (1,3005),
  (1,3006),
  (1,3007),
  (1,3010),
  (2,7856),
  (2,7863),
  (3,7856),
  (3,7863),
  (3,9010),
  (3,9015),
  (4,9010),
  (4,9015);
COMMIT;

#
# Data for the `kunde` table  (LIMIT -494,500)
#

INSERT INTO `kunde` (`kunde_id`, `nachname`, `vorname`, `rechnung_adresse_id`, `liefer_adresse_id`, `bezahlart`, `art`, `deleted`) VALUES

  (1,'Gamdschie','Samweis',1,2,0,'prv',0),
  (2,'Beutlin','Frodo',2,NULL,0,'prv',0),
  (3,'Beutlin','Bilbo',2,4,0,'prv',0),
  (4,'Telcontar','Elessar',3,NULL,0,'prv',0),
  (5,'Earendilionn','Elrond',4,NULL,0,'gsch',0);
COMMIT;

#
# Data for the `bestellung` table  (LIMIT -497,500)
#

INSERT INTO `bestellung` (`bestellung_id`, `kunde_id`, `adresse_id`, `datum`, `status`, `deleted`) VALUES

  (1,1,1,'2012-03-24 17:41:00','offen',0),
  (2,2,2,'2012-03-23 16:11:00','offen',0);
COMMIT;

#
# Data for the `bestellung_position` table  (LIMIT -494,500)
#

INSERT INTO `bestellung_position` (`bestellung_id`, `position_nr`, `artikel_id`, `menge`, `deleted`) VALUES

  (1,1,7856,30.000000,0),
  (1,2,7863,50.000000,0),
  (1,3,9015,1.000000,0),
  (2,1,7856,10.000000,0),
  (2,2,9010,5.000000,0);
COMMIT;



/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;