Relationale Datenbanken
=======================

.. only:: html

	.. sidebar:: Inhalt
	
	     - Abgrenzung relationale Datenbank zu anderen DB-Typen 
	     - ANSI SPARC


Historie
--------

.. only:: lehrer

	Das sieht nur der Lehrer
	
	
.. image:: figure/db_hist01.png

.. image:: figure/db_hist02.png

.. image:: figure/db_hist03.png



ANSI SPARC
-----------

Nachdem 1969 das Gremium CODASYL ein Datenbankkonzept basierend auf zwei Ebenen entwickelt hatte, in dem es um die Unabhängigkeit von einer speziellen Programmiersprache ging, wurde 1975 vom Gremium ANSI/SPARC ein Standard verabschiedet, der drei Ebenen definiert. Diese drei Ebenen gewährleisten nun eine Unabhängigkeit von der Programmiersprache wie auch von der Hardware. Dieses ANSI-3-Ebenenmodell hat sich bis in die Gegenwart als Standard für die Architektur von Datenbanksystemen durchgesetzt.


Er besteht aus drei Abstraktionsebenen

- physische Sicht auf die Daten, die interne Ebene
- logische Gesamtsicht auf die Daten, die konzeptionelle Ebene
- Benutzersichten auf die Daten, die externe Ebene 


.. image:: figure/dbmanagementsystem_01.png
	:width: 400px

.. image:: figure/dbmanagementsystem_02.png
	:width: 400px


**Externe Ebene**

Die externe Ebene enthält die Benutzersichten auf die Daten, bzw. den Ausschnitt aus den Gesamtdaten, den spezielle Benutzergruppen benötigen. Benutzersichten können hilfreich sein, um bestimmten Benutzern bzw. Benutzergruppen nur Ausschnitte des Gesamtdatenmodells zugänglich zu machen (Rechteverwaltung), oder für Umbenennungen von Attributen, um dem unterschiedlichen Vokabular von Fachabteilungen Rechnung zu tragen. Als Beispiel sei hier die Begriffe Lieferant/Kunde und Debitor/Kreditor genannt. Diese Ebene wird vom Datenbankentwickler verwaltet.


**Konzeptionelle / konzeptuelle Ebene**

Diese Ebene hat die logische Darstellung der Gesamtsicht der Daten in einem speziellen Datenmodell, z.B. dem relationalen Datenmodell zum Inhalt. Ziel ist hier eine Unternehmensgesamtsicht zu erstellen, was ohne detaillierte Kenntinisse über relevante Informationen, Logik, Abläufe und Zusammenhänge im Unternehmen nicht gelingen kann. Diese Gesamtsicht beschreibt alle Daten, deren Beziehungen untereinander und auch die Konsistenzanforderungen in Form z.B. von Integritätsbedingungen. Die verschiedenen Unternehmensansichten aus der externen Ebene aus den verschiedenen Abteilungen werden zu einem Gesamtmodell zusammengeführt. Zentrales Ziel ist hier eine redundanzfreie Speicherung der Daten, z.B. durch Normalisierung.
Innerhalb dieser Ebene wird in der Regel zweistufig verfahren. Zuerst wird ein konzeptionelles Schema (z.B. ER-Modell, UML, ...) erstellt, welches noch unabhängig vom konkret eingesetzten Datenmodell resp. Datenbanksystem ist. Davon wird dann in einem zweiten Schritt das logische Datenbankschema abgeleitet, welches schon auf das Modell eines bestimmten Datenbanktyps (relational, objektorientiert, ...) angepasst ist. Diese Ebene wird primär vom Datenbankentwickler verwaltet.



.. image:: figure/db_konzept_ansisparc.jpg




**Interne Ebene**

Dies stellt die physikalische Implementierung des konzeptionellen Schemas in Abhängigkeit zum verwendeten DBS dar. Information über die Art und den Aufbau der Datenstrukturen auf dem physikalischen Speicher und Zugriffsmechanismen sind Bestandteil der internen Ebene. Diese Ebene wird vom Datenbankadministrator verwaltet. Zu seinen Aufgaben gehören insbesondere Maßnahmen zur Laufzeitverbesserung (Performance), Verwaltung von Benutzerrechten, Verfügabarkeit (Ausfallsicherheit), Datensicherheit und Datenschutz und vieles mehr. Trotz aller SQL-Standardisierungen unterscheiden sich die Hersteller wie IBM, Oracle, MySQL, Microsoft, ... hier doch teilse gravierend.

Für die sogenannten klassischen DBS, wie z.B. Netzwerk- relationale bis hin zu objektorientierten Datenbanksystemen stellt dieses ANSI-3-Ebenenmodell wirklich ein Paradigma dar, das zwingend eingehalten wurde mit eben dem Vorteil der Datenunabhängigkeit. 


.. image:: figure/dbmanagementsystem_03.png



Hierarchisches Datenmodell
---------------------------

Ein hierarchisches Datenmodell ist ein Datenmodell in der Art eines Baumes. Es ist historisch gesehen das älteste Datenmodell und stammt aus den 1960er Jahren. Die Adressverknüpfungen werden gemeinsam mit den Daten gespeichert. Ein Datensatz kann höchstens mit einem übergeordneten, sowie mehreren untergeordneten Datensätzen in Beziehung stehen. Dieser Datenbanktyp wurde von IBM mit dem System IMS/DB implementiert und ist sogar heutzutage noch im Einsatz, auch wenn das Datenmodell selber eher von historischem Interesse ist. Eine gewisse Renaissance erlebt die hierarchische Datenspeicherung mit XML.

Aufgrund der baumstrukturierten Adressverweise zwischen den Daten, die ja bereits bei der Speicherung bestimmt werden müssen, sind einerseits lesende Zugriffe äußerst schnell, andererseits lesende Verknüpfungen sehr unflexibel, denn die Adresspfade mussen ja schon beim Einfügen der Daten festgelegt werden. 


.. image:: figure/hierarchische_Datenbank.jpg


Netzwerk-Datenmodell
----------------------

Auszug aus:  http://de.wikipedia.org/wiki/Netzwerkdatenbankmodell

"Nach der Vorstellung beider Datenbankmodelle (Relational vs. Netzwerk) anfang der 1970er Jahre gab es schon zwanzig Jahre lang hocheffiziente Netzwerkdatenbanksysteme auf mittleren und großen Mainframes für höchste Transaktionsraten, bis relationale Datenbanksysteme bezüglich der Performance einigermaßen gleichziehen konnten. Nicht ohne Grund ist das hierarchische Datenbanksystem von IBM von Ende der 1960er noch heute bei vielen IBM-Kunden im Einsatz. Auch Abfragesprachen für Ad-hoc-Anfragen standen auf Netzwerksystemen zur Verfügung, beispielsweise QLP/1100 von Sperry Rand. Heute wird das Netzwerkdatenbankmodell hauptsächlich auf Großrechnern eingesetzt.

Bekannte Vertreter des Netzwerkdatenbankmodells sind UDS (Universal Datenbank System) von Siemens, DMS (Database Management System) von Sperry Univac. Mischformen zwischen Relationalen Datenbanken und Netzwerkdatenbanken wurden entwickelt – z. B. von Sperry Univac (RDBMS Relational Database Management System) und Siemens (UDS/SQL), mit der Absicht, die Vorteile beider Modelle zu verbinden.

Seit den 1990er Jahren wird das Netzwerkdatenbankmodell vom relationalen Datenbankmodell mehr und mehr verdrängt. Mit der Idee des semantischen Webs gewinnt das Netzwerkdatenbankmodell wieder mehr an Bedeutung. Mittlerweile existieren eine Reihe von Graphdatenbanken, die man als moderne Nachfolger der Netzwerkdatenbanken bezeichnen könnte. Ein wesentlicher Unterschied ist allerdings die Möglichkeit der dynamischen Schemaänderung."


Abgrenzung zu anderen historischen Datenbanktypen
--------------------------------------------------

Der Begriff relationale Datenbank selbst ist häufig missverständlich beschrieben. "Zwischen den Daten bestehen Beziehungen (Relationen)", daher der Name. **Nun ist aber der englische Begriff für Beziehung relationship und nicht relation**. Relation ist ein wohldefinierter Fachbegriff und kann in etwa mit "Tabelle" übersetzt werden. Relationale Datenbanken sind also in Tabellen organisiert. Die Daten werden in Spalten und Zeilen organisiert. Das hört sich so selbstverständlich an, dass man sich klar machen muss, dass es auch Alternativen gibt. Als Beispiel sei hier die Ablage von Bestelldaten eines Kunden im Format von COBOL Data Division angegeben:

.. code-block:: sh

	01 12345 Gamdschie Samweis Beutelhaldenweg 5 67676 Hobbingen Privatkunde
	05 00001 20120512 12:30:00 bezahlt
	10 7856 Silberzwiebel 15 6.15 €
	10 7863 Tulpenzwiebel 10 32.90 €
	10 9015 Spaten 1 19.90 €
	05 00002 20120530 17:15:00 offen
	10 9010 Schaufel 1 15.00 $
	10 9015 Spaten 1 19.90 €
	01 12346 Beutlin Frodo Beutelhaldenweg 1 67676 Hobbingen Privatkunde
	05 00001 20111110 11:15:00 bezahlt
	10 3001 Papier (100) 10 23.00 €
	10 3005 Tinte (gold) 1 55.70 €
	10 3006 Tinte (rot) 1 6.20 €
	10 3010 Feder 5 25.00 €


Die Informationen sind hier hierarchisch organisiert. Die Zugehörigkeit einer Information (beispielsweise eines bestellten Artikels) ergibt sich aus der Position, wo die Information steht, oder besser: unter welcher Information diese steht. In der grafischen Darstellung wird dies deutlicher

.. image:: figure/data_hierarchisch.jpg



